<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
chdir(__DIR__ . '/../../../');
$filename = 'Kcdns/core.phar';
file_exists($filename) and unlink($filename);
$phar = new Phar($filename);
$dsp = "[\/\\\\]";
$phar->buildFromDirectory('Kcdns', "/^Kcdns{$dsp}(?!Public|Cmd).*/");
echo "Create phar file : " . $filename."\n";