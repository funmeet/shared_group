<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Cron;

class Model extends \Think\Model
{

    static $instace;

    protected $name = 'cron';

    public static function getInstance ()
    {
        return self::$instace ?  : self::$instace = new self();
    }

    public function gets ()
    {
        $_tasks = $this->where(array(
                "status" => 1
        ))->select();
        $task = array();
        foreach ($_tasks as $v)
        {
            $task[$v['name']] = $v;
        }
        return $task;
    }

    public function get ($idOrName)
    {
        $map = array(
                "status" => 1
        );
        is_numeric($idOrName) ? $map['id'] = (int) $idOrName : $map['name'] = $idOrName;
        return $this->where($map)->find();
    }

    public function log ($name, $time, $response)
    {
        $this->where(array(
                'name' => $name
        ))->data(array(
                'last_time' => date('Y-m-d H:i:s',NOW_TIME),
                'last_exec' => $time
        ))->save();

        M('cron_log')->data(array(
                'name' => $name,
                'add_time' => date('Y-m-d H:i:s',NOW_TIME),
                'exec_time' => $time,
                'response' => $response
        ))->add();
    }
}