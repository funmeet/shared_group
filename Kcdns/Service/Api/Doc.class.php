<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Api;

class Doc
{

    /**
     * 生成接口测试工具
     *
     * @param unknown $urlBase
     *            接口路径
     * @param unknown $hosts
     *            域名列表
     * @param string $version
     *            接口版本
     *            
     * @return string $docHtml
     */
    public static function html ($urlBase, $hosts = [], $version = 'V1')
    {
        $apiItem = [
                'hosts' => $hosts,
                'urlBase' => $urlBase,
                'api' => [],
                'code' => Code::$$version
        ];
        
        $apiDir = __DIR__ . "/$version/";
        $apiSuffix = ".class.php";
        foreach (glob("{$apiDir}*{$apiSuffix}") as $docFile)
        {
            $apiName = substr($docFile, strlen($apiDir), - strlen($apiSuffix));
            $className = "\\Service\\Api\\$version\\$apiName";
            $apiInstance = new $className();
            $apiItem['api'][$apiName]['input'] = $apiInstance->input;
            $apiItem['api'][$apiName]['output'] = $apiInstance->output;
            $apiItem['api'][$apiName]['desc'] = nl2br($apiInstance->desc);
        }
        
        ksort($apiItem['api']);
        
        require __DIR__ . "/Doc/{$version}.html";
    }

    protected static function _loadStatic ($file)
    {
        $tag = substr($file, '-3') == ".js" ? "script" : "style";
        echo "<$tag>" . file_get_contents(__DIR__ . "/Doc/Static/$file") . "</$tag>";
    }
}