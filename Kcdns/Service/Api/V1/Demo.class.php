<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Api\V1;

class Demo
{

    public $login = false;

    public $desc = "用于获取大厦列表和商品分类";

    public $input = [
            'id' => 'required;int;'
    ];

    public $output = [
            'building' => [
                    [
                            'id' => 'int;label=大厦id',
                            'title' => "label=大厦名称",
                            'def_type' => 'enum=0,1;label=默认大厦表示;comment=1：当前所选大厦，0：其他',
                            'lon' => 'label=经度;',
                            'lat' => 'label=纬度;',
                            'url' => 'label=大厦列表地址;comment=无独立地址'
                    ]
            ],
            'categroy' => [
                    [
                            'id' => 'int;label=分类ID',
                            'title' => 'label=分类名称',
                            'icon' => 'label=分类图标URL',
                            'url' => 'label=分类列表页地址'
                    ]
            ]
    ];

    public function run ($input)
    {
        return [];
    }
}