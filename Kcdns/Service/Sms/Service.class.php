<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Sms;

/**
 * 短信服务
 */
class Service
{

    /**
     * 发送短信
     *
     * @param unknown $mobile            
     * @param unknown $msg            
     * @param unknown $tpl
     *            短信模板 key
     * @param string $from
     *            来源. 值为 system 时不受发送频率等限制
     */
    public function send ($mobile, $msg, $tpl = '', $from = "web")
    {
        filter($mobile, 'required;mobile');
        filter($from, 'length=,32');
        return Sms::getInstance()->sendMsg($mobile, $msg, $tpl, $from);
    }

    /**
     * 发送短信验证码
     *
     * @param string $sceneId
     *            场景ID
     * @param string $mobile
     *            手机号
     * @param string $tpl
     *            短信模板
     */
    public function sendCode ($mobile, $tpl = '', $sceneId = '')
    {
        filter($mobile, 'required;mobile');
        return Sms::getInstance()->sendCode($sceneId, $mobile, $tpl);
    }

    /**
     * 校验短信验证码
     */
    public function checkCode ($mobile, $verifyCode, $sceneId = '', $isdelete = true)
    {
        filter($mobile, 'required;mobile');
        filter($verifyCode, 'required;length=2,10');
        return Sms::getInstance()->checkCode($mobile, $sceneId, $verifyCode, $isdelete);
    }
}
