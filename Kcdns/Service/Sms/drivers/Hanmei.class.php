<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Sms\drivers;

/**
 * 短信发送驱动 网信短信平台（云行)
 *
 * @package Service\Util
 *         
 */
class Hanmei extends \Service\Sms\DriverBase
{
    
    //短信发送
    public function send($mobile,$content){
        
        $params=$this->params;
        $params['mobile']=$mobile;
        $params['content']=$content;

        $oriResponse= OE('util')->curl($this->api_url,$params,true);

        $response = $this->xml_to_array($oriResponse);

        //发送后 返回信息 通过统一方法封装返回  参数:发送状态   接口完整响应  错误码  错误描述
        return $this->parseResponse(
            $response['SubmitResult']['code']==2,
            $oriResponse,
            $response['SubmitResult']['code']==2?"":$response['SubmitResult']['code'],
            $response['SubmitResult']['code']==2?"":$response['SubmitResult']['msg']
        );
    }

    public function xml_to_array($xml){
        $reg = "/<(\w+)[^>]*>([\\x00-\\xFF]*)<\\/\\1>/";
        if(preg_match_all($reg, $xml, $matches)){
            $count = count($matches[0]);
            for($i = 0; $i < $count; $i++){
                $subxml= $matches[2][$i];
                $key = $matches[1][$i];
                if(preg_match( $reg, $subxml )){
                    $arr[$key] = $this->xml_to_array( $subxml );
                }else{
                    $arr[$key] = $subxml;
                }
            }
        }
        return $arr;
    }


}