<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\User;

use Kcdns\Service\api\V1\UserVipInfo;

class Service
{
    // *****************************************************************************
    // ******************************** 用户基础操作 ********************************
    // *****************************************************************************
    // 注册
    public function register($username = '', $password = '', $nickname = '', $set_login = false, $email = '', $mobile = '')
    {
        return User::getInstance()->register($username, $password, $nickname, $set_login, $email, $mobile);
    }

    // 账号密码登陆
    public function login($username, $password)
    {
        return User::getInstance()->login($username, $password);
    }

    // 使用户名登录, 适用于短信登录场景
    public function loginWithUsername($username, $autoRegister = false)
    {
        try {
            $userinfo = User::getInstance()->get($username, 'username');
        } catch (\Exception $e) {
            $userinfo = null;
        }
        if (!$userinfo && $autoRegister) {
            return User::getInstance()->register($username, uniqid(), '');
        }
        return $userinfo ? User::getInstance()->setLogin($userinfo) : false;
    }

    public function setLogin($userinfo)
    {
        return User::getInstance()->setLogin($userinfo);
    }

    // 使用 Oauth2.0 信息登录, 适用于Oauth2.0登录场景
    public function loginWithOauth($oauthInfo, $autoRegister = false)
    {
        try {
            $userinfo = User::getInstance()->getUserWithOauth($oauthInfo);
        } catch (\Exception $e) {
            $userinfo = null;
        }
        if (!$userinfo && $autoRegister) {
            $oauthInfo['openid'] AND $oauthInfo['open_id'] = $oauthInfo['openid'];
            $oauthInfo['user_info']['nickname'] AND $oauthInfo['nickname'] = $oauthInfo['user_info']['nickname'];
            $uid = User::getInstance()->register(uniqid(), uniqid(), $oauthInfo['nickname']);
            return User::getInstance()->bindOauth($oauthInfo, $uid);
        }
        return $userinfo ? User::getInstance()->setLogin($userinfo, $bindOauth = false) : false;
    }


    //获取当前授权的oauth信息
    public function currentOauth()
    {
        return User::getInstance()->getCurrentOauth();
    }

    //设置当前授权的oauth信息
    public function setOauth($oauthInfo)
    {
        return User::getInstance()->setOauth($oauthInfo);
    }

    // 绑定用户 Oauth2.0 信息
    public function bindOauth($oauthInfo)
    {
        User::getInstance()->bindOauth($oauthInfo);
    }

    // 解除用户 Oauth2.0 绑定
    public function unbindOauth($oauthInfo)
    {
        User::getInstance()->unbindOauth($oauthInfo);
    }

    // 退出登录
    public function logout()
    {
        User::getInstance()->setLogout();
    }

    // 获取当前用户
    public function current()
    {
        try {
            return User::getInstance()->current();
        } catch (\Exception $e) {
            return false;
        }
    }

    // 修改用户昵称
    public function modifyNickname($nickname)
    {
        return User::getInstance()->modifyNickname($nickname);
    }

    // 修改头像
    public function modifyAvatar($avatar_id)
    {
        return User::getInstance()->modifyAvatar($avatar_id);
    }

    // 获取用户信息
    public function getinfo($usernameOrId, $field = 'id')
    {
        return User::getInstance()->get($usernameOrId, $field);
    }

    // 获取用户信息（报表专用）
    public function getInList($uidArr)
    {
        return User::getInstance()->getInList($uidArr);
    }

    // *****************************************************************************
    // ******************************** 账户操作 ************************************
    // *****************************************************************************
    // 充值
    public function recharge($type = 1, $orderRecord = array(), $refData = array())
    {
        return Account::getInstance()->recharge($type, $orderRecord, $refData);
    }

    // 激活码充值
    public function rechargeWithCode($userId, $code)
    {
        return Account::getInstance()->rechargeWithCode($userId, $code);
    }

    // 红包赠送 （备用）
    public function rechargeHongbao($userId, $money)
    {
        return Account::getInstance()->rechargeHongbao($userId, $money);
    }

    // 消费
    public function pay($userId, $accountId, $orderRecort = array())
    {
        return Account::getInstance()->pay($userId, $accountId, $orderRecort);
    }

    // 零元余额支付
    public function payZero($userId, $orderRecort = [])
    {
        return Account::getInstance()->payZero($userId, $orderRecort);
    }

    // 订单退款到账户
    public function refund($userId, $shopUserId, $orderRecord, $refundMoney)
    {
        return Account::getInstance()->refund($userId, $shopUserId, $orderRecord, $refundMoney);
    }

    // 记录
    public function accountLog($userId, $accountId, $pageNo = 1, $pageSize = 20)
    {
        return Account::getInstance()->getLog($userId, $accountId, $pageNo, $pageSize);
    }

    // *****************************************************************************
    // ******************************** 会员卡操作 **********************************
    // *****************************************************************************
    // 获取会员卡详情
    public function cardGet($cardId)
    {
        return Mcard::getInstance()->get($cardId);
    }

    // 创建会员卡
    public function add($suid, $cardInfo = array())
    {
        return Mcard::getInstance()->add_($suid, $cardInfo);
    }

    // 编辑会员卡
    public function edit($suid, $cardInfo = array())
    {
        return Mcard::getInstance()->edit($suid, $cardInfo);
    }

    // 删除会员卡
    public function delete($suid, $cardId)
    {
        return Mcard::getInstance()->delete_($suid, $cardId);
    }

    // 会员卡列表
    public function getList($suid)
    {
        return Card::getInstance()->list_($suid);
    }

    // 会员卡详情
    public function get($suid, $cardId)
    {
    }

    // 用户会员卡列表
    public function userCardList($userId, $type = 2)
    {
        return Mcard::getInstance()->listu($userId, $type);
    }

    // 用户会员卡列表
    public function userCardListUnget($userId)
    {
        return Mcard::getInstance()->listur($userId);
    }

    // 用户会员卡信息
    public function userCardInfo($userId, $accountId, $suid)
    {
        return Mcard::getInstance()->infou($userId, $accountId, $suid);
    }

    // 获取会员信息
    public function userVipInfo($uid, $type)
    {
        return UserVipItem::getInstance()->userVipInfo($uid, $type);
    }

    // 获取企业会员列表
    public function companyList($uid, $muid, $company)
    {
        return UserVip::getInstance()->companyList($uid, $muid, $company);
    }

    // 修改会员信息
    public function userVipEdit($id, $data)
    {
        return UserVipItem::getInstance()->userVipEdit($id, $data);
    }

    // 会议室赠送时间记录
    public function addVipRoomLog($uid, $suid, $type, $time_long)
    {
        return UserVipRoom::getInstance()->addVipRoomLog($uid, $suid, $type, $time_long);
    }
}