<?php
/**
 * 用户属性方法封装
 * @author hayden <hayden@yeah.net>
 */

namespace Kcdns\Service\User;

use Kcdns\Service\Coupon\Dic;
use Kcdns\User\Api\UserApi;

class User extends \Think\Model
{

    static $instace;

    protected $name = 'user';

    protected $sessionKey = '_CURRENT_UID_';

    protected $oauthSessionKey = '_OAUTH_INFO2_';

    protected $currentUser = null;

    public static function getInstance()
    {
        return self::$instace ?: (self::$instace = new self());
    }

    /**
     * 用户登录状态设置
     *
     * @author hayden <hayden@yeah.net>
     */
    public function setLogin($userinfo, $bindOauth = true)
    {
        // 注册登录统计
//        $this->queueSend($userinfo);

        $uid = is_array($userinfo) ? $userinfo['uid'] : $userinfo;
        session($this->sessionKey, $uid);

        if ($bindOauth && $oauthInfo = $this->getCurrentOauth()) {
            //Oauth 绑定
            $this->bindOauth($oauthInfo, $uid);
            //
            $this->oauthNickSave($userinfo, $oauthInfo);
        }

        /* 更新登录信息 */
        $data = array(
            'uid' => $uid,
            'login' => array(
                'exp',
                '`login`+1'
            ),
            'last_login_time' => NOW_TIME,
            'last_login_ip' => get_client_ip(1, true)
        );

        $this->save($data);
        (new UserApi())->updateLogin($uid);

        return $this->current();
    }

    public function queueSend($userinfo)
    {
        $time = strtotime(date('Y-m-d'));
        $is_app_client = O('Util')->isClient();

        if (is_numeric($userinfo)) {
            $uid = $userinfo;
        } else {
            $uid = $userinfo['id'];
        }

        if (empty($userinfo['last_login_time'])) { // 注册
            $type = 2;
        } elseif ($userinfo['last_login_time'] < $time) { // 登录
            $type = 1;
        } else {
            $in = M('report_log')->where([
                'type' => 1,
                's_type' => $is_app_client,
                'datetime' => date('Y-m-d'),
                'uid' => $uid,
            ])->find();
            if ($in) {
                return true;
            }
            $type = 1;
        }

        O('Mqueue')->send(array(
            "\\Service\\Util\\Service",
            'notifyUser'
        ), [
            'type' => $type,
            's_type' => $is_app_client,
            'ip' => $_SERVER['REMOTE_ADDR'],
            'uid' => $uid
        ]);
    }

    /**
     * 用户退出设置
     *
     * @author hayden <hayden@yeah.net>
     */
    public function setLogout()
    {
        if ($oauthInfo = $this->getCurrentOauth()) {
            $this->unbindOauth($oauthInfo, session($this->sessionKey));
        }
        session($this->sessionKey, null);
    }

    /**
     * 获取当前登录用户
     */
    public function current()
    {
        $uid = session($this->sessionKey);
        if (!$uid) {
            throw new \Exception('用户未登录');
        }
        if ($this->currentUser === null || $this->currentUser['id'] != $uid) {
            $this->currentUser = $this->get($uid);
        }
        return $this->currentUser;
    }

    public function get($value, $field = 'id')
    {
        $where = array(
            $field => $value
        );
        filter($where, array(
            'id' => 'int',
            'username' => 'mobile_or_email'
        ));
        if (!$where) {
            throw new \Exception('查询参数无效');
        }

        // Base Info
        $ucenterUserinfo = M('ucenter_member')->where($where)->find();
        if (!$ucenterUserinfo) {
            throw new \Exception('用户不存在');
        }

        // User Info
        $userinfo = $this->where(array(
            'uid' => $ucenterUserinfo['id']
        ))->find();

        if (!$userinfo) {
            throw new \Exception('用户不存在');
        }

        if (is_callable(array('\Service\User\Service', 'getUserInfoCallback'))) {
            if ($result = O('user')->getUserInfoCallback($userinfo)) {
                $userinfo = $result;
            }
        }

        // OAuth2.0 Info
        $where = array(
            'user_id' => $ucenterUserinfo['id']
        );
        $res = M("Oauth")->where($where)->field('type,openid')->group("type")->limit(20)->select();
        $userinfo['oauth'] = array();
        if ($res) {
            foreach ($res as $k => $v) {
                $userinfo['oauth'][$v['type']] = $v['openid'];
            }
        }

        // 头像处理
        if ($userinfo['cover_id']) {
            $userinfo['avatar_url'] = 'http://' . $_SERVER['HTTP_HOST'] . get_cover($userinfo['cover_id'])['url'];
        }

        return array_merge($ucenterUserinfo, $userinfo);
    }

    // 获取用户信息（报表专用）
    public function getInList($uidArr = array())
    {
        $ucInfo = M('ucenter_member')->where(['id' => ['in', $uidArr]])->getField('id,username,mobile');
        if (!$ucInfo) {
            throw new \Exception('用户不存在');
        }

        $where = [];
        foreach ($ucInfo as $k => $v) {
            $where[$k] = $k;
        }
        $userInfo = $this->where(['uid' => ['in', $where]])->getField('uid, nickname');

        foreach ($ucInfo as $k => &$v) {
            if ($userInfo[$k]) {
                $v['nickname'] = $userInfo[$k];
            }
        }
        return $ucInfo;
    }

    /**
     * 用户注册
     *
     * @author hayden <hayden@yeah.net>
     */
    public function register($username = '', $password = '', $nickname = '', $login = false, $email = '', $mobile='')
    {
        /* 调用注册接口注册用户 */
        $uid = (new UserApi())->register($username, $password, $email, $mobile);
        if (0 < $uid) {
            // 注册成功
            $user = array(
                'uid' => $uid,
                'nickname' => $nickname,
                'mobile' => $mobile,
                'create_time' => date('Y-m-d H:i:s'),
            );
            if (!$user = $this->create($user, 1) or !$this->add($user)) {
                throw new \Exception('用户添加失败！');
            }

            // 自动登录
            $login and $this->setLogin($uid);

            return $uid;
        }
        throw new \Exception('创建用户失败:' . $uid);
    }

    /**
     * 修改用户昵称
     */
    public function modifyNickname($nickname)
    {
        $user = $this->current();
        $uid = session($this->sessionKey);
        if (!$uid)
            throw new \Exception('无效的用户');
        $nickname = O('Util')->validate($nickname, "required|length=1,32");
        return $this->where(array(
                'uid' => $uid
            ))->save(array(
                'nickname' => $nickname
            )) !== false;
    }

    public function modifyAvatar($avatar_id)
    {
        $user = $this->current();
        $uid = session($this->sessionKey);
        if (!$uid)
            throw new \Exception('无效的用户');
        $avatar_id = O('Util')->validate($avatar_id, "required|int|min=1");
        return $this->where(array(
                'uid' => $uid
            ))->save(array(
                'cover_id' => $avatar_id
            )) !== false;
    }



    /**************Oauth相关处理***************************/

    /**
     * 依据Oauth获取绑定的用户信息
     */
    public function getUserWithOauth($oauthInfo)
    {
        $oauthRecord = M('Oauth')->where(array('openid' => $oauthInfo['openid'], 'type' => $oauthInfo['type']))->find();
        if (!$oauthRecord) throw new \Exception('invalid oauth');

        $userInfo = $oauthRecord['user_id'] ? $this->get($oauthRecord['user_id']) : false;

        if (!$userInfo) {
            throw new \Exception('invalid oauth user');
        }

        return $userInfo;
    }

    /**
     * 获取当前的oauth登录信息
     */
    public function getCurrentOauth()
    {
        return session($this->oauthSessionKey) ?: false;
    }

    /**
     * 设置当前Oauth登录信息
     */
    public function setOauth($oauthInfo)
    {
        session($this->oauthSessionKey, $oauthInfo);
    }

    /**
     * Oauth与用户绑定
     */
    public function bindOauth($oauthInfo = array(), $userId = 0)
    {
        $where = array(
            'openid' => $oauthInfo['openid'],
            'type' => $oauthInfo['type']
        );
        return M('Oauth')->where($where)->save(array('user_id' => $userId)) !== false;
    }

    /**
     * Oauth与用户解绑
     */
    public function unbindOauth($oauthInfo, $userId)
    {
        $where = array(
            'openid' => $oauthInfo['openid'],
            'type' => $oauthInfo['type'],
            'user_id' => $userId
        );
        return M('Oauth')->where($where)->save(array('user_id' => 0)) !== false;
    }

    public function oauthNickSave($userInfo, $oauthInfo)
    {
        $data = [];
        if (preg_match("/^1[3578]\d{9}$/", $userInfo['nickname']) && !empty($oauthInfo['user_info']['nickname'])) {
            $data['nickname'] = $oauthInfo['user_info']['nickname'];
        }

        if (empty($userInfo['avatar_url']) && !empty($oauthInfo['user_info']['head'])) {
            $data['avatar_url'] = $oauthInfo['user_info']['head'];
        }


        if (!empty($data)) {
            $data['uid'] = $userInfo['uid'];
            return M('User')->save($data) !== false;
        }
        return true;
    }
}
