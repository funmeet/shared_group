<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Mqueue;

class Service
{

    /**
     * 发布异步任务
     *
     * @param string $callback            
     * @param string $param            
     * @param number $queue            
     * @throws \Exception
     */
    public function send ($callback, $param, $queue = 0)
    {
        Mqueue::getInstance()->send($callback, $param, $queue);
    }

    /**
     * 发布异步任务
     *
     * @param string $callback            
     * @param string $param            
     * @param number $queue            
     * @throws \Exception
     */
    public function handle ($queue = 0)
    {
        return Mqueue::getInstance()->handle($queue);
    }

    /**
     * 执行指定的未成功任务
     *
     * @param unknown $messageId            
     * @throws \Exception
     */
    public function execute ($messageId)
    {
        return Mqueue::getInstance()->execute($messageId);
    }
}