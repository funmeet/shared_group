<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Util;

/**
 * 加载静态文件 (js/css)
 * 动态修改版本号, 刷新客户端缓存
 * 使用 nginx/apache concat 模块压缩静态文件
 *
 * 目前支持本地文件
 */
class Assets
{

    protected static $config = [
            // 是否缓存文件信息
            'cache' => false,
            // 是否输出 concat 模式标签
            'concat' => false,
            // contct 最大文件数量
            'concat_max' => 10,
            // 版本号变量
            'var' => 'v',
            // 本地地址前缀, 前缀 + 文件url = 文件本地路径
            'path_prefix' => ''
    ];
    
    // 待加载静态资源文件列表
    protected static $assets = [];

    /**
     * 静态资源操作
     *
     * @param $assets string
     *            true : 返回 html 标签
     *            false : 清空待加载静态资源文件列表
     *            字符串 : 文件url(绝对路径), 注册为待加载静态资源文件
     *            数组 : 更新配置
     *            
     * @return string
     */
    public static function import ($assets = true)
    {
        $return = '';
        
        // 返回 html 标签
        if ($assets === true)
        {
            $return = self::_getHtmlTags();
            self::$assets = [];
        }
        
        // 清空已注册静态资源文件列表
        elseif ($assets === false)
        {
            self::$assets = [];
        }
        
        // 更新配置
        elseif (is_array($assets))
        {
            self::$config = array_merge(self::$config, $assets);
        }
        
        // 注册资源文件
        elseif (is_string($assets))
        {
            self::$assets[] = $assets;
        }
        
        return $return;
    }

    /**
     * 生成 html 资源标签 script/link
     */
    protected static function _getHtmlTags ()
    {
        $assets = [];
        foreach (self::$assets as $v)
        {
            $_fileinfo = self::_loadFile($v) and $assets[$_fileinfo['type']][] = $_fileinfo;
        }
        
        $tags = [];
        $tagTpl = [
                'css' => '<link rel="stylesheet" type="text/css" href="%s?%s=%s" />',
                'js' => '<script type="text/javascript" src="%s?%s=%s"></script>'
        ];
        foreach ($assets as $type => $files)
        {
            if (! isset($tagTpl[$type]))
            {
                continue;
            }
            
            // 开启 concat 模式时合并文件
            self::$config['concat'] and $files = self::_concat($files);
            
            foreach ($files as $file)
            {
                $tags[] = sprintf($tagTpl[$type], $file['url'], self::$config['var'], $file['md5']);
            }
        }
        return implode("\r\n", $tags)."\r\n";
    }

    /**
     * 加载文件信息
     *
     * @param string $filename            
     */
    protected static function _loadFile ($filename)
    {
        $cacheKey = "ASSETS_FILE_" . $filename;
        $fileinfo = self::$config['cache'] ? S($cacheKey) : false;
        if ($fileinfo === false)
        {
            $filepath = self::$config['path_prefix'] . $filename;
            $fileinfo = file_exists($filepath) ? [
                    'type' => substr($filepath, strrpos($filepath, '.') + 1),
                    'md5' => md5_file($filepath),
                    'url' => $filename
            ] : 0;
            self::$config['cache'] and S($cacheKey, $fileinfo);
        }
        if($fileinfo===0){
            echo "<pre>";var_dump( $filepath );exit;
        }
        return $fileinfo;
    }

    protected function _concat ($files)
    {
        $tmp = [];
        $concatFiles = [];
        $css = $js = 0;
        
        foreach ($files as $v)
        {
            $index = $v['type'] . floor((${$v['type']} ++) / self::$config['concat_max']);
            $tmp[$index]['type'] = $v['type'];
            $tmp[$index]['urls'][] = substr($v['url'], 1);
            $tmp[$index]['md5s'][] = $v['md5'];
        }
        foreach ($tmp as $type => $v)
        {
            $concatFiles[] = [
                    'type' => $type,
                    'url' => '/??' . implode(',', $v['urls']),
                    'md5' => md5(implode('', $v['md5s']))
            ];
        }
        
        return $concatFiles;
    }
}