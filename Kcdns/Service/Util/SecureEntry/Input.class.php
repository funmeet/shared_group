<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Util\SecureEntry;

class Input
{

    protected static $config = [];

    protected static $route = '';

    public static function init ($config, $route)
    {
        self::$config = $config;
        self::$route = $route;
    }

    /**
     * *******************************************************************************************
     * *************************************** 校验 input 参数 *************************************
     * *******************************************************************************************
     */
    public static function check ($controller)
    {
        if (self::_isProtected(self::$route))
        {
            $inputFilterProperty = ACTION_NAME . "Input";
            $inputFilter = property_exists($controller, $inputFilterProperty) ? $controller->$inputFilterProperty : [];
            filter($_GET, isset($inputFilter['get']) ? $inputFilter['get'] : []);
            filter($_POST, isset($inputFilter['post']) ? $inputFilter['post'] : []);
            $_REQUEST = array_merge($_POST, $_GET);
        }
        return true;
    }

    /**
     * 检测入口是否过滤 GET/POST 数据
     */
    protected static function _isProtected ($route)
    {
        is_array(self::$config['SECURE_ENTRY_INPUT_IGNORE']) or self::$config['SECURE_ENTRY_INPUT_IGNORE'] = [];
        foreach (self::$config['SECURE_ENTRY_INPUT_IGNORE'] as $_vv)
        {
            if (preg_match("#^{$_vv}$#i", $route))
            {
                return false;
            }
        }
        return true;
    }
}