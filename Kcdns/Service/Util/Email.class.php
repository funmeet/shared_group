<?php
namespace Kcdns\Service\Util;

class Email
{

    public static function send ($emailAddress, $subject = '', $body = '', $attachment = array())
    {
        require_once 'PHPMailer/PHPMailerAutoload.php';
        $mail = new \PHPMailer();
        
        $mail->isSMTP();
        $mail->Host = C('EMAIL_SMTP_SERVER');
        $mail->SMTPAuth = true;
        $mail->Username = C('EMAIL_SMTP_USER');
        $mail->Password = C('EMAIL_SMTP_PASSWORD');
        $mail->Port = C('EMAIL_SMTP_PORT');
        $mail->setFrom(C('EMAIL_SMTP_USER'), C('EMAIL_SMTP_NICKNAME'));
        $mail->CharSet = 'UTF-8';
        $mail->isHTML(true);
        
        // 设置接收邮箱
        is_array($emailAddress) or $emailAddress = array(
                $emailAddress
        );
        foreach ($emailAddress as $v)
        {
            $mail->addAddress($v);
        }
        
        // 设置附件
        foreach ($attachment as $v)
        {
            $mail->addAttachment($v);
        }
        
        $mail->Subject = $subject;
        $mail->Body = $body;
        
        if (! $mail->send())
        {
            throw new \Exception($mail->ErrorInfo);
        }
        
        \KCSLog::DEBUG("Email To : " . implode(',', $emailAddress));
        \KCSLog::DEBUG("Email Subject : " . $subject);
        \KCSLog::DEBUG("Email Body : " . $body);
        
        return true;
    }
}