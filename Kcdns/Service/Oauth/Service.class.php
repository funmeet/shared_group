<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Oauth;

class Service
{
    // oauth授权发起 获取用户信息
    public function getUserInfo ($oauthType = 'weixin')
    {
        return Oauth::getInstance()->getAuthinfo($oauthType);
    }
    
    // 依据openid获取已授权用户oauth用户信息
    public function getByOpenId ($openid, $oauthType = 'weixin')
    {
        return Oauth::getInstance()->getOauthByKey($openid, $oauthType);
    }
}