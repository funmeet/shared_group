<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Pay;

/**
 * 支付相关组件
 *
 * @package Service\Pay
 *         
 */
class Service
{
    
    // 发起支付参数校验说明
    protected $payValidate = array(
            'orderNo' => 'required', // 订单号
            'amount' => 'required', // 支付金额
            'payType' => 'required', // 支付类型 Weixin Alipay
            'callback' => 'required', // 支付成功业务回调
            'redirectUrl' => 'required', // 支付成功后跳转地址
            'orderTitle' => 'required', // 订单名称
            'orderDesc' => '', // 订单描述
            'returnUrl' => '', // 支付同步通知回调地址 默认 Pay/Gateway/callback
            'notifyUrl' => '', // 支付异步通知回调地址 默认 Pay/Gateway/notify
            'weixin_openid' => '', // 微信支付 openid
    );

    /**
     * 发起支付 支付网关
     *
     * @param $orderNo 订单号            
     * @param $amount 支付金额(元)            
     * @param string $payType
     *            支付类型
     * @param $callback 支付成功后的业务回调            
     * @param string $returnUrl
     *            支付同步通知地址
     * @param string $notifyUrl
     *            支付异步通知地址
     */
    public function pay ($params)
    {
        OE('Util')->validate($this->payValidate);
        return Pay::getInstance()->pay($params);
    }

    /**
     * 同步回调
     */
    public function sync ($paySn)
    {
        return Pay::getInstance()->notify($paySn, 'return');
    }

    /**
     * 异步回调
     */
    public function async ($paySn)
    {
        return Pay::getInstance()->notify($paySn, 'notify');
    }

    /**
     * 退款发起
     */
    public function refund ($orderNo, $amount,$callback)
    {
        return Pay::getInstance()->refund($orderNo,$amount,$callback);
    }

    /**
     * 退款同步回调处理
     */
    public function syncRefund($paySn){
        return Pay::getInstance()->rnotify($paySn,'rreturn');
    }

    /**
     * 退款异步回调处理
     */
    public function asyncRefund($paySn){
        return Pay::getInstance()->rnotify($paySn,'rnotify');
    }

    /**
     * 签名验证测试(开发测试)
     */
    public function checkSign($params,$payType){
        return Pay::getInstance()->checkSign($params,$payType);
    }

    // 获取交易流水号信息
    public function getInList($orderNo)
    {
        return Pay::getInstance()->getInList($orderNo);
    }

    //判断当前支付类型是否可用
    public function isEnabled($payType){
        return Pay::getInstance()->isEnabled($payType);
    }

    //订单查询
    public function query($paySn){
        return Pay::getInstance()->query($paySn);
    }
}