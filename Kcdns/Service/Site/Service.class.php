<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Site;

/**
 * 前台站点数据获取组件
 *
 * @package Service\Site
 *
 */
class Service
{

    /*
     * 获取广告列表
     * @param mixed $position   string:指定单个广告位列表  array：指定多个广告位列表
     */
    public function getAdsList($position)
    {
        return Ads::getInstance()->get($position);
    }

    /*
     * 获取广告位分类
     */
    public function getAdsCategory()
    {
        return Ads::getInstance()->getCategory() ?: C('ADS_CATEGORY');
    }

    /*
     * 获取广告分类下广告数量
     */
    public function getAdsNumByCate()
    {
        return Ads::getInstance()->getAdsNumByCate();
    }

    /*
     * 获取导航列表
     */
    public function getNavList()
    {
        return Site::getInstance()->getChannel();
    }

    /*
     * 获取站点设置
     */
    public function getSetting($name)
    {
        return Site::getInstance()->getSetting($name);
    }

    /*
     * 获取文案配置
     */
    public function getContent($name)
    {
        return Site::getInstance()->getContent($name);
    }

    /*
     * 获取站点LOGO图片
     */
    public function getLogo($width, $height)
    {
        return Site::getInstance()->getLogo($width, $height);
    }

    /*
     * 获取下载中心列表
     */
    public function getDownloadList($page = 1, $pageSize = 10)
    {
        return Site::getInstance()->getDownloadList($page, $pageSize);
    }

    /*
     * 获取文件
     */
    public function getFile($id, $field)
    {
        return Download::getInstance()->getFile($id, $field);
    }

    /*
     * 检测文档别名
     */
    public function checkDocumentAlias($uri)
    {
        return Site::getInstance()->checkDocumentUrl($uri);
    }

    /**
     * 检测分类别名
     */
    public function checkCategoryAlias($uri)
    {
        return SIte::getInstance()->checkCategoryUrl($uri);
    }

    /*
     * 生成站点token
     */
    public function createSiteToken()
    {
        return Site::getInstance()->createSiteToken();
    }

    /*
     * 检测站点token
     */
    public function checkSiteToken()
    {
        return Site::getInstance()->checkSiteToken();
    }

    /**
     * $tag = array(
     * 'cateid' => 51,
     * 'row'=>10.
     * 'field'=>'*',
     * 'pos'=>0
     * );
     */
    public function articleList($tag)
    {
        return Article::getList($tag);
    }

    public function pageList($page = 0, $cateid = 0, $where = array())
    {
        return Article::pageList($page, $cateid, $where);
    }

    public function myGetList($category_id, $page = 1, $size = 20)
    {
        return Article::myGetList($category_id, $page, $size);
    }

    public function myGetDetail($id)
    {
        return Article::myGetDetail($id);
    }

    public function articleListsAll($cate_name, $page = 1, $page_size = 10)
    {
        return Article::listsAll($cate_name, $page, $page_size);
    }

    public function articleLists($cate_name, $page = 1, $page_size = 10)
    {
        return Article::lists($cate_name, $page, $page_size);
    }

    public function detailSingle($cate_name)
    {
        return Article::detailSingle($cate_name);
    }

    public function articleDetail($id, $field='id')
    {
        return Article::Detail($id, $field);
    }

    public function getPosterList($category)
    {
        return Poster::getInstance()->getPosterList($category);
    }
}