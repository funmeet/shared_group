<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Service\Site;

/**
 * 获取前台广告模块数据
 *
 * @package Service\Site
 *
 */
class Poster extends \Think\Model
{

    protected $name = 'poster';
    static $instance;

    public static function getInstance()
    {
        self::$instance or self::$instance = new self();
        return self::$instance;
    }

    public function getPosterList($category)
    {
        $list = $this->where([
            'category' => $category
        ])->order('level asc,id asc')->select();

        foreach ($list as $key => $row) {
            $list[$key]['pic_path'] = get_cover($row['pic'], 'path');
            $list[$key]['link_url'] = $row['link'] ?: 'javascript:;';
            $list[$key]['target_str'] = $row['target'] ? '_blank' : '';
        }

        return $list;
    }

}