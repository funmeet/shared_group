//url 轮询的url
//data 轮询提交的数据
//speed 轮询的时间
function Progress( config ){
	this.config = config;
	this.init();
}
Progress.prototype = {
	init: function(){
		this.build();
		// this.lx();
	},
	build: function(){
		var content = '<div class="kc_progress" style="display: none; width: 200px;position: absolute;top: 15px;left: 35%;text-align: center;"><img src="/Public/Wap/images/loading.gif" alt=""><span class="ptitle"></span><span style="font-size: 12px; margin-left: 5px">处理进度:<span class="pprogress">0%</span></span></div>'
		$( ".content-header" ).append( content )
	},
	updata: function( title, progress ){
		if( !$( ".ptitle" ).text() ){
			$( ".ptitle" ).text( title )
		}
		$( ".pprogress" ).text( progress )
	},
	requset: function( url, data ){
		return $.ajax( {
			url: url,
			type: "post",
			data: data,
			dataType: "json"
		} )
	},
	lx: function( url, data, o ){
		if( o ){
			$( ".kc_progress" ).css( "display", "block" );
		}
		var _this = this;
		var timer = setInterval( function(){
			_this.requset( url, data )
				.always( function( data ){
					$( ".kc_progress" ).css( "display", "none" );
					if( data.status == 1 ){
						$( ".kc_progress" ).css( "display", "block" );
						this.updata( data.data.title, data.data.progress );
					}
					if( data.status == 2 ){
						clearInterval( timer );
						timer = null;
						alert( data.info )
					}
					if( data.status == 0 ){
						$( ".kc_progress" ).css( "display", "none" );
						clearInterval( timer );
						timer = null;
						alert( data.info )
					}
				} )
		}, this.config.speed||3000 )
	}
}