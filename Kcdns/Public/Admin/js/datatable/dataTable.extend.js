$.AdminLTE.formInput = {
    appendInput: function(type, val, selOptions,columnIndex,option) {
        var string = '';
        var option = $.extend({'boxClass':'list_filter_box','inputClass':''},option);
        var value = val == null ? "" : ("value='" + val + "'");
        switch (type) {
            case "range":
                if (!val) {
                    string += "<div class='"+option.boxClass+"  text-center'>" + "<input type='number' name='range_left' class='form-control "+option.inputClass+" list_filter_range list_filter_left' /> - " + "<input type='number' name='range_right' class='form-control "+option.inputClass+" list_filter_range  list_filter_right' />" + "</div>";
                } else {
                    string += "<div class='"+option.boxClass+"  single'>" + "<input type='number' class='form-control "+option.inputClass+" list_filter_range' value='" + val + "'/>" + "</div>";
                }
                break;
            case 'time':
                if (!val) {
                    string += "<div class='"+option.boxClass+" '>" + "<input type='text' name='time' class='form-control "+option.inputClass+" list_filter_time' />" + "</div>";
                } else {
                    string += "<div class='"+option.boxClass+" '>" + "<input type='text' name='time' class='form-control "+option.inputClass+" list_filter_time' value='" + val + "'/>" + "</div>";
                }
                break;
            case "timerange":
            case "timestamprange":
                var _tv = val.split(',');
                var _tv1 = $.trim(_tv[0]);
                var _tv2 = $.trim(_tv[1]);
                string += "<div class='"+option.boxClass+"  text-center'>" + "<input type='text' name='range_left' class='form-control "+option.inputClass+" list_filter_timerange   list_filter_left' value='"+_tv1+"'/> - " + "<input type='text' name='range_right' class='form-control "+option.inputClass+" list_filter_timerange   list_filter_right' value='"+_tv2+"' />" + "</div>";
                break;
            case "btn":
                string += "<div class='"+option.boxClass+" '>" + "<button class='list_filter_btn'>筛选</button>" + "</div>";
                break;
            default:
                // 输入类型 检测值是否是枚举值
                if (selOptions) {
                    // 枚举类型 筛选值从枚举选择
                    string += "<div class='"+option.boxClass+" '>" + "<select  name='' class='form-control "+option.inputClass+"  select2 list_filter_select'><option value='__NO_CHOOSE__'>--请选择--</option>";
                    for (var y in selOptions) {
                        if (y === val) {
                            string += "<option value='" + y + "' selected = true>";
                        } else {
                            string += "<option value='" + y + "'>";
                        }
                        string += selOptions[y] + "</option>";
                    }
                    string += "</select></div>";
                } else {
                    //type = "text"
                    string += ("<div class='"+option.boxClass+" '>" + "<input type='text' name='filter' class='list_filter_text form-control "+option.inputClass+"' " + value + " />" + "</div>");
                }
        }

        return string;

    },
    isRequired: function(elem) {
        elem.removeClass('error');
        if (elem.val() === '') {
            elem.addClass("error").focus();
            elem.parents('td').siblings('td').off("dblclick");
            return false;
        } else {
            return true;
        }

    },
    //区间值输入框
    range: function(elem, type, eventType) {
        //判断区间输入框中的左侧输入框
        //正常情况下左侧输入框改变不会发起请求，值跳转下个输入框(右侧输入框)
        if ((elem.attr("name")).indexOf('left') !== -1) {
            var inputRight = elem.nextAll('input[name$="_right"]');
            elem.removeClass('error');
            inputRight.val("").focus();
            return false;

        } else if ((elem.attr("name")).indexOf('right') !== -1) {
            //判断区间输入框中的右侧输入框
            //正常情况下右侧输入框的值发生改变，发起请求
            //在左侧输入框的值为空的时候，不发起请求，光标调到左侧输入框，并(左侧)添加'.error'类
            //在右侧输入框的值小于或等于左侧输入框的值时，不发起请求，光标在当前输入框，并(右侧)添加'.error'类
            //if(type === ''){
            //'timerange' || type === 'range
            var inputLeft = elem.prevAll('input[name$="_left"]');
            var valLeft, valRight;

            if (type === 'timerange') {
                valLeft = $.AdminLTE.KcDate.timesTamp(inputLeft.val());
                valRight = $.AdminLTE.KcDate.timesTamp(elem.val());
            } else {
                valLeft = parseFloat(inputLeft.val());
                valRight = parseFloat(elem.val());
            }

            elem.removeClass('error');

            if (isNaN(valLeft)) {
                inputLeft.addClass('error').focus();
                elem.val("");
                return false;
            }

            if (isNaN(valRight)) {
                elem.addClass('error').focus();
                elem.val("");
                return false;
            }

            if (valLeft >= valRight) {
                elem.addClass('error');
                elem.val("");
                return false;
            }

        }
        return true;
    },
    //数组值转换
    valSwitch: function(val, type) {
        for (var i = 0; i < val.length; i++) {
            if (type === "Date") {
                val[i] = $.AdminLTE.KcDate.timesTamp(val[i]);
            } else if (type === "Number") {
                val[i] = parseFloat(val[i]);
            }
        }

        return val;
    }
};

//datatable 行内编辑
$.AdminLTE.datatableCellEdit = {
    edit: function(tableEle, table, list_grids) {
        this._tableEle = tableEle;
        this._table = table;
        this._list_grids = list_grids;
        // this._table_head = null;
        this._table_head = this._tableEle.find("thead th");
        this._edit_data = {};
        this._btnSave = this._tableEle.parents(".dataTables_wrapper").prev(".dataTables_tool").find(".btn-save");
        this._cellDblClick();

        return this._edit_data;
    },
    //双击单元格
    _cellDblClick: function() {
        var context = this;
        context._tableEle.find('tbody td').off('dblclick').on("dblclick", function() {
            //context._table_head = context._tableEle.find("thead th");
            context._cellTarget = $(this);
            context._columnVis = context._table.cell(this).index().columnVisible;
            context._columnNum = context._table.cell(this).index().column;
            context._rowId = context._cellTarget.parent('tr').children('td').find("input[type='checkbox']").val();

            var aLink = context._cellTarget.find('a');
            var checkBox = context._cellTarget.find('input[type="checkbox"]');
            var cellHead = context._table_head[context._columnVis];
            context._inputType = $(cellHead).attr("data-type");
            context._headId = $(cellHead).attr("data-id");

            //超链接不可以编辑
            if (aLink.length || checkBox.length) {
                return false;
            }
            context._cellTarget.addClass("editing");
            //插入输入框
            context._appendInput(context._inputType);
        });

        $(document).off('click.tdEdit').on('click.tdEdit', function(e) {
            if ($(e.target).closest('td .form-control').length === 0) {
                var formControl = context._tableEle.find('tbody td .form-control');
                if (formControl.length) {
                    var oldVal = formControl.parents('td').attr("title");
                    if (!$.AdminLTE.formInput.isRequired(formControl)) return false;
                    context._compareVal(formControl, oldVal);
                }
                formControl = null;
            }
        });

    },
    //插入表单
    _appendInput: function(type) {
        var val = this._cellTarget.children('.cellTd').text();
        var cell = $.AdminLTE.formInput.appendInput(type, val, this._list_grids[this._columnNum]['enum']);
        this._cellTarget.html(cell);
        this._cellTarget.attr('title', val);
        this._cellTarget.off("dblclick");

        if (type !== 'timerange') {
            this._cellTarget.find('.form-control:eq(0)').select();
        }
        //Date picker
        $('.list_filter_timerange,.list_filter_time').datepicker({
            language: 'zh-CN',
            autoclose: true,
            format: "yyyy-mm-dd",
        });

        this._bindEvent(val);
    },
    //绑定事件
    _bindEvent: function(oldVal) {
        var context = this;
        var input = context._cellTarget.find(".form-control");
        var changeType = 'change.edit';
        if (context._inputType === 'timerange') {
            changeType = "changeDate.edit";
        }

        //回车事件
        input.on("keyup", function(e) {
            if (e.keyCode === 13) {
                if (!$.AdminLTE.formInput.isRequired($(this))) return false;
                context._compareVal(input, oldVal);
            } else {
                return false;
            }
        });

        //change事件
        input.off(changeType).on(changeType, function() {
            if (!$.AdminLTE.formInput.isRequired($(this))) return false;
            context._compareVal(input, oldVal);
        });

    },
    //新旧比较
    _compareVal: function(input, oldVal) {
        var context = this;
        var currentVal = input.val();
        var cell = currentVal;
        var selVal;

        if (context._inputType === "select") {
            selVal = input.val();
            cell = currentVal = input.find("option:selected").text();
        }

        if (context._inputType === "timerange") {
            currentVal = $.AdminLTE.KcDate.timesTamp(currentVal);
            oldVal = $.AdminLTE.KcDate.timesTamp(oldVal);
        }

        context._cellTarget.html("<div class='cellTd'>" + cell + "</div>");
        context._cellDblClick();

        if (currentVal !== oldVal) {
            context._cellTarget.addClass("edited");
            if (context._inputType === "select") {
                context._getCellValue(selVal);
            } else {
                context._getCellValue(currentVal);
            }

            if (context._btnSave.hasClass('hide')) {
                context._btnSave.removeClass('hide');
            }

        }

        context._cellTarget.removeClass("editing");
    },
    //获取单元格改变的值得json
    _getCellValue: function(currentVal) {
        var context = this;
        context._rowVal = {};
        if (!context._edit_data[context._rowId]) {
            context._rowVal[context._headId] = currentVal;
            context._edit_data[context._rowId] = context._rowVal;
        } else {
            context._edit_data[context._rowId][context._headId] = currentVal;
        }
    },
};

//创建datatable
$.AdminLTE.handle_datatable = {
    'init':function(tableEle, list_grids, first_page_data){
        initDatatable(tableEle, list_grids, first_page_data);
    }
};
