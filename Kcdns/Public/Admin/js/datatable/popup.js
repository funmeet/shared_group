// 显示表单弹窗
var popup = (function() {

    // 初始化全局配置
    layer.config({
        shadeClose: false
    });

    // 开启通用弹窗
    var open = function(settings) {
        var id;
        // 默认配置
        var defaultSettings = {
            type: 1,
            title: false, // 标题
            skin: "layer-adminLTE", // 皮肤
            content: "", // 内容 : html,jquery
            area: ["800px", "auto"], // 弹窗高度,宽度
            offset: '70px', // 垂直距离
            closeClass: "#datatable-popup .btn-close", // 关闭触发对象
            //move: ".popup-title",
            shadeClose: true,
            closeBtn: 0,
            fixed: false,
            zIndex: '999999',
            success: function(el) {
                var height = el.height() + 70 + parseInt(el.css('margin-bottom'));
                $('body').height(height);
            },
            end: function() {
                $('body').height('100%');
            }
        };

        settings = $.extend({}, defaultSettings, settings, shadeOption());
        id = layer.open(settings);

        if ($(settings.closeClass).length > 0) {
            // 使用闭包参数保持 id
            + function(id) {
                $(settings.closeClass + ",.popup-close").on("click", function() {
                    close(id);
                });
            }(id);
        }

        return id;
    };

    // 开启 layer 预设类型弹窗
    var _index_ = 99999999;
    var quickOpen = function(type, content, settings, ok) {
        var defaultSettings = {
            title: "提示",
            skin: "layer-adminLTE",
            zIndex: '999999'
        };

        settings.zIndex = _index_++;

        return layer[type](content, settings, function(index) {
            index ? close(index) : null;
            ok ? ok() : null;
        });
    }

    // 关闭弹窗
    var close = function(id) {
        var closeFunc = id ? 'close' : 'closeAll';
        layer[closeFunc](id);
    };


    // 浮动确认框
    var popover = function(elem, content, yes, end) {
        var html = '<div class="popover popover-adminLTE" role="tooltip">' +
            '<div class="arrow"></div>' +
            '<button type="button" class="close popover-close"><span aria-hidden="true">&times;</span></button>' +
            '<h3 class="popover-title"></h3>' +
            '<div class="popover-content"></div>' +
            '<div class="popover-footer">' +
            '<button class="btn btn-danger btn-flat btn-ok btn-sm">确定</button>' + //<button class="btn btn-default btn-flat btn-no btn-sm">取消</button>
            '</div></div>';
        var defaultOptions = {
            delay: 0,
            placement: 'left',
            title: '提示',
            content: '',
            template: html,
            trigger: 'click|focus'
        };
        if (typeof content === "string") {
            defaultOptions.content = content;
            content = defaultOptions;
        } else {
            options = $.extend({}, defaultOptions, content);
        }

        //实例化popover
        elem.popover(content).popover('toggle');
        elem.next("div.popover-adminLTE").find(".popover-title").prepend('<i class="fa-exclamation-circle fa"></i>');
        var okBtn = elem.next("div.popover-adminLTE").find(".btn-ok");
        var noBtn = elem.next("div.popover-adminLTE").find(".popover-close");

        //确定回调
        okBtn.off().on("click", function() {
            if (yes) {
                yes();
            }
            elem.popover("hide");
        });

        //取消回调
        noBtn.off().on("click", function() {
            if (end) {
                end();
            }
            elem.popover("hide");
        });
    }

    // 判断是否有弹框
    function shadeOption() {
        var noShade = {
            shade: 0
        };
        return $(".layui-layer").length ? noShade : {};
    }

    return {
        'open': open,
        'tip': function(content) {
            var settings = {
                time: 3000
            };
            return quickOpen('msg', content, settings);
        },
        'confirm': function(content, ok) {
            return quickOpen('confirm', content, shadeOption(), ok);
        },
        'alert': alert = function(content, ok) {
            return quickOpen('alert', content, shadeOption(), ok);
        },
        'popover': popover,
        'close': close
    };
})();
