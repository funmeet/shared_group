var _datatable_column_ = (function() {

    var datatableColumn = function(o, dt, box, grids) {
        this.o = o; // 上级对象
        this.dt = dt; // datatable 对象
        this.box = box; // 容器
        this.grids = grids; // 列
        this.init();
    };

    datatableColumn.prototype = {
        init: function(o, dt, box, grids) {
            this._ceateHtml();
            this._bindEvent();
        },

        _ceateHtml: function() {

            var columns = this.dt.state()['columns'];
            var html = '';
            var grid;
            // 第一列 checkbox 不允许隐藏
            for (var i = 1; i < columns.length; i++) {
                grid = this.grids[i-1];
                if(grid.fixed || grid.fixed=='-1'){
                    continue;
                }
                html += "<li><a href='javascript:void(0)' data-column='" + i + "'>";
                html += columns[i]['visible'] ? "<i class='fa fa-check-square-o'></i>" : "<i class='fa fa-square-o'></i>";
                html += this.grids[i-1]['title'] + "</a></li>";
            }

            this.box.find(".dataTables_tool .btn-set>.dropdown-menu").html(html);
        },

        _bindEvent: function() {
            //设置列的显示和隐藏
            var context = this;
            var selector = '.dataTables_tool .btn-set>.dropdown-menu a';
            this.box.find(selector).on("click", function(e) {

                var checkBox = $(this).children('i'); //获取当前的图标元素
                var columnNum = $(this).attr('data-column'); //获取操作的‘列数’

                //判断是否选中状态
                if (checkBox.hasClass('fa-check-square-o')) {
                    checkBox.removeClass('fa-check-square-o').addClass('fa-square-o');
                } else if (checkBox.hasClass('fa-square-o')) {
                    checkBox.removeClass('fa-square-o').addClass('fa-check-square-o');
                }

                //打击当前元素下拉框不能隐藏
                $(document).off('click.tl').on('click.tl', function(e) {
                    var target = e.target;
                    if (target.closest(selector) !== null) {
                        $('.dataTables_tool .btn-set').addClass('open');
                    }
                });

                //控制dataTable列的显示隐藏
                e.preventDefault();
                context.box.find('.dataTable .search_row>td:eq(' + columnNum + ')').toggleClass('cell_hide');
                context.box.find('.dataTable .search_row>td:eq(' + columnNum + ')').find(".form-control").val("");
                context.box.find("table.dataTable thead>tr.search_row").find(".list_filter_select").val("--请选择--");
                var column = context.dt.column(columnNum);
                column.visible(!column.visible());
				context.o._setWidth();
            });
        }
    };


    return function(o, dt, box, grids) {
        return new datatableColumn(o, dt, box, grids);
    }
})();
