/*! AdminLTE app.js
 * ================
 * Main JS application file for AdminLTE v2. This file
 * should be included in all pages. It controls some layout
 * options and implements exclusive AdminLTE plugins.
 *
 * @Author  Almsaeed Studio
 * @Support <http://www.almsaeedstudio.com>
 * @Email   <abdullah@almsaeedstudio.com>
 * @version 2.3.6
 * @license MIT <http://opensource.org/licenses/MIT>
 */

//Make sure jQuery has been loaded before app.js
//确保jQuery在 app.js之前加载
if (typeof jQuery === "undefined") {
  throw new Error("AdminLTE requires jQuery");
}

/* AdminLTE
 *
 * @type Object
 * @description $.AdminLTE is the main object for the template's app.
 *              It's used for implementing functions and options related
 *              to the template. Keeping everything wrapped in an object
 *              prevents conflict with other plugins and is a better
 *              way to organize our code.
 */
$.AdminLTE = {};

/* --------------------
 * - AdminLTE Options -
 * --------------------
 * Modify these options to suit your implementation
 */
$.AdminLTE.options = {
  //Add slimscroll to navbar menus  //添加slimscroll效果到导航菜单
  //This requires you to load the slimscroll plugin //这需要你来加载slimscroll插件
  //in every page before app.js //在每一页app.js之前
  navbarMenuSlimscroll: true,   //是否加入滚动条
  navbarMenuSlimscrollWidth: "3px", //The width of the scroll bar  滚动条的宽度
  navbarMenuHeight: "200px", //The height of the inner menu //可滚动区域高度
  //General animation speed for JS animated elements such as box collapse/expand and
  //JS动画元素的动画过度速度，如盒子的展开/收缩 和
  //sidebar treeview slide up/down. This options accepts an integer as milliseconds,
  //侧边树形菜单上/下滑动.这个选项接受一个整数作为毫秒,
  //'fast', 'normal', or 'slow'
  animationSpeed: 500,
  //Sidebar push menu toggle button selector
  //侧边栏按菜单'切换按钮'选择器(控制侧边菜单的折叠效果)
  sidebarToggleSelector: "[data-toggle='offcanvas']",
  //Activate sidebar push menu
  //激活(启动)侧边栏菜单折叠效果，false时没有折叠效果
  sidebarPushMenu: true,
  //启动菜单搜索
  sidebarMenuSearch: true,
  //Activate sidebar slimscroll if the fixed layout is set (requires SlimScroll Plugin)
  //激活侧边栏slimscroll如果固定布局设置(需要slimscroll插件)
  sidebarSlimScroll: true,
  sidebarSlimScrollColor: "rgba(255,255,255,.7)",//滚动条颜色
  //Enable sidebar expand on hover effect for sidebar mini 鼠标悬浮迷你侧边栏时，侧边栏展开(注：侧边栏处去收缩状态)
  //This option is forced to true if both the fixed layout and sidebar mini
  //参数设置为true,在fixed布局和侧边菜单迷你(收缩)状态
  //are used together //被使用
  sidebarExpandOnHover: false,
  //BoxRefresh Plugin BoxRefresh(盒子刷新)插件
  enableBoxRefresh: true,
  //Bootstrap.js tooltip //Bootstrap.js 工具提示
  enableBSToppltip: true,
  BSTooltipSelector: "[data-toggle='tooltip']", //工具提示选择器
  //提示框
  enablePopTip: true,
  popTipSelector: "[data-toggle='popTip']",
  //列表全选
  checkboxAllSelector: '.check-all',
  checkboxSelector: '.ids',
  //checkbox，radio样式
  enableIcheck: true,
  //Enable Fast Click. Fastclick.js creates a more
  //native touch experience with touch devices. If you
  //choose to enable the plugin, make sure you load the script
  //before AdminLTE's app.js
  //启用快速点击。在触控设备中Fastclick.js创建一个更自然的触摸体验。如果你选择启用插件,确保你在AdminLTE app.js加载脚本
  enableFastclick: false,
  //Control Sidebar Options 控制栏参数
  enableControlSidebar: true, //是否使用控制栏
  controlSidebarOptions: {
    //Which button should trigger the open/close event //触发开/关事件的按钮选择器
    toggleBtnSelector: "[data-toggle='control-sidebar']",
    //The sidebar selector //控制栏选择器
    selector: ".control-sidebar",
    //Enable slide over content //滑动控制栏 悬浮/占用内容
    slide: true
  },
  //Box Widget Plugin. Enable this plugin
  //to allow boxes to be collapsed and/or removed
  //Box Widget(盒子部件) 插件。用这个插件可以让盒子折叠或移除
  enableBoxWidget: true,
  //Box Widget plugin options 参数
  boxWidgetOptions: {
  	//图标
    boxWidgetIcons: {
      //Collapse icon 折叠图标
      collapse: 'fa-minus',
      //Open icon 展开图标
      open: 'fa-plus',
      //Remove icon 移除图标
      remove: 'fa-times'
    },
    //选择器
    boxWidgetSelectors: {
      //Remove button selector 移除按钮选择器
      remove: '[data-widget="remove"]',
      //Collapse button selector 折叠按钮选择器
      collapse: '[data-widget="collapse"]'
    }
  },
  //Direct Chat plugin options
  //直接聊天插件参数
  directChat: {
    //Enable direct chat by default
    enable: true,
    //The button to open and close the chat contacts pane
    //聊天联系人窗格打开/关闭——控制按钮
    contactToggleSelector: '[data-widget="chat-pane-toggle"]'
  },
  //Define the set of colors to use globally around the website
  //定义一组颜色使用在全网站
  colors: {
    lightBlue: "#3c8dbc",
    red: "#f56954",
    green: "#00a65a",
    aqua: "#00c0ef",
    yellow: "#f39c12",
    blue: "#0073b7",
    navy: "#001F3F",
    teal: "#39CCCC",
    olive: "#3D9970",
    lime: "#01FF70",
    orange: "#FF851B",
    fuchsia: "#F012BE",
    purple: "#8E24AA",
    maroon: "#D81B60",
    black: "#222222",
    gray: "#d2d6de"
  },
  //The standard screen sizes that bootstrap uses. 用在bootstrap的标准屏幕尺寸
  //If you change these in the variables.less file, change
  //them here too.
  //如果你在variables.less文件改变他们，也要在这改变他们
  screenSizes: {
    xs: 480,
    sm: 768,
    md: 992,
    lg: 1200
  }
};
/* ------------------
 * - Implementation 启用JS-
 * ------------------
 * The next block of code implements AdminLTE's
 * functions and plugins as specified by the
 * options above.
 * 以下代码块按上边的参数实现了AdminLTE的方法和插件
 */
$(function () {
  "use strict";
  //Fix for IE page transitions
  //解决IE页面过渡
  $("body").removeClass("hold-transition");


  //Extend options if external options exist
  //如果外部选项存在扩展选项
  if (typeof AdminLTEOptions !== "undefined") {
    $.extend(true,
      $.AdminLTE.options,
      AdminLTEOptions);
  }

  //Easy access to options
  var o = $.AdminLTE.options;

  //Set up the object
  //设置对象
  //_init();

  //Activate the layout maker
  //激活布局 (布局：设置最小高度，侧边菜单添加 slimscroll 应用)
  $.AdminLTE.layout.activate();

  //启用菜单搜索
  if (o.enableControlSidebar) {
  	$.AdminLTE.sidebarMenuSearch.activate("[data-toggle='menuSearch']");
  }

  //Enable sidebar tree view controls
  //启用侧边树形视图控件(树形菜单)
//$.AdminLTE.tree('.sidebar');
  $.AdminLTE.tree.init('.sidebar');

  //Enable control sidebar
  //启用控制菜单
  if (o.enableControlSidebar) {
    $.AdminLTE.controlSidebar.activate();
  }

  //Add slimscroll to navbar dropdown
  //添加 slimscroll效果到导航下拉菜单
  if (o.navbarMenuSlimscroll && typeof $.fn.slimscroll != 'undefined') {
    $(".navbar .menu").slimscroll({
      height: o.navbarMenuHeight,
      alwaysVisible: false,
      size: o.navbarMenuSlimscrollWidth
    }).css("width", "100%");
  }

  //Activate sidebar push menu
  //启动侧边菜单关闭
  if (o.sidebarPushMenu) {
    $.AdminLTE.pushMenu.activate(o.sidebarToggleSelector);
  }

  //Activate Bootstrap tooltip
  //启动bootstrap 工具提示
  if (o.enableBSToppltip) {
  	if(typeof $.fn.tooltip != "undefined"){
  		$('body').tooltip({
	      selector: o.BSTooltipSelector
	    });
  	}else{
  		return false;
  	}
  }



  //Activate box widget
  //启动部件插件(折叠，移除)
  if (o.enableBoxWidget) {
    $.AdminLTE.boxWidget.activate();
  }

  //Activate fast click
  //启动快速点击(消除延迟)
  if (o.enableFastclick && typeof FastClick != 'undefined') {
    FastClick.attach(document.body);
  }

  //Activate direct chat widget
  //启动聊天部件(聊天聊天联系人框显示/隐藏)
  if (o.directChat.enable) {
    $(document).on('click', o.directChat.contactToggleSelector, function () {
      var box = $(this).parents('.direct-chat').first();
      box.toggleClass('direct-chat-contacts-open');
    });
  }



  /*
   * INITIALIZE BUTTON TOGGLE
   * 初始化按钮开关
   * ------------------------
   */
  $('.btn-group[data-toggle="btn-toggle"]').each(function () {
    var group = $(this);
    $(this).find(".btn").on('click', function (e) {
      group.find(".btn.active").removeClass("active");
      $(this).addClass("active");
      e.preventDefault();
    });

  });


});

/* ----------------------------------
 * - Initialize the AdminLTE Object -
 * ----------------------------------
 * All AdminLTE functions are implemented below.
 * 所有AdminLTE函数实现如下
 */
//function _init() {
  'use strict';

  //全局初始化函数
  $.AdminLTE.initKC2016 = {
      run: function(){
          //列表全选
          //$.AdminLTE.checkAll.init();
          //check,radio 样式启动
          if($.AdminLTE.options.enableIcheck){
              $.AdminLTE.isCheck.init();
          }
      }
  };

  /*
   * Cookie
   */
  $.AdminLTE.Cookie = {
  	prefix : 'kcdns_admin_',
  	//有效期是秒
  	setCookie: function(name,value,expire){
  		var expires = "";
  		if(expire){
  			var exp = new Date();
			exp.setTime(exp.getTime() + expire*1000);
			expires = ";expires=" + exp.toGMTString();
  		}

		document.cookie = this.prefix + name + "="+ escape (value) + expires + ";path=/";
  	},
  	getCookie: function(name){
  		var arr,reg=new RegExp("(^| )"+this.prefix+name+"=([^;]*)(;|$)");
  		if(arr=document.cookie.match(reg))
  		return unescape(arr[2]);
  		else
  		return null;
  	},
  	delCookie: function(name){
  		var exp = new Date();
  		exp.setTime(exp.getTime() - 1);
  		var cval=this.getCookie(name);
  		if(cval!=null)
  		document.cookie= this.prefix + name + "="+cval+";expires="+exp.toGMTString();
  	}
  };

  $.AdminLTE.Storage = {
  	prefix : 'kcdns_admin_',
  	setStorage: function(name,value,expire){
  		//var expires = "";
		if (typeof (Storage) !== "undefined") {
	      localStorage.setItem(this.prefix + name, value);
	    } else {
	      window.alert('Please use a modern browser to properly view this template!');
	    }
  	},
  	getStorage: function(name){
  		if (typeof (Storage) !== "undefined") {
	      return localStorage.getItem(this.prefix + name);
	    } else {
	      window.alert('Please use a modern browser to properly view this template!');
	    }
  	}
  };

  /*
   * 格式化时间
   */

  $.AdminLTE.KcDate = {
    format:function(format,timestamp){
      date=this._getDate(timestamp);
      format=this._parseFormat(date,format);
      return format;
    },
    timesTamp:function(stringTime){
      var timestamp = Date.parse(new Date(stringTime));
      timestamp = timestamp / 1000;
      return timestamp;
    },
    _getDate:function(timestamp){
      //根据时间戳返回Date对象 无则是当前时间
      var date=new Date();
      if(timestamp){
        date.setTime(timestamp*1000);
      }
      return date;
    },
    _parseFormat:function(date,format){
      //解析规则
      var o = {
        "M+" : date.getMonth()+1,                 //月份
        "d+" : date.getDate(),                    //日
        "h+" : date.getHours(),                   //小时
        "m+" : date.getMinutes(),                 //分
        "s+" : date.getSeconds(),                 //秒
        "q+" : Math.floor((date.getMonth()+3)/3), //季度
        "S"  : date.getMilliseconds()             //毫秒
      };
      //年的格式最多4位
      if(/(y+)/.test(format)){
        format=format.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length));
      }
      //最多2位
      for(var k in o){
        if(new RegExp("("+ k +")").test(format)) {
              format = format.replace(
              RegExp.$1,
                   (RegExp.$1.length==1) ? (o[k]) : (("00"+ o[k]).substr((""+ o[k]).length))
                );
        }
      }
      return format;
    }
  };

  /*
   * ready
   */

  $.AdminLTE.ready = function(callback){
  	$(function(){
  		callback();
  	});
  };
  /* Layout 布局
   * ======
   * Fixes the layout height in case min-height fails.
   *
   * @type Object
   * @usage $.AdminLTE.layout.activate()
   *        $.AdminLTE.layout.fix()
   *        $.AdminLTE.layout.fixSidebar()
   */
  $.AdminLTE.layout = {
    activate: function () {
      var _this = this;
      _this.fix();
      _this.fixSidebar();
      $(window, ".wrapper").resize(function () {
        _this.fix();
        _this.fixSidebar();
      });
    },
    fix: function () {
      // $('.content-wrapper .content').css('padding-bottom','0');
      // $('.main-footer').css('margin-top','auto');
      //Get window height and the wrapper height
      //得到window的高度和wrapper的高度
      var neg = $('.main-header').outerHeight() + $('.main-footer').outerHeight();
      var window_height = $(window).height();
      var sidebar_height = $(".sidebar").height();
      //Set the min-height of the content and sidebar based on the
      //the height of the document.
      //设置内容和侧边栏的最小高度基于文档的高度
      if ($("body").hasClass("fixed") || $("body").hasClass("layout-fixed")) {
        $(".content-wrapper, .right-side").css('min-height', window_height - $('.main-footer').outerHeight());
      }else {
        var postSetWidth;
        if (window_height >= sidebar_height) {
          $(".content-wrapper, .right-side").css('min-height', window_height - $('.main-footer').outerHeight());
          postSetWidth = window_height - $('.main-footer').outerHeight();

        } else {
          $(".content-wrapper, .right-side").css('min-height', sidebar_height);
          postSetWidth = sidebar_height;
        }

        //Fix for the control sidebar height
        //解决控制栏的高度
        var controlSidebar = $($.AdminLTE.options.controlSidebarOptions.selector);
        if (typeof controlSidebar !== "undefined") {
          if (controlSidebar.height() > postSetWidth)
            $(".content-wrapper, .right-side").css('min-height', controlSidebar.height());
        }

      }

    },
    fixSidebar: function () {
      //Make sure the body tag has the .fixed class
      //确保body标签有.fixed类
      if (!$("body").hasClass("fixed")) {//如果没有.fixed
        if (typeof $.fn.slimScroll != 'undefined') {
          //销毁slimScroll插件
          $(".sidebar").slimScroll({destroy: true}).height("auto").css("overflow",'visible');
          $(".wrapper").height("auto");
          $('.content-wrapper .content').css('padding-bottom','0');
          $('.main-footer').css('margin-top','auto');
        }
        return;
      } else if (typeof $.fn.slimScroll == 'undefined' && window.console) {
        window.console.error("Error: fixed(固定)布局要引入slimscroll 插件!"); //the fixed layout requires the slimscroll plugin
      }
      //$(".wrapper").height("100%");
      //Enable slimscroll for fixed layout
      //启用 slimscroll 用于 fixed(固定)布局
      if ($.AdminLTE.options.sidebarSlimScroll) {
        if (typeof $.fn.slimScroll != 'undefined') {
          //Destroy if it exists
          //如果存在销毁
          $(".sidebar").slimScroll({destroy: true}).height("auto");
          //Add slimscroll
          //添加 slimscroll
          $(".sidebar").slimscroll({
            height: ($(window).height() - $(".main-header").height()) + "px",
            color: $.AdminLTE.options.sidebarSlimScrollColor,//"rgba(0,0,0,0.7)",
            size: "3px"
          });
        }
      }
    }
  };

  /* sidebarMenuSearch()
   * $.AdminLTE.pushMenu("[data-toggle='menuSearch']")
   */
  $.AdminLTE.sidebarMenuSearch = {
  	activate: function(inputT) {
  		var _this = this;
  		//获得左侧菜单
  		_this.sMenu = $(".sidebar-menu");
  		//获得当前展开的li
  		_this.sMenuActiveLi = _this.sMenu.find('.active');
  		//获得查询输入框
  		_this.sInput = $("input"+inputT);
  		_this.sInput.val("");
  		//清除按钮，默认隐藏（.sr-only）
  		_this.sEmptyBtn =  _this.sInput.parent('.input-group').find('#search-btn').first();

  		//查询框获取焦点显示“x”,
  		//点击查询框外隐藏“x”,
  		//点击清空按钮，清除查询框内容，显示所有菜单项
  		_this.iState();

  		//监听查询框“keyup”事件
  		_this.iKeyup();

  	},
  	iState: function(){
  		var _this = this;
  		//获取查询框和清空按钮的父类
  		var parent = _this.sInput.parent('.input-group');
  		//鼠标悬浮在'.input-group'父类，显示清除按钮，反正隐藏
  		parent.hover(function(){
  			//显示清空按钮
  			_this.sEmptyBtn.children("i").removeClass('sr-only');
  		},function(){
  			//隐藏
  			_this.sEmptyBtn.children("i").addClass('sr-only');
  		});

  		//隐藏"清空按钮"
		$(document).on("click",function(e){
			//获取当前点击元素
			var target = $(e.target);
			//判断当前元素的父类是否有('.input-group')
			if(target.closest(parent).length === 0){
				//隐藏
				_this.sEmptyBtn.children("i").addClass('sr-only');
			}
		});

		//点击清空按钮，清除查询框内容
		_this.sEmptyBtn.on("click",function(){
			//查询框内容为空
			_this.sInput.val('');
			//还原初始状态
			_this.restore();

		});
  	},
  	iKeyup:function(){
  		var _this = this;
  		//监听查询框“keyup”事件
  		_this.sInput.on('keyup',function(e){
  			//获得查询框输入内容
			var inputVal = $(this).val();
			//创建正则表达式
			var reg = new RegExp(inputVal);
			//标记查找到的个数
			var n = 0;
			//隐藏菜单项
			_this.sMenu.find('a').parent('li').hide().removeClass('active');
			//遍历菜单下的超链接
			_this.sMenu.find('a').each(function(){
				//隐藏菜单项
				//$(this).parent('li').hide().removeClass('active');
				//判断输入内容与菜单文字的匹配
	  			if(reg.test($(this).text())){
	  				$(this).parent('li').show().attr('data-result','true')
	  				.parents('li:not(.header)').show().addClass('active');
	  				$(this).next('ul').find('li').show();
	  				// 累计
	  				n = n+1;
	  			}else{
	  				$(this).parent('li').attr('data-result','false');
	  			}

			});

			//如果查询框内容为空
			if(inputVal === ""){
				//console.log("xxxx");
				//还原初始状态
				_this.restore();
			}

			//判断查找到的个数
			if(n === 0){
				//如果不存在
				if($('.sidebar-menu .no_find').length === 0){
					//插入
					$("<li class='no_find'>没有找到你输入的菜单名</li>").appendTo(".sidebar-menu");
				}
  			}else{
  				//移除
  				$('.sidebar-menu .no_find').remove();
  			}
			if(e.keyCode == 13){
				//跳转页面
				_this.enter();
			}
  		});
  	},
  	restore:function(){
  		var _this = this;
  		//菜单下'li'标签全部显示，并去除'active'class类
  		_this.sMenu.find('li').show().removeClass('active').removeAttr('data-result').removeAttr("style");
  		//遍历最开始'li'的状态(active),增加'active'class类
  		_this.sMenuActiveLi.each(function(){
  			$(this).addClass("active");
  		});
  	},
  	enter: function(){
  		var _this = this;
  		//遍历查找结果
  		_this.sMenu.find('li[data-result="true"]').each(function(){
  			var link = $(this).children("a").attr('href');
  			if( link != "#" ){
  				window.location.href = link;
  				return;
  			}
  		});
  	}

  };

  /*
   * formCheck() 表单验证
   */
  $.AdminLTE.formCheck = {
  	_config: null,
  	_init: function(form, rules, message, extendConfig){
    		this._config = $.extend({},{
  			'rules': rules,
  			'messages': message,
  			'errorClass' : 'error',
  			'errorElement': 'span',
  			'submitHandler': this.formSubmitHandler,
  			'showErrors': null,
  			'onfocusin': false,
  			'onfocusout': false,
  			'onkeyup': false,
  			'onclick': false
      }, extendConfig);
  	},
  	valid: function(form, rules, message, extendConfig){
  		this._init(form, rules, message, extendConfig);
  		//jquery-validate
    	$(form).validate(this._config);

 	  },
  	formSubmitHandler:function(){
  		console.log("表单提交");
  	}
  };

  /*
   * popTip() 提示框
   */
  $.AdminLTE.popTip = {
  	_settings: {
		type: 'tips',
		containerSelect: '.content-pop',
    //alertWrapper: 'alert-wrapper',
		className: 'alert-error',
		radiusClass: 'flat',
		content: '错误信息',
		title: 'error',
		titleIcon: 'fa fa-ban',
		seconds: 2000,
    callback:null,
    ele:null,
    timer:null
  	},
  	_parseConfig:function(options){
  		var _this = this;
  		_this._settings = $.extend({},_this._settings,options);
  	},
  	tips: function(options){
  		var _this = this;
  		_this._parseConfig(options);
		if($.AdminLTE.options.enablePopTip){
	  			if(!_this.elem){
					_this._openAlert();
	  			}else{
	  				_this.closeAlert(_this.elem,true);
            _this._openAlert();
	  			}
		}else{
			console.log("如果要用到popTip()功能，请到app.js中开启enablePopTip(设置为true)");
		}

  	},
  	_openAlert:function(){
  		//创建容器
  		this.elem = this.htmlContent();
  		//定时关闭
  		this.timer = this.removeAlert(this.elem);
  	},
  	//成功
  	success:function(msg,callback){
  		this.tips({
  			content:msg,
  			callback:callback,
  			title: 'success',
			titleIcon: 'fa fa-check',
  			className:'alert-success'
  		});
  	},
  	//失败
  	error:function(msg,callback){
  		this.tips({
  			content:msg,
  			callback:callback,
  			title: 'error',
			titleIcon: 'fa fa-ban',
  			className:'alert-error'
  		});
  	},
  	//创建容器
  	htmlContent: function(){
  		var _this = this;
  		var content,titleIcon;
  		if( _this._settings.titleIcon !== ''){
  			titleIcon = "<i class='icon "+_this._settings.titleIcon+"'></i>";
  		}
  		content = "<div style='display:none;' class='alert alert-dismissible "+_this._settings.className+" "+_this._settings.radiusClass+"' >" +
  				   		"<button type='button' class='close'  aria-hidden='true'>×</button>"+
  				   		"<h4>"+titleIcon+_this._settings.title+"</h4>"+
  				   		_this._settings.content +
  				   	"</div>";
	   	var html = $(content).prependTo(_this._settings.containerSelect);
	   	html.stop().slideDown(function(){
	   		_this.wrapperHeight(html);
	   	});

	   	//点击关闭按钮删除Alter
	   	html.find(".close").on("click",function(e){
	   		e.preventDefault();
	   		_this.closeAlert(html);
	   	});

  		return html;
  	},
  	wrapperHeight: function(elem){
  		var _this = this;
  		var parent = elem.parent();
  		var height = elem.outerHeight(true) +15;
  		// console.log(height);
		parent.css("height",height);
  	},
  	//等待 一定时间(_this._settings.seconds)后删除
  	removeAlert: function(elem){
  		var _this = this;
  		var timer = setTimeout(function(){
  			_this.closeAlert(elem,true);
  			elem.parent().css("height",'');
		},_this._settings.seconds);

		return timer;
  	},
  	//直接删除
  	closeAlert: function(elem,pclose){
  		var _this = this;
		var parentElem = elem.parent(_this._settings.containerSelect);
		if(!pclose){
			elem.parent().css("height",'');
		}
		elem.stop().slideUp(function(){
			elem.remove();
      if(_this._settings.callback)_this._settings.callback.call(_this);
		});
		clearInterval(_this.timer);
  		_this.elem = null;
  	}
  };


  /*
   * checkAll() 列表全选功能
   */
  $.AdminLTE.checkAll = {
  	init: function(elem){
  		var _this = this;
      _this.elem = elem;
      _this.allCheckbox = _this.elem.find($.AdminLTE.options.checkboxAllSelector);
      _this.checkboxChild = _this.elem.find($.AdminLTE.options.checkboxSelector);
      _this.enableIcheck = $.AdminLTE.options.enableIcheck;
  		_this.checkAll();
  		_this.checkChild();
  	},
  	checkAll: function(){
        var _this = this;
        $(_this.allCheckbox).iCheck('uncheck');
        if(_this.enableIcheck){
            $(_this.allCheckbox).on("ifClicked",function(){
                $(this).iCheck('toggle');
                if($(this).is(":checked")){
                    $(_this.checkboxChild).iCheck('check').change();
                }else{
                    $(_this.checkboxChild).iCheck('uncheck').change();
                }
            });
            return;
        }
        $(_this.allCheckbox).click(function(){
            $(_this.checkboxChild).prop("checked", this.checked);
        });
  	},
  	checkChild: function(){
        var _this = this;
        if(_this.enableIcheck){
            $(_this.checkboxChild).on("ifClicked",function(){
    		  		var v=0;
              var options = $(_this.checkboxChild);
	  		      $(this).iCheck('toggle');
    		  		options.each(function(){
      		  			if(!$(this).is(":checked")){
      		  				$(_this.allCheckbox).iCheck('uncheck');
      			  			return;
      		  			}else{
      		  				v++;
      		  				if(v == options.length){
      		  					$(_this.allCheckbox).iCheck('check');
      		  				}
      		  			}
    		  		});
  		  	});
          return;
        }
        $(_this.checkboxChild).click(function(){
          	var option = $(_this.checkboxChild);
          	option.each(function(i){
            		if(!this.checked){
            			$(_this.allCheckbox).prop("checked", false);
            			return false;
            		}else{
            			$(_this.allCheckbox).prop("checked", true);
            		}
          	});
        });
  	}
  };

  $.AdminLTE.isCheck = {
      init: function(elem,isAll){
          this._icheckInit(elem);
          if(isAll && elem.length>0){
              $.AdminLTE.checkAll.init(elem);
          }
      },
      _icheckInit: function(elem){
          var el = $('input[type="checkbox"],input[type="radio"]');
          if(elem){
              el = elem.find('input[type="checkbox"],input[type="radio"]');
          }
          el.iCheck({
             checkboxClass: 'icheckbox_minimal-blue',
             radioClass: 'iradio_minimal-blue',
             increaseArea: '20%',
          });
      }
  };



  /* PushMenu()
   * ==========
   * Adds the push menu functionality to the sidebar.
   *
   * @type Function
   * @usage: $.AdminLTE.pushMenu("[data-toggle='offcanvas']")
   */
  $.AdminLTE.pushMenu = {
    activate: function (toggleBtn) {
      //Get the screen sizes
      var screenSizes = $.AdminLTE.options.screenSizes;

      //Enable sidebar toggle
      $(document).on('click', toggleBtn, function (e) {
        e.preventDefault();

        //Enable sidebar push menu
        //收缩
        if ($(window).width() > (screenSizes.sm - 1)) {
          if ($("body").hasClass('sidebar-collapse')) {
            $("body").removeClass('sidebar-collapse').trigger('expanded.pushMenu').addClass("fixed layout-fixed");
          } else {
            $("body").addClass('sidebar-collapse').trigger('collapsed.pushMenu').removeClass("fixed layout-fixed");
          }
        }
        //Handle sidebar push menu for small screens
        //展开
        else {
          $("body").addClass("fixed layout-fixed");
          if ($("body").hasClass('sidebar-open')) {
            $("body").removeClass('sidebar-open').removeClass('sidebar-collapse').trigger('collapsed.pushMenu');
          } else {
            $("body").addClass('sidebar-open').trigger('expanded.pushMenu');
          }
        }

        var expire = 30*24*60*60;
        if($("body").hasClass('fixed layout-fixed')){
            console.log("x");
            $.AdminLTE.Cookie.setCookie("layout", "fixed layout-fixed" ,expire);
        }else{
            console.log("f");
            $.AdminLTE.Cookie.setCookie('layout', "sidebar-collapse" ,expire);
        }

        $.AdminLTE.layout.fixSidebar();
      });

      $(".content-wrapper").click(function () {
        //Enable hide menu when clicking on the content-wrapper on small screens
        if ($(window).width() <= (screenSizes.sm - 1) && $("body").hasClass("sidebar-open")) {
          $("body").removeClass('sidebar-open');
        }
      });


      //Enable expand on hover for sidebar mini
      if ($.AdminLTE.options.sidebarExpandOnHover || ($('body').hasClass('fixed') && $('body').hasClass('sidebar-mini'))) {
        this.expandOnHover();
      }
    },
    expandOnHover: function () {
      var _this = this;
      var screenWidth = $.AdminLTE.options.screenSizes.sm - 1;
      //Expand sidebar on hover
      $('.main-sidebar').hover(function () {
        if ($('body').hasClass('sidebar-mini') && $("body").hasClass('sidebar-collapse') && $(window).width() > screenWidth) {
          _this.expand();
        }
      }, function () {
        if ($('body').hasClass('sidebar-mini') && $('body').hasClass('sidebar-expanded-on-hover') && $(window).width() > screenWidth) {
          _this.collapse();
        }
      });
    },
    expand: function () {
      $("body").removeClass('sidebar-collapse').addClass('sidebar-expanded-on-hover');
    },
    collapse: function () {
      if ($('body').hasClass('sidebar-expanded-on-hover')) {
        $('body').removeClass('sidebar-expanded-on-hover').addClass('sidebar-collapse');
      }
    }
  };

  /* Tree() 树形菜单
   * ======
   * Converts the sidebar into a multilevel
   * tree view menu.
   *
   * @type Function
   * @Usage: $.AdminLTE.tree('.sidebar')
   */
  $.AdminLTE.tree = {
  	settings: {
  		activeClass: 'active',
  		openClass: 'tree-open',
  		menuOpenClass: 'menu-open',
  		targetElem: null
  	},
  	config: function(options){
  		this.settings = $.extend({},this.settings,options);
  	},
  	init: function(menu,options){
  		var _this = this;
  		_this.animationSpeed = $.AdminLTE.options.animationSpeed;
  		_this.config(options);
  		$(document).off('click', menu + ' li a')
  		  .on('click', menu + ' li a', function (e){
  		  	_this.targetELement = _this.settings.targetElement = $(this);
  		  	_this.clickEvent($(this));
  		});
  		//$.AdminLTE.layout.fix();
  	},
  	clickEvent: function(elem){
  		var _this = this;
  		var $this = elem;
  		var checkElement = $this.next();

        	if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')) && (!$('body').hasClass('sidebar-collapse'))){
        		_this.close(checkElement,_this.animationSpeed,function(){
        			checkElement.removeClass(_this.settings.menuOpenClass);
        		});

        	}else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible'))) {
        		_this.open(checkElement,_this.animationSpeed,function(){
        			$.AdminLTE.layout.fix();
        		});
        	}



  	},
  	toggle: function(){},
  	close: function(elem,speend,callback){
  		var $this = this.targetELement;
  		var parent_li = $this.parent("li");
  		var parent_ul = $this.parents('ul').first();
  		elem.slideUp(speend, callback);
  		// parent_ul.find('li.active').removeClass('active');
  		if( parent_li.hasClass(this.settings.openClass)){
  			// parent_li.addClass('active');
  			$this.find('.fa-angle-right').addClass('up');
  		}

  	},
  	open: function(elem,speend,callback){
  		var $this = this.targetELement;
  		var parent_li = $this.parent("li");
  		var parent_ul = $this.parents('ul').first();
  		if( !parent_li.hasClass(this.settings.openClass)){
	        var ul = parent_ul.find('ul:visible').slideUp(speend);
	        ul.removeClass(this.settings.menuOpenClass);
  		}else{
  			$this.find('.fa-angle-right').removeClass('up');
  		}
  		elem.slideDown(speend, callback);
  		// parent_ul.find('li.active').removeClass('active');
		  // parent_li.addClass('active');
  	}

  };
//$.AdminLTE.tree = function (menu) {
//  var _this = this;
//  var animationSpeed = $.AdminLTE.options.animationSpeed;
//  $(document).off('click', menu + ' li a')
//    .on('click', menu + ' li a', function (e) {
//      //Get the clicked link and the next element
//      //得到点击链接和下一个元素
//      var $this = $(this);
//      var checkElement = $this.next();
//      var pElement = $this.parent("li");
//
//      //Check if the next element is a menu and is visible
//      //检测这个元素(下一个元素)是一个菜单并且可见
//      if ((checkElement.is('.treeview-menu')) && (checkElement.is(':visible')) && (!$('body').hasClass('sidebar-collapse'))) {
//        //Close the menu
//        //收起菜单
//        checkElement.slideUp(animationSpeed, function () {
//          checkElement.removeClass('menu-open');
//          //Fix the layout in case the sidebar stretches over the height of the window
//          //_this.layout.fix();
//        });
//        checkElement.parent("li").removeClass("active");
//      }
//      //If the menu is not visible
//      //如果菜单不可见
//      else if ((checkElement.is('.treeview-menu')) && (!checkElement.is(':visible')) && (!pElement.hasClass('tree-open'))) {
//        //Get the parent menu
//        //得到父类ul菜单
//        var parent = $this.parents('ul').first();
//        //Close all open menus within the parent
//        //收起所有父类中展开的菜单
//        var ul = parent.find('ul:visible').slideUp(animationSpeed);
//        //Remove the menu-open class from the parent
//        //移除menu-open类
//        ul.removeClass('menu-open');
//        //Get the parent li
//        //得到父类li
//        var parent_li = $this.parent("li");
//
//        //Open the target menu and add the menu-open class
//        //展开当前菜单并添加menu-open类
//        checkElement.slideDown(animationSpeed, function () {
//          //Add the class active to the parent li
//          checkElement.addClass('menu-open');
////          parent.find('li.active').removeClass('active');
////          parent_li.addClass('active');
//          //Fix the layout in case the sidebar stretches over the height of the window
//          _this.layout.fix();
//        });
//        parent.find('li.active').removeClass('active');
//        parent_li.addClass('active');
//      }
//      //if this isn't a link, prevent the page from being redirected
//      if (checkElement.is('.treeview-menu')) {
//        e.preventDefault();
//      }
//    });
//};

  /* ControlSidebar 控制栏
   * ==============
   * Adds functionality to the right sidebar
   *
   * @type Object
   * @usage $.AdminLTE.controlSidebar.activate(options)
   */
  $.AdminLTE.controlSidebar = {
    //instantiate the object
    //实例化对象
    activate: function () {
      //Get the object
      var _this = this;
      //Update options
      //更新参数
      var o = $.AdminLTE.options.controlSidebarOptions;
      //Get the sidebar
      //得到 控制栏
      var sidebar = $(o.selector);
      //The toggle button
      //开关按钮
      var btn = $(o.toggleBtnSelector);

      //Listen to the click event
      //监听点击事件
      btn.on('click', function (e) {
        e.preventDefault();
        //If the sidebar is not open
        //如果控制栏没有打开
        if (!sidebar.hasClass('control-sidebar-open') && !$('body').hasClass('control-sidebar-open')) {
          //Open the sidebar
          _this.open(sidebar, o.slide);
        } else {
          _this.close(sidebar, o.slide);
        }
      });

      //If the body has a boxed layout, fix the sidebar bg position
      //如果body有boxed布局，解决控制栏背景位置
      var bg = $(".control-sidebar-bg");
      _this._fix(bg);

      //If the body has a fixed layout, make the control sidebar fixed
      //如果body有fixed布局，使控制栏fixed定位
      if ($('body').hasClass('fixed') || $('body').hasClass('layout-fixed')) {
        _this._fixForFixed(sidebar);
      } else {
        //If the content height is less than the sidebar's height, force min height
        //如果内容(.content-wrapper)高度小于控制栏高,强迫设置最小高度
        if ($('.content-wrapper, .right-side').height() < sidebar.height()) {
          _this._fixForContent(sidebar);
        }
      }
    },
    //Open the control sidebar
    //打开控制栏
    open: function (sidebar, slide) {
      //Slide over content
      if (slide) {
        sidebar.addClass('control-sidebar-open');
      } else {
        //Push the content by adding the open class to the body instead
        //of the sidebar itself
        $('body').addClass('control-sidebar-open');
      }
    },
    //Close the control sidebar
    //关闭控制栏
    close: function (sidebar, slide) {
      if (slide) {
        sidebar.removeClass('control-sidebar-open');
      } else {
        $('body').removeClass('control-sidebar-open');
      }
    },
    _fix: function (sidebar) {
      var _this = this;
      if ($("body").hasClass('layout-boxed')) {
        sidebar.css('position', 'absolute');
        sidebar.height($(".wrapper").height());
        if (_this.hasBindedResize) {
          return;
        }
        $(window).resize(function () {
          _this._fix(sidebar);
        });
        _this.hasBindedResize = true;
      } else {
        sidebar.css({
          'position': 'fixed',
          'height': 'auto'
        });
      }
    },
    _fixForFixed: function (sidebar) {
      sidebar.css({
        'position': 'fixed',
        'max-height': '100%',
        'overflow': 'auto',
        'padding-bottom': '50px'
      });
    },
    _fixForContent: function (sidebar) {
      $(".content-wrapper, .right-side").css('min-height', sidebar.height());
    }
  };



  /* BoxWidget 部件
   * =========
   * BoxWidget is a plugin to handle collapsing and
   * removing boxes from the screen.
   *
   * @type Object
   * @usage $.AdminLTE.boxWidget.activate()
   *        Set all your options in the main $.AdminLTE.options object
   */
  $.AdminLTE.boxWidget = {
    selectors: $.AdminLTE.options.boxWidgetOptions.boxWidgetSelectors,
    icons: $.AdminLTE.options.boxWidgetOptions.boxWidgetIcons,
    animationSpeed: $.AdminLTE.options.animationSpeed,
    activate: function (_box) {
      var _this = this;
      if (!_box) {
        _box = document; // activate all boxes per default
      }
      //Listen for collapse event triggers
      //监控折叠
      $(_box).on('click', _this.selectors.collapse, function (e) {
        e.preventDefault();
        _this.collapse($(this));
      });

      //Listen for remove event triggers
      //监控移除
      $(_box).on('click', _this.selectors.remove, function (e) {
        e.preventDefault();
        _this.remove($(this));
      });
    },
    collapse: function (element) {
      var _this = this;
      //Find the box parent
      var box = element.parents(".box").first();
      //Find the body and the footer
      var box_content = box.find("> .box-body, > .box-footer, > form  >.box-body, > form > .box-footer");
      if (!box.hasClass("collapsed-box")) {
        //Convert minus into plus
        element.children(":first")
          .removeClass(_this.icons.collapse)
          .addClass(_this.icons.open);
        //Hide the content
        box_content.slideUp(_this.animationSpeed, function () {
          box.addClass("collapsed-box");
        });
      } else {
        //Convert plus into minus
        element.children(":first")
          .removeClass(_this.icons.open)
          .addClass(_this.icons.collapse);
        //Show the content
        box_content.slideDown(_this.animationSpeed, function () {
          box.removeClass("collapsed-box");
        });
      }
    },
    remove: function (element) {
      //Find the box parent
      var box = element.parents(".box").first();
      box.slideUp(this.animationSpeed);
    }
  };



   /*
   * webSetUp()  皮肤和结构设置
   */

  $.AdminLTE.webSetUp = {
  	settings: {
  		container: 'control-sidebar',
  		my_skins: [
		    "skin-blue",
		    "skin-black",
        //"skin-hb",
		    "skin-red",
		    "skin-yellow",
		    "skin-purple",
		    "skin-green",
		    "skin-blue-light",
		    "skin-black-light",
		    "skin-yellow-light",
		    "skin-purple-light",
		    "skin-green-light",
		    "skin-red-light"
	  	],
	  	demo: null
  	},
  	config: function(options){
  		this.settings =  $.extend({},this.settings,options);
  	},

  	init: function(options){
  		var _this = this;
  		_this.config(options);
  		_this.htmlContent();
  		$.AdminLTE.ready(function(){
  			_this.layout();
  		});

  	},
  	layout: function(){
  		var _this = this;
		  var tmp = $.AdminLTE.Cookie.getCookie('skin');
	    if (tmp && $.inArray(tmp, _this.settings.my_skins))
	      _this.change_skin(tmp);

	    //Add the change skin listener
	    $("[data-skin]").on('click', function (e) {
	      if($(this).hasClass('knob'))
	        return;
	      e.preventDefault();
	      _this.change_skin($(this).data('skin'));
	    });

	    //Add the layout manager
	    $("[data-layout]").on('click', function () {
	      _this.change_layout($(this).data('layout'));
	    });

	    $("[data-controlsidebar]").on('click', function () {
	      change_layout($(this).data('controlsidebar'));
	      var slide = !$.AdminLTE.options.controlSidebarOptions.slide;
	      $.AdminLTE.options.controlSidebarOptions.slide = slide;
	      if (!slide)
	        $('.control-sidebar').removeClass('control-sidebar-open');
	    });

	    $("[data-sidebarskin='toggle']").on('click', function () {
	      var sidebar = $(".control-sidebar");
	      if (sidebar.hasClass("control-sidebar-dark")) {
	        sidebar.removeClass("control-sidebar-dark");
	        sidebar.addClass("control-sidebar-light");
	      } else {
	        sidebar.removeClass("control-sidebar-light");
	        sidebar.addClass("control-sidebar-dark");
	      }
	    });

	    $("[data-enable='expandOnHover']").on('click', function () {
	      $(this).attr('disabled', true);
	      $.AdminLTE.pushMenu.expandOnHover();
	      if (!$('body').hasClass('sidebar-collapse'))
	        $("[data-layout='sidebar-collapse']").click();
	    });

	    // Reset options
	    if ($('body').hasClass('fixed')) {
	      $("[data-layout='fixed']").attr('checked', 'checked');
	    }
	    if ($('body').hasClass('layout-boxed')) {
	      $("[data-layout='layout-boxed']").attr('checked', 'checked');
	    }
	    if ($('body').hasClass('sidebar-collapse')) {
	      $("[data-layout='sidebar-collapse']").attr('checked', 'checked');
	    }
  	},
  	htmlContent: function(){
  		var _this = this;
  		_this.tabHtml();
  	},
  	tabHtml: function(){
  		var _this = this;
  		//Create the new tab
		var tab_pane = $("<div />", {
		    "id": "control-sidebar-theme-demo-options-tab",
		    "class": "tab-pane active"
		});

		//Create the tab button
		var tab_button = $("<li />", {"class": "active"})
		      .html("<a href='#control-sidebar-theme-demo-options-tab' data-toggle='tab'>"
		      + "<i class='fa fa-wrench'></i>"
		      + "</a>");



		//Create the menu
		var demo_settings = $("<div />");
		//Layout options
		// demo_settings.append(
		//       "<h4 class='control-sidebar-heading'>"
		//       + "布局"
		//       + "</h4>"
		//         //Fixed layout
		//       + "<div class='form-group'>"
		//       + "<label class='control-sidebar-subheading'>"
		//       + "<input type='checkbox' data-layout='fixed' class='pull-right'/> "
		//       + "固定布局"
		//       + "</label>"
		//       + "<p>激活固定布局。你不能同时使用 fixed 和boxed 布局</p>"
		//       + "</div>"
		//         //Control Sidebar Skin Toggle
		//       + "<div class='form-group'>"
		//       + "<label class='control-sidebar-subheading'>"
		//       + "<input type='checkbox' data-sidebarskin='toggle' class='pull-right'/> "
		//       + "切换右侧控制栏背景"
		//       + "</label>"
		//       + "<p>在深色和浅色皮肤间切换右侧控制栏</p>"
		//       + "</div>"
		// );

		var skins_list = _this.skinHtml();

		//demo_settings.append("<h4 class='control-sidebar-heading'>皮肤</h4>");
		demo_settings.append(skins_list);



		$.AdminLTE.ready(function(){
			//Add the tab button to the right sidebar tabs
			$("[href='#control-sidebar-settings-tab']")
		      .parent()
		      .before(tab_button);
			tab_pane.append(demo_settings);
			demo_settings = $("#control-sidebar-settings-tab").after(tab_pane);
		});

		return demo_settings;

  	},
  	skinHtml: function(){
  		var _this = this;
  		var skins_list = $("<ul />", {"class": 'list-unstyled clearfix list-layout'});
  		for( var i = 0,len = _this.settings.my_skins.length; i<len;i++){
  			_this.settings.demo = "<li class='"+ _this.settings.my_skins[i] +"-layout'>"
	          + "<a href='javascript:void(0);' data-skin='"+ _this.settings.my_skins[i] +"' class='clearfix full-opacity-hover'>"
	          + "<div><span class='model-logo'></span><span class='model-header'></span></div>"
	          + "<div><span class='model-menu'></span><span class='model-content'></span></div>"
	          + "</a>"
	          + "<p class='text-center no-margin'>"+ _this.settings.my_skins[i].substr(5) +"</p>"
	          + "</li>";

	        skins_list.append(_this.settings.demo);
  		}

  		return skins_list;
  	},
  	change_skin: function(cls) {
  		var _this = this;
	    $.each(_this.settings.my_skins, function (i) {
	      $("body").removeClass(_this.settings.my_skins[i]);
	    });

	    $("body").addClass(cls);
	    //cookie 保存期限
	    var expire = 30*24*60*60;
	    $.AdminLTE.Cookie.setCookie('skin', cls ,expire);
	    return false;
	},
	change_layout: function (cls) {
	    $("body").toggleClass(cls);
	    $.AdminLTE.layout.fixSidebar();
	    //Fix the problem with right sidebar and layout boxed
	    if (cls == "layout-boxed")
	      $.AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
	    if ($('body').hasClass('fixed') && cls == 'fixed') {
	      $.AdminLTE.pushMenu.expandOnHover();
	      $.AdminLTE.layout.activate();
	    }
	    $.AdminLTE.controlSidebar._fix($(".control-sidebar-bg"));
	    $.AdminLTE.controlSidebar._fix($(".control-sidebar"));
	}
};





//}

/* ------------------
 * - Custom Plugins -
 * ------------------
 * All custom plugins are defined below.
 */

/*
 * BOX REFRESH BUTTON  刷新按钮
 * ------------------
 * This is a custom plugin to use with the component BOX. It allows you to add
 * a refresh button to the box. It converts the box's state to a loading state.
 *
 * @type plugin
 * @usage $("#box-widget").boxRefresh( options );
 */
;(function ($) {

  "use strict";

  $.fn.boxRefresh = function (options) {

    // Render options
    var settings = $.extend({
      //Refresh button selector //刷新选择器
      trigger: ".refresh-btn",
      //File source to be loaded (e.g: ajax/src.php) 加载文件来源
      source: "",
      //Callbacks
      onLoadStart: function (box) {
        return box;
      }, //Right after the button has been clicked 在点击按钮之后
      onLoadDone: function (box) {
        return box;
      } //When the source has been loaded 当资源已经被加载

    }, options);

    //The overlay
    //覆盖层
    var overlay = $('<div class="overlay"><div class="fa fa-refresh fa-spin"></div></div>');

    return this.each(function () {
      //if a source is specified
      if (settings.source === "") {
        if (window.console) {
          window.console.log("请先指定一个来源 - boxRefresh()"); //Please specify a source first
        }
        return;
      }
      //the box
      var box = $(this);
      //the button
      var rBtn = box.find(settings.trigger).first();

      //On trigger click
      rBtn.on('click', function (e) {
        e.preventDefault();
        //Add loading overlay
        start(box);

        //Perform ajax call
        box.find(".box-body").load(settings.source, function () {
          done(box);
        });
      });
    });

    function start(box) {
      //Add overlay and loading img
      box.append(overlay);

      settings.onLoadStart.call(box);
    }

    function done(box) {
      //Remove overlay and loading img
      box.find(overlay).remove();

      settings.onLoadDone.call(box);
    }

  };

})(jQuery);

/*
 * EXPLICIT BOX CONTROLS 显式框控件
 * -----------------------
 * This is a custom plugin to use with the component BOX. It allows you to activate
 * a box inserted in the DOM after the app.js was loaded, toggle and remove box.
 *
 * @type plugin
 * @usage $("#box-widget").activateBox();
 * @usage $("#box-widget").toggleBox();
 * @usage $("#box-widget").removeBox();
 */
(function ($) {

  'use strict';

  $.fn.activateBox = function () {
    $.AdminLTE.boxWidget.activate(this);
  };

  $.fn.toggleBox = function () {
    var button = $($.AdminLTE.boxWidget.selectors.collapse, this);
    $.AdminLTE.boxWidget.collapse(button);
  };

  $.fn.removeBox = function () {
    var button = $($.AdminLTE.boxWidget.selectors.remove, this);
    $.AdminLTE.boxWidget.remove(button);
  };

})(jQuery);

/*
 * TODO LIST CUSTOM PLUGIN TODO列表自定义插件
 * -----------------------
 * This plugin depends on iCheck plugin for checkbox and radio inputs
 *
 * @type plugin
 * @usage $("#todo-widget").todolist( options );
 */
(function ($) {

  'use strict';

  $.fn.todolist = function (options) {
    // Render options
    var settings = $.extend({
      //When the user checks the input
      onCheck: function (ele) {
        return ele;
      },
      //When the user unchecks the input
      onUncheck: function (ele) {
        return ele;
      }
    }, options);

    return this.each(function () {

      if (typeof $.fn.iCheck != 'undefined') {
        $('input', this).on('ifChecked', function () {
          var ele = $(this).parents("li").first();
          ele.toggleClass("done");
          settings.onCheck.call(ele);
        });

        $('input', this).on('ifUnchecked', function () {
          var ele = $(this).parents("li").first();
          ele.toggleClass("done");
          settings.onUncheck.call(ele);
        });
      } else {
        $('input', this).on('change', function () {
          var ele = $(this).parents("li").first();
          ele.toggleClass("done");
          if ($('input', ele).is(":checked")) {
            settings.onCheck.call(ele);
          } else {
            settings.onUncheck.call(ele);
          }
        });
      }
    });
  };
}(jQuery));

var KCONE = $.AdminLTE;
KCONE.webSetUp.init();

var AjaxFromLock = false;
var AjaxFrom = function(){
    if(AjaxFromLock){
        return false;
    }
    AjaxFromLock = true;
    var el = $(this);
    $.ajax({
        'url':el.attr('action'),
        'type':el.attr('method'),
        'data':el.serialize(),
        'dataType':'json',
        'success':function(info){
            if(info.status){
                if(info['url']){
                    window.location.href=info['url'];
                }else{
                    reload();
                }
            }
            popup.tip(info.info);
        },
        'error':function(){
            popup.tip("系统异常");
        },
        'complete':function(){
            AjaxFromLock = false;
        }
    });
    return false;
}
