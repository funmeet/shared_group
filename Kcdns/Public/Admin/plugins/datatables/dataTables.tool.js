$.extend( true, $.fn.dataTable.defaults, {
	dom:
		"<'row'<'col-sm-7'><'col-sm-5'f>>" +
		"<'row'<'col-sm-12'<'table-box' tr>>>" +
		"<'row'<'col-sm-6'li><'col-sm-6'p>>",
	renderer: 'bootstrap'
} );