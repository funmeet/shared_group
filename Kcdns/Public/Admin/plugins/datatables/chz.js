;(function(){
    var oLanguage={
        "oAria": {
            "sSortAscending": ": 升序排列",
            "sSortDescending": ": 降序排列"
        },
        "oPaginate": {
            "sFirst": "首页",
            "sLast": "末页",
            "sNext": "下页",
            "sPrevious": "上页"
        },
        "sEmptyTable": "<div style='line-height:80px;text-align:center;color:#777;'>没有相关记录</div>",
        "sInfo": "共 _TOTAL_ 条", // 第 _START_ 到 _END_ 条记录，
        "sInfoEmpty": "第 0 到 0 条记录，共 0 条",
        "sInfoFiltered": "(从 _MAX_ 条记录中检索)",
        "sInfoPostFix": "",
        "sDecimal": "",
        "sThousands": ",",
        "sLengthMenu": "每页显示 _MENU_ 条",
        "sLoadingRecords": "正在载入...",
        "sProcessing": "正在载入...",
        "sSearch": "",
        "sSearchPlaceholder": "输入关键字进行搜索",
        "sUrl": "",
        "sZeroRecords": "<div style='line-height:80px;text-align:center;color:#777;'>没有相关记录</div>"
    };
    $.fn.dataTable.defaults.oLanguage=oLanguage;
    //$.extend($.fn.dataTable.defaults.oLanguage,oLanguage)
})();
