<?php

namespace Kcdns\Admin\Widget;

use Think\Controller;

/**
 * 后台列表数据挂件
 */
class DatatableWidget extends Controller
{

    /**
     * 显示列表
     */
    public function render($listData, $cfg, $export = false)
    {
        $listGrid = $this->_toListGrid($cfg->listGrid, $cfg->listOpration, $cfg);
        $listData['data'] = $this->_toListData($listData['data'], $listGrid);

        $this->assign('_total', $listData['count']);
        $this->assign('list_grids', $listGrid);
        $this->assign('list_data', $listData['data']);
        $this->assign('list_btn', $this->_parseBtn($cfg->listBtutton));

        $export and $this->_export($listGrid, $listData['data'], $export);
        IS_PJAX || !IS_AJAX ? $this->display('Widget:datatable') : $this->ajaxRender($listData['data'], $listData['count']);
    }

    /**
     * 解析请求
     */
    public function request($cfg)
    {
        $request = I('post.');
        if (isset($request['order'])) {
            $fieldIndex = $request['order'][0]['column'];
            $fieldDir = $request['order'][0]['dir'];
        }

        $i = 1;
        $order = '';
        $where = [];
        foreach ($cfg->listGrid as $alias => $v) {
            $_field = $v['field'];

            // 排序
            if ($fieldIndex == $i) {
                $order = "$_field $fieldDir";
            }

            // 搜索
            if ($v['search'] && $request['columns'][$i]['search']['value'] != '' && $request['columns'][$i]['search']['value'] != '__NO_CHOOSE__') {
                $_search = $request['columns'][$i]['search']['value'];
                // 时间区间
                if ($v['search'] == 3) {
                    $_search = explode(',', $_search);
                    $_search[0] and $where[$_field][] = [
                        'egt',
                        date('Y-m-d H:i:s', strtotime($_search[0]))
                    ];
                    $_search[1] and $where[$_field][] = [
                        'elt',
                        date('Y-m-d H:i:s', strtotime(date('Y-m-d', strtotime($_search[1]))) + 86399)
                    ];
                } // 字典
                elseif ($v['value_dic']) {
                    $where[$_field] = $_search;
                }

                // 精确搜索
                elseif ($v['search'] == 1)
                {
                    $where[$_field] = $_search;
                }

                // 默认使用模糊搜索
                else
                {
                    $where[$_field] = [
                            'LIKE',
                            "%{$_search}%"
                    ];
                }
                $a = 'sdfa';
            }

            $i++;
        }

        // 分页
        filter($request['start'], 'int;');
        filter($request['length'], 'int;max=9999');
        $request['start'] = $request['start'] ?: 0;
        $request['length'] = $request['length'] ?: 10;
        $limit = "{$request['start']},{$request['length']}";

        return compact('where', 'order', 'limit');
    }

    // 转换列表定义，兼容 listWidget
    protected function _toListGrid($listGrid, $listOpration, $cfg)
    {
        $_listGrid_ = [];
        foreach ($listGrid as $alias => $v) {
            $_v_ = [
                'field' => [
                    $alias
                ],
                'title' => $v['label'],
                'sortable' => (boolean)$v['sort'],
                'orderdir' => $v['sort'] ?: '',
                'filter' => $v['search'] == 3 ? 'timerange' : '',
                'width' => $v['width'] . 'px',
                'enum' => $v['value_dic'],
                'searchable' => $v['search'] ? 1 : 0,
                'fixed' => $v['visiable'] == 0 ? false : $v['visiable']
            ];
            $_listGrid_[] = $_v_;
        }
        if ($listOpration) {
            $_listGrid_[] = [
                'field' => [
                    '_OPRATION_'
                ],
                'title' => '操作',
                'sortable' => '',
                'orderdir' => '',
                'filter' => '',
                'width' => $cfg->listOprationWidth . 'px',
                'enum' => '',
                'searchable' => '',
                'fixed' => '1'
            ];
        }
        return $_listGrid_;
    }

    // 转换数据，兼容 listWidget
    protected function _toListData($data)
    {
        foreach ($data as &$row) {
            if ($row['_opration_']) {
                $a = [];
                foreach ($row['_opration_'] as $v) {
                    $a[] = "<a href='{$v['url']}' class='dataTable_operator_btn pjax' data-callback='{$v['type']}' data-param='{$v['param']}'>{$v['label']}</a>";
                }
                $row['_opration_'] = implode(' ', $a);
            }

            $row = array_values($row);
        }
        return $data;
    }

    // 输出 CSV
    protected function _export($listGrid, $data, $export)
    {
        $dir = './Data/Runtime/Export/';
        $file = $dir . uniqid() . '.csv';
        file_exists($dir) or mkdir($dir, 0777, true);
        $fp = fopen($file, 'w');
        if (!$fp) {
            \KCSLog::WARN('创建导出临时文件失败 ' . $file);
            E('导出文件失败');
        }

        $row1 = [];
        $findOpColumn = false;
        foreach ($listGrid as $v) {
            if ($v['title'] == '操作') {
                $findOpColumn = true;
                // continue;
            }
            $row1[] = $v['title'];
        }
        array_unshift($data, $row1);

        foreach ($data as &$row) {
            if ($findOpColumn) {
                unset($row[count($row) - 1]);
            }
            unset($row[0]);
            foreach ($row as &$v) {
                // TODO warning here
                if (($v{0} == '0') && is_numeric($v)) {
//                    $v = '="' . $v . '"';
                    $v = "\t{$v}";
                } else if (is_string($v)) {
                    $v = iconv("UTF-8", 'GBK', $v);
                }
            }
            fputcsv($fp, $row);
        }
        fclose($fp);

        header("Last-Modified: " . gmdate("l d F Y H:i:s") . " GMT");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . ($export ? (string)$export : date('ymdHi')) . ".csv");
        header("Content-Transfer-Encoding: binary");

        readfile($file);
        unlink($file);
        exit();
    }

    protected function _parseBtn($btns)
    {
        $parsed = array();
        foreach ($btns as $btn) {
            if (!checkRule($btn['url'])) {
                continue;
            }
            $btn['url'] = $btn['url'] ? U($btn['url']) : U("/" . MODULE_ALIAS . "/" . CONTROLLER_NAME . "/" . ACTION_NAME . '?' . http_build_query($_GET));
            $parsed[$btn['title']] = $btn;
        }

        return $parsed;
    }

    /**
     * 挂件异步获取列表数据
     * 输出JSON列表数据
     */
    protected function ajaxRender($list_data, $count = 0)
    {
        $response_data = array();
        // 数据总条数修正
        $count = $count ? $count : count($list_data);
        $response = array(
            'recordsFiltered' => $count,
            'recordsTotal' => $count,
            'data' => $list_data
        );

        echo json_encode($response);
    }
}
