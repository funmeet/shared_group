<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Model;

class MenuModel extends CommonModel
{

    protected function _insert ($formData, $fillData = [])
    {
        $this->_checkUrl($formData);
        return parent::_insert($formData, $fillData);
    }

    protected function _update ($formData = [], $where = [])
    {
        $this->_checkUrl($formData);
        return parent::_update($formData, $where);
    }
    
    // url 不能重复
    protected function _checkUrl ($formData)
    {
        $data = $formData[$this->name];
        
        $where = [
                'url' => $data['url']
        ];
        $data['id'] and $where['id'] = [
                'neq',
                $data['id']
        ];
        
        if ($data['url'] && $this->where($where)->find())
        {
            throw new \Exception('url 已存在');
        }
    }

    protected function _del ($delOptions)
    {
        $status = parent::_del($delOptions);
        $status and syncAuthNodes();
        return $status;
    }
}