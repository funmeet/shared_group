<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;

/**
 * 后台首页
 */
class IndexController extends AdminController
{

    public function index ()
    {
        $this->display();
    }
    
    // 清理缓存
    public function cache ()
    {
        delDirAndFile(rtrim(RUNTIME_PATH, '/'));
        syncAuthNodes();
        clearMenu();
        
        // 更新 favicon.ico
        $uploadIcoFile = ROOT_PATH . 'Uploads/favicon.ico';
        $icoFile =  trim(get_cover(C('SITE_ICON'), 'path'),'/');
        if (file_exists(ROOT_PATH.$icoFile))
        {
            unlink($uploadIcoFile);
            copy(ROOT_PATH.$icoFile, $uploadIcoFile);
        }
        
        IS_AJAX and ! IS_PJAX and $this->success("缓存清理完毕");
        
        $loglist = '';
        $loglist = "清理菜单<br>" . $loglist;
        $loglist = "同步权限<br>" . $loglist;
        $loglist = "更新商铺商品起步价<br>" . $loglist;
        $this->assign('loglist', $loglist);
        $this->display();
    }
    
    // 查看日志
    public function logview ()
    {
        // 分割日志
        if (isset($_GET['new']))
        {
            $destination = LOG_PATH . date('y_m_d') . '.log';
            if (file_exists($destination))
            {
                $st = copy($destination, dirname($destination) . '/' . substr(basename($destination), 0, - 4) . '_[' . date('H:i:s') . ']'.'.log');
                $st and unlink($destination);
            }
        }
        
        // 读取日志文件列表
        $files = glob(LOG_PATH . '*'.'.log');
        rsort($files);
        
        // 默认显示最新的日志文件
        if (! $file = I('file'))
        {
            foreach ($files as $file)
            {
                if (! strpos($file, '['))
                {
                    break;
                }
            }
        }
        
        // 文件名安全检测
        in_array($file, $files) or $file = $files[0];
        
        $this->assign('file', $file);
        $this->assign('files', $files);
        $this->display();
    }
}
