<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;
use Kcdns\User\Api\UserApi;

/**
 * 登录/退出
 */
class PublicController extends \Think\Controller
{
    // 登录
    public function login ($username = null, $password = null, $verify = null)
    {
        if (IS_POST)
        {
            
            if (defined('APP_DEBUG') && APP_DEBUG === false && ! (new \Think\Verify())->check($verify,1))
            {
                $this->error('验证码输入错误！');
            }
            
            // 调用UC登录接口登录
            $User = new UserApi();
            $uid = $User->login($username, $password);
            if (0 < $uid)
            {
                // UC登录成功
                $Member = D('Member');
                $Member->login($uid) and $this->success('登录成功！', U('index/index'));
                $error = $Member->getError();
            }
            
            // 登录失败 -1 : 账号错误或者禁用; -2 : 密码错误; 0 : 未知错误;
            $this->error($error ?  : '账号或密码错误！');
        }
        
        is_login() and $this->redirect('index/index');
        
        $ajaxData = array(
                'data' => '',
                'logout' => true,
                'loginUrl' => U('public/login')
        );
        
        IS_AJAX ? $this->ajaxReturn($ajaxData) : $this->display();
    }
    
    // 退出登录
    public function logout ()
    {
        is_login() or $this->redirect('login');
        
        D('Member')->logout();
        session('[destroy]');
        $this->success('退出成功！', U('login'));
    }
    
    // 验证码
    public function verify ()
    {
        $config = array(
                'imageH' => 38, // 验证码图片高度
                'imageW' => 152, // 验证码图片宽度
                'length' => 4, // 验证码位数
                'fontttf' => '4.ttf'
        );
        $verify = new \Think\Verify($config);
        $verify->entry(1);
    }
}
