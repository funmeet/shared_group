<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Admin\Controller;

/**
 * 短信管理
 */
class SmsController extends CommonController
{

    /**
     * 发送短信
     * 重新发送 : 传入短信 ID 时, 使用原短信内容填充表单
     */
    public function add ()
    {
        if (IS_POST)
        {
            $nickname = M('member')->where(['uid'=>UID])->getField('nickname');
            $status = OE('sms')->send(I('post.sms.target'), I('post.sms.content'), '', $nickname);
            $status ? $this->success('发送成功', U('index')) : $this->error('发送失败');
        }
        
        $id = I('get.sms.id');
        if ($id)
        {
            $data = M('sms')->where([
                    'id' => $id
            ])->find();
            foreach ($data as $k => $v)
            {
                $this->insertSaveData['sms.' . $k] = $v;
            }
        }
        parent::add();
    }
}
