<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Common\Behavior;
defined('THINK_PATH') or exit();

class SecureEntryBehavior extends \Think\Behavior
{

    public function run (&$content)
    {
        try
        {
            \Kcdns\Service\Util\SecureEntry::init(
                    [
                            'SECURE_ENTRY' => C('SECURE_ENTRY'),
                            'SECURE_ENTRY_URL_IGNORE' => C('SECURE_ENTRY_URL_IGNORE'),
                            'SECURE_ENTRY_URL_PRIVATE' => C('SECURE_ENTRY_URL_PRIVATE'),
                            'SECURE_ENTRY_URL_VAR' => C('SECURE_ENTRY_URL_VAR'),
                            'SECURE_ENTRY_URL_KEY' => C('DATA_AUTH_KEY'),
                            'SECURE_ENTRY_POST_IGNORE' => C('SECURE_ENTRY_POST_IGNORE'),
                            'SECURE_ENTRY_INPUT_IGNORE' => C('SECURE_ENTRY_INPUT_IGNORE'),
                            'SECURE_ENTRY_POST_EXPIRE'=>C('SECURE_ENTRY_POST_EXPIRE')
                    ]);
            \Kcdns\Service\Util\SecureEntry::check();
        }
        catch (\Exception $e)
        {
            $GLOBALS['THINK_PHP_404'];
            throw new \Exception('Page not found');
        }
    }
}