<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
$_config_ = array(
        'SESSION_OPTIONS' => [
            'domain' => strstr($_SERVER['HTTP_HOST'], '.'),
        ],

        // *************************************************************
        // ************************** 系统配置 **************************
        // *************************************************************
        'AUTOLOAD_NAMESPACE' => array(
                'Service' => 'Service',
                'Workerman' => 'workerman/workerman',
                'GatewayWorker' => 'workerman/gateway-worker/src',
                'Predis' => 'Service/Redis/Predis'
        ),
        
        // 数据加密 KEY
        'DATA_AUTH_KEY' => DATA_ENCRYPT_KEY,
        
        // 全局过滤函数
        'DEFAULT_FILTER' => 'htmlspecialchars',
        
        // 参数绑定过滤
        'URL_PARAMS_SAFE' => true,
        
        // 模板文件分隔符
        'TMPL_FILE_DEPR' => '_',
        
        // 数据缓存类型
        'DATA_CACHE_TYPE' => 'File',
        
        // 异常模板
        'TMPL_EXCEPTION_FILE' => KC_APP_PATH . 'Common/View/error.html',
        
        // *************************************************************
        // ************************** 模块配置 **************************
        // *************************************************************
        // 默认模块
        'DEFAULT_MODULE' => DEFAULT_MODULE ? : 'Home',
        
        // 屏蔽模块列表
        'MODULE_DENY_LIST' => array_merge(array(
                'Common',
                'User'
        ), explode(',', DENY_MODULE)),
        
        // 开放模块列表
        'MODULE_ALLOW_LIST' => array_map(function  ($value) {
            return substr($value, strlen(APP_PATH));
        }, glob(APP_PATH . '*')),
        
        // 二级域名绑定
        'APP_SUB_DOMAIN_DEPLOY' => defined('ADMIN_HOST') && ADMIN_HOST,
        
        'APP_SUB_DOMAIN_RULES' => array(
                ADMIN_HOST => 'Admin'
        ),
        
        // **************************************************************
        // ************************** URL配置 ****************************
        // **************************************************************
        'URL_CASE_INSENSITIVE' => false,
        
        'URL_MODEL' => 2,
        
        'URL_PATHINFO_DEPR' => '/',
        
        'VAR_URL_PARAMS' => '',
        
        // **************************************************************
        // ************************** 安全配置 ***************************
        // **************************************************************
        
        // 启用安全防护的入口 : url 地址签名; post 数据签名;
        // ['admin/.*']
        'SECURE_ENTRY' => [],
        
        // url 验证白名单
        // ['wap/index/index']
        'SECURE_ENTRY_URL_IGNORE' => [],
        
        // url 私有 key 签名入口 (session key)
        // ['wap/user/.*']
        'SECURE_ENTRY_URL_PRIVATE' => [],
        
        // post 数据签名白名单
        // ['.*upload.*']
        'SECURE_ENTRY_POST_IGNORE' => [],
        
        // post 令牌有效期
        'SECURE_ENTRY_POST_EXPIRE' => SECURE_TOKEN_EXPIRE,
        
        // get/post 参数过滤白名单
        'SECURE_ENTRY_INPUT_IGNORE' => [],
        
        // **************************************************************
        // ************************** 数据库配置 *************************
        // **************************************************************
        'DB_TYPE' => 'mysqli',
        
        'DB_HOST' => DB_HOST,
        
        'DB_NAME' => DB_NAME,
        
        'DB_USER' => DB_USER,
        
        'DB_PWD' => DB_PWD,
        
        'DB_PORT' => DB_PORT,
        
        'DB_PREFIX' => DB_PREFIX,
        
        // **************************************************************
        // ************************** 上传配置 ***************************
        // **************************************************************
        
        // 上传目录路径
        'UPLOAD_DIR' => './Uploads/',
        
        // 上传目录 web 访问路径,
        'UPLOAD_PATH' => '/Uploads/'
);

// 后台绑定域名
if (! defined('ADMIN_HOST') || ! ADMIN_HOST)
{
    $_config_['URL_MODULE_MAP'] = array(
            ADMIN_ALIAS => 'admin'
    );
    $_config_['MODULE_ALLOW_LIST'][] = ADMIN_ALIAS;
}

return $_config_;
