<?php
return [
        
        'app_init' => [
                'Kcdns\Common\Behavior\InitHookBehavior',
                'Kcdns\Common\Behavior\WebAccessBehavior',
        ],
        
        'secure_entry' => [
                'Kcdns\Common\Behavior\SecureEntryBehavior'
        ]
];