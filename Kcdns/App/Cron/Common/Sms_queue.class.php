<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Cron\Common;

class Sms_queue
{
    
    // 每次发送 100 条短信, 并检查发送频率, 防止由于短信平台的发送频率限制导致的发送失败
    // 发送成功后清理队列
    public static function run ()
    {
        $list = M('sms_queue')->order('id ASC')->limit('0,100')->select()?:[];
        foreach ($list as $v)
        {
            // 最近发送时间
            $lastSendTime = M('sms')->where([
                    'target' => $v['mobile']
            ])->order('id DESC')->getField('create_time');
            
            // 发送间隔小于 1 秒时不发送
            if (strtotime($lastSendTime) >= time() - 2)
            {
                sleep(1);
                // continue;
            }
            
            // 1 分钟内发送次数
            $lastMinuteCount = M('sms')->where([
                    'target' => $v['mobile'],
                    'create_time' => [
                            'gt',
                            date('Y-m-d H:i:s', time() - 61)
                    ]
            ])->count();
            
            // 发送频率不高于 10 次/分钟
            if ($lastMinuteCount > 9)
            {
                continue;
            }
            
            // 发送短信
            O('sms')->send($v['mobile'], $v['content'], '', 'system');
            
            // 移除队列记录
            M('sms_queue')->where([
                    'id' => $v['id']
            ])->delete();
        }
    }
}