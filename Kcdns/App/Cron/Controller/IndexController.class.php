<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\Cron\Controller;

/**
 * 定时任务控制器
 */
class IndexController extends \Think\Controller
{
    
    // 用于通过前台请求激活计划任务
    public function js ()
    {
        if (CRON_CLI)
        {
            return false;
        }
        o('util')->curl(U('start', '', true, true), '', false, array(
                'TIMEOUT' => 1
        ));
    }
    
    // 启动计划任务
    public function start ()
    {
        if (CRON_CLI && ! IS_CLI)
        {
            return false;
        }
        o('cron')->start(CRON_CLI ? CRON_COMMAND : null);
    }
    
    // cli 模式下执行指定任务
    public function run ()
    {
        IS_CLI or exit();
        o('cron')->run($GLOBALS['argv'][1]);
    }
}
