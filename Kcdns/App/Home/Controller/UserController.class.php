<?php

namespace Kcdns\Home\Controller;
use User\Api\UserApi;

/**
 * 用户控制器
 */
class UserController extends HomeController {

	/* 用户中心首页 */
	public function index(){
		$GLOBALS['THINK_PHP_404'] = true;
		throw new \Exception('非开放页面');
		$this->display();
	}

	/* 登录页面 */
	public function login($username = '', $password = '', $verify = '') {
		if(IS_POST){
			/* 检测验证码 */
			if(!check_verify($verify)){
				$this->error('验证码输入错误！');
			}

        	$Api = new UserApi();
			$loginUid = $Api->login(I('username'),I('passwd'));
			if ($loginUid>0) {
				o('User')->setLogin($loginUid);
				$this->success('验证成功！',U('Member/index'));
			}else {
			    $this->error('验证无效，请重新输入！');
			}
			exit;
		}
		$this->display();
	}

	/* 验证码，用于登录和注册 */
	public function verify(){
		$verify = new \Think\Verify();
		$verify->entry(1);
	}

}
