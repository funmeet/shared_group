<?php
namespace Kcdns\Home\Controller;
use User\Api\UserApi;
/**
 * 前台首页控制器
 */
class OauthController extends HomeController {

	/**
	 * 互联登录入口
	 *
	 * @author hayden <hayden@yeah.net>
	 */
	function index() {
		OE('User')->Oauth();
		$jump = I('get.loginjump');
		$url = $jump ? $jump : U('/', null, '.html');
		redirect($url);
	}
	
    ///退出登录
	function loginout() {
		OE('User')->setLogout();
		$this->success('退出成功！',U('/'));
	}

    ///登录绑定
	function bind() {
		$GLOBALS['THINK_PHP_404'] = true;
		$bindUser = session('bindUser');
		if (!$bindUser) {
			throw new \Exception('获取信息失败');
		}
		if(IS_POST){
        	$Api = new UserApi();
        	$loginUid = $Api->login(I('username'),I('passwd'));
			if ($loginUid>0) {
			    o('User')->bindUser($bindUser, $loginUid);
				o('User')->setLogin($loginUid);
				session('bindUser',null);
				$this->success('验证成功！',U('Member/index'));
			}else {
			    $this->error('验证无效，请重新输入！');
			}
			exit;
		}
		$this->display();
	}
}