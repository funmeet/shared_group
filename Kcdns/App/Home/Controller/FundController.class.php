<?php
namespace Kcdns\Home\Controller;

/**
 * 基金超市控制器
 */
class FundController extends HomeController {
    
    protected $pageSize=10;
    
    public function _initialize() {
        parent::_initialize();
        if(IS_AJAX&&!o('site')->checkSiteToken()){
            exit('invalid request');
        }
        $this->assign('pageSize',$this->pageSize);
    }

    //基金超市
    public function index(){
        $this->meta_title="基金超市";
        $page=I('page',1)+0;
        $search=array();
        I('type')&&$search['fundTypeCode']=I('type')+0;
        I('company')&&$search['companyCode']=I('company')+0;
        if(IS_AJAX){
            //异步请求
            $data=o('Fund')->getFundList($page,$this->pageSize,$search);
            $this->ajaxReturn($data);
        }
        $data=o('Fund')->getFundList($page,$this->pageSize,$search);
        $fundCompany=o('Fund')->getFundCompany($companyCode='',1,100);
        $this->assign('fundList',$data['list']);
        $this->assign('fundCount',$data['count']);
        $this->assign('fundCompany',$fundCompany['list']);
        $this->assign('fundType',o('Fund')->getFundType());
        $this->display();
    }
    
    //基金详情
    public function detail(){
        $this->meta_title="基金详情";
        $code=I('get.code');
        $this->assign('fundInfo',o('Fund')->getFund($code));
        $this->assign('code',$code);
        $this->display();
    }
    //基金搜索
    public function search(){
        $page=I('page',1)+0;
        $keywords=substr(I('get.keywords',''),0,30);
        $type=I('type');
        $company=I('company');
        $searchWhere=array();
        $type&&$searchWhere['fundTypeCode']=$type;
        $company&&$searchWhere['companyCode']=$company;
        
        $fundCompany=o('Fund')->getFundCompany($companyCode='',1,100);
        $fundList=o('Fund')->searchFund(trim($keywords),$searchWhere,$page,$this->pageSize);
        if(IS_AJAX){
            $this->ajaxReturn($fundList);
        }else{
            $this->assign('fundList',$fundList['list']);
            $this->assign('fundCount',$fundList['count']);
            $this->assign('fundCompany',$fundCompany['list']);
            $this->assign('fundType',o('Fund')->getFundType());
            $this->display('Fund_index');
        }
    }
    
    //基金分红
    public function ajaxFundDivid(){
        $code=I('code');
        $info=o('Fund')->getFundDividend($code);
        $this->ajaxReturn(array(
            'data'=>$info['list'],
            'count'=>$info['count']
        ));
    }
    //基金拆分
    public function ajaxFundSplit(){
        $code=I('code');
        $info=o('Fund')->getFundShareSplit($code);
        $this->ajaxReturn(array(
            'count'=>$info['count'],
            'data'=>$info['list']
        ));
    }
    //申购费率
    public function ajaxFundChargeRate(){
        $code=I('code');
        $info=o('Fund')->getFundChargeRate($code);
        $this->ajaxReturn(array(
            'data'=>$info
        ));
    }
    //获取净值走势
    public function ajaxFundNetvalueTrend(){
        $code=I('code');
        $month=I('month',1);
        $info=o('Fund')->getFundNetvalueTrend($code,$month);
        $this->ajaxReturn(array(
            'data'=>$info
        ));
    }
    
    //获取基金历史净值走势
    public function ajaxFundHistoryNetvalue(){
        $code=I('code');
        $month=I('month',1);
        $info=o('Fund')->getFundNetvalueTrend($code,$month);
        $this->ajaxReturn(array(
            'data'=>$info
        ));
    }
    
    //获取货币型基金万份收益、7日年化收益、累计收益走势
    public function ajaxFundDailyProfitTrend(){
        $code=I('code');
        $month=I('month',1);
        $info=o('Fund')->getFundDailyProfitTrend($code,$month);
        $this->ajaxReturn(array(
            'data'=>$info
        ));
    }
    
    //获取资产配置
    public function ajaxFundAssetAllocation(){
        $code=I('code');
        $info=o('Fund')->getFundAssetAllocation($code);
        $this->ajaxReturn(array(
            'data'=>$info['list']
        ));
    }
    //获取行业投资
    public function ajaxFundIndustryInvest(){
        $code=I('code');
        $info=o('Fund')->getFundInvestIndustry($code);
        $this->ajaxReturn(array(
            'data'=>$info['list']
        ));
    }
    
    
    //获取基金公告
    public function ajaxFundNotice(){
        $code=I('code');
		$where=array(
			'InnerCode'=>$code
		);
        $info=o('Fund')->getFundNews('notice',1,6,$where);
        $this->ajaxReturn(array(
            'data'=>$info['list']
        ));
    }
    
    //获取基金经理
    public function ajaxFundManager(){
    	$code=I('code');
    	$info=o('Fund')->getFundManager($code);
        $this->ajaxReturn(array(
            'data'=>$info
        ));
    }
}
