<?php
namespace Kcdns\Home\Controller;

/**
 * 用户登陆状态控制器
 */
class AjaxUserInfoController extends HomeController {

	public function userlogin(){
		if (!O('Ucenter')->isLogin()){
			echo $this->fetch('AjaxUserInfo:isnotlogin');
		}else{
			$userinfo = O('Ucenter')->getUserInfo();
			if(!in_array(6,$userinfo['group'])){
				echo $this->fetch('AjaxUserInfo:notgroup6');
			}else{
				echo $this->fetch('AjaxUserInfo:islogin');
			}
		}
    }

}