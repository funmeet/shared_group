<?php
namespace Kcdns\Home\Controller;

use Common\Api\CategoryApi;

/**
 * 前台首页控制器
 */
class IndexController extends HomeController {

    protected function _initialize(){}
    
	//系统首页
    public function index(){
		//$condition['view'] = array( 'gt',10 );
		$condition['position'] = array( 'eq',1 );

		$list = OE('site')->getCache(600)->pageList(I('get.page'), 0, $condition);//getCache()->

		$Api = new CategoryApi();
		
		//将分类信息加入到文章列表中
		foreach ($list as $k => $v) {
			$cate = $Api->get_category( $v['category_id'] );
			$list[$k]['cate'] = array(
				'name'=>$cate['name'],
				'title'=>$cate['title']
			);
		}
        /* 模板赋值并渲染模板 */
		$this->assign('list', $list);

		if(IS_AJAX){
			$return['status'] = 1;
			/* 模板赋值并渲染模板 */
			$return['data'] = $this->fetch('indexajax');
			$this->ajaxReturn($return);
			exit;
		}
		$this->display();
    }
}
