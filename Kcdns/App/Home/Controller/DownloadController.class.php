<?php
namespace Kcdns\Home\Controller;

/**
 * 下载控制器
 */
class DownloadController extends HomeController {
    protected $pageSize=10;
    
    //下载中心
    public function index(){
        $this->meta_title='下载中心';
        $data=o("Site")->getDownloadList(o("Util")->getPageNo(),$this->pageSize);
        $this->assign('pageHtml',o('Util')->getPageHtml($data['count'],$this->pageSize,'/download-[PAGE]'));
        $this->assign('downloadList',$data['list']);
        $this->display();
    }
    
    public function download(){
        $fileCode=I('file');
        o('util')->download($fileCode);
        get_list_count();
    }
    
}