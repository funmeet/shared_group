<?php 
namespace Kcdns\Home\Controller;

class SearchController extends HomeController {
	
	const LIST_ROWS = 6;
	const MAX_PAGE=50;

        public function index()
	{
		$keyword = trimBlankChars(I('keyword'));
                !$keyword&&$this->error('请输入关键词!');
		$p = max(1,(int)I('p'));
		$p=min($p,  self::MAX_PAGE);
		
		$where=array(
			'model_id' => 2,
			'title' => array('like','%'.$keyword.'%'),
			'status' => 1,
		);
                $count=D('Document')->where($where)->count();
		$page_total=ceil($count/self::LIST_ROWS);
                $page_total > self::MAX_PAGE && $page_total=self::MAX_PAGE;
                $p>$page_total&&$p=$page_total;
		$list=D('Document')->where($where)->page($p,self::LIST_ROWS)->order('position desc,level desc,create_time desc,id desc')->select();			
		
		foreach($list as $key => $row){
			$article=D('document_article')->where(array('id' => $row['id']))->find();
			$row['content']=$article ? $article['content'] : '';
			$list[$key]=$row;
		}
		
		$this->assign('SEO',array(
		        'title'=>"搜索",'keyword'=>C('WEB_SITE_KEYWORD'),'description'=>C('WEB_SITE_DESCRIPTION')
		));
		$this->assign('p',$p);
		$this->assign('page_total',$page_total);
		$this->assign('keyword',$keyword);
		$this->assign('hidePosition', true);
		$this->assign('q', $q);
		$this->assign('count', $count);
		$this->assign('list', $list);
		$this->display();
	}
	
}