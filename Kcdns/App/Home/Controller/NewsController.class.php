<?php
namespace Kcdns\Home\Controller;

/**
 * 公告资讯控制器
 */
class NewsController extends HomeController {
    
    protected $pageSize=10;
    
    protected $news_category=array('news','notice','data','exchange','publish','special');

    //资讯公告
    public function index(){
        $this->meta_title='公告资讯';
        
        $type=I('get.type','news');
        if(!in_array($type,$this->news_category))$type=$this->news_category[0];
        
        $newsList=o('Fund')->getFundNews($type,o("Util")->getPageNo(),$this->pageSize);
        
        $this->assign('fundNewsType',o('Fund')->getFundNewsType());
        $this->assign('pageHtml',o('Util')->getPageHtml($newsList['count'],$this->pageSize,'/news-'.$type.'-[PAGE]'));
        $this->assign('fundNews',$newsList['list']);
        $this->assign('type',$type);
        $this->display();
    }
    
    //资讯详情
    public function detail(){
        $news=o('Fund')->getFundNewsDetail(I('get.id',0),I('get.type'));
        $this->assign('fundNewsType',o('Fund')->getFundNewsType());
        $this->assign('type',I('get.type'));
        $this->assign('newsDetail',$news);
        $this->display();
    }
    
}