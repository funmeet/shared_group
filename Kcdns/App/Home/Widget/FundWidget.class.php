<?php
namespace Kcdns\Home\Widget;
use Think\Controller;

/**
 * 基金推荐等相关挂件
 */

class FundWidget extends Controller{
	
	
    //基金推荐
    public function recommend(){
        $recommendList=o('Fund')->getRecommendFund();
        $this->assign('recommendList',$recommendList);
        $this->display('Widget:recommend');
    }
	
}
