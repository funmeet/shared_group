<?php
namespace Kcdns\Home\Widget;
use Think\Controller;

/**
 * 活跃作家
 */

class WriterWidget extends Controller{
	
	
    //活跃作家
    public function getactive(){

		$cha = time()-86400*(APP_DEBUG?30:7);
		$opt['create_time'] = array('gt',$cha);
		$opt['status'] = 1;
		$cachekey = 'WriterWidget/getactive';
		if (!$list = S($cachekey)) {
			$active = D('Document')->where($opt)->field(array("count(id)"=>"hy", "uid", "create_time"))->group('uid')->order('hy desc,create_time desc')->limit(6)->select();
			if (!$active) {
				return false;
			}
			$uids = array_column($active, 'uid'); 

			$writers = D('Member')->where(
					array('uid'=>array('in',implode(',',$uids)))
				)->field(true)->select();

			$tmplist = array_combine(array_column($writers, 'uid'),$writers);

			foreach ($active as $k=>$v ) {
				if ( array_key_exists($v['uid'],$tmplist)) {
					$list[] = $tmplist[$v['uid']];
				}
			}
			S($cachekey, $list, array('expire'=>3600));
		}
        $this->assign('active',$list);
        $this->display('Widget:writer');
    }

	//最近登录作家
    public function getRecentlogin(){
        $Member = D('Member');

		$cha = time()-86400*7;
		$map['last_login_time']  = array('gt',$cha);
		$map['status']  = array('eq',1);
		$active = $Member->where($map)->order('last_login_time desc')->limit(100)->select();
		
		$uids = array_column($active, 'uid'); 

		$iswriter = M('auth_group_access')->where(
				array('uid'=>array('in',implode(',',$uids)),'group_id'=>6 )
			)->field('uid')->select();

		$iswriterUids = array_flip(array_column($iswriter, 'uid')); 

		foreach ($active as $k=>$v ) {
		    if ( array_key_exists($v['uid'],$iswriterUids)) {
		        $list[] = $v;
		    }
			if (count($list)==5) {
			    break;
			}
		}
        $this->assign('active',$list);
        $this->display('Widget:recent');
    }

	//栏目相关作家
    public function getRelevant($cate_id){
        $Member = D('Member');
		
		$map['cate_auth']  = array('like','%'.$cate_id.'%');
		$map['status']  = array('eq',1);
		$active = $Member->where($map)->order('last_login_time desc')->limit(5)->select();
		
		$uids = array_column($active, 'uid'); 

		$iswriter = M('auth_group_access')->where(
				array('uid'=>array('in',implode(',',$uids)),'group_id'=>6 )
			)->field('uid')->select();

		$iswriterUids = array_flip(array_column($iswriter, 'uid')); 

		foreach ($active as $k=>$v ) {
		    if ( array_key_exists($v['uid'],$iswriterUids)) {
		        $list[] = $v;
		    }
			if (count($list)==5) {
			    break;
			}
		}
        $this->assign('active',$list);
        $this->display('Widget:relevant');
    }

	//详情页作家文章列表
    public function getwlist($uid = ''){

		$condition = array(
			'addfield'=>'uuid',
			'limit'=>'3',
			'where'=>array('uid'=>$uid)
		);
		$list = OE('site')->getCache()->articleList($condition);


		$map3 = array('uid' => $uid , 'status' => 1);
		$writer = D('Member')->where($map3)->find();

        $this->assign('list',$list);
		$this->assign('writer',$writer);
        $this->display('Widget:getwlist');
    }
	
}
