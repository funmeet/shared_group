<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Kcdns\User\Controller;

class PublicController extends BaseController
{

    protected $forceLogin = false;

    public function login ()
    {}

    public function ajaxLogin ()
    {
        $code = I('get.code');
        $mobile = I('get.mobile');
        
        OE('sms')->check($mobile, $code);
        $userinfo = OE('user')->loginWithUsername($mobile);
        $oauthinfo = O('oauth')->current() and O('user')->bindOauth($oauthinfo);
        
        $this->ajaxReturn(true, null, '登录成功');
    }
}
