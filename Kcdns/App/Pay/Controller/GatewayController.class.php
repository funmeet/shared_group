<?php
namespace Kcdns\Pay\Controller;

/**
 * 支付控制器
 */
class GatewayController extends BaseController {

    //同步回调处理
    public function callback(){
         if($syncInfo=O('Pay')->sync(I('get.paysn'))){
             $this->redirect($syncInfo['redirect']);
         }else{
             exit('同步通知异常');
             $this->error('支付失败');
         }
    }

    //异步回调处理
    public function notify(){
        echo O('Pay')->async(I('get.paysn'));
    }


    //退款同步回调(本地模拟同步回调) 真实退款 是由服务端发起 所以并没有支付层面的同步回调 为了保证业务流程处理一致  同步退款完毕后 可以执行一次同步回调 保证业务流程完成
    public function rcallback(){
        echo O('Pay')->syncRefund(I('get.paysn'));
    }
    //退款异步回调
    public function rnotify(){
        echo O('Pay')->asyncRefund(I('get.paysn'));
    }

}
