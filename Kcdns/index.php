<?php
// +-----------------------------------------------------------------------------------------------------------------
// | 
// +-----------------------------------------------------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-----------------------------------------------------------------------------------------------------------------

/**
 * ------------------------------------------------------------------------------------------------------------------
 * 路径常量配置
 * ------------------------------------------------------------------------------------------------------------------
 */

// KCDNS 核心包路径常量
define('KC_ROOT_PATH', dirname(__FILE__) . '/');
define('KC_APP_PATH', KC_ROOT_PATH . 'App/');
define('COMMON_PATH', KC_APP_PATH . 'Common/');

// ThinkPHP 路径常量
define('APP_PATH', ROOT_PATH . 'App/');
define('LOG_PATH', ROOT_PATH . 'Data/Logs/');
define('RUNTIME_PATH', ROOT_PATH . 'Data/Runtime/');

/**
 * ------------------------------------------------------------------------------------------------------------------
 * 加载环境配置
 * 配置项转换为常量
 * 优先级 : config.php, config_dev.php(开发), config_product.php(生产)
 * ------------------------------------------------------------------------------------------------------------------
 */
$config = [];
$configList = [ KC_ROOT_PATH . 'config.php', ROOT_PATH . 'config.php', ROOT_PATH . 'config.local.php'];
foreach ($configList as $configFile) file_exists($configFile) and $config = array_merge($config, require $configFile);
foreach ($config as $_const => $_value) defined($_const) or define($_const, $_value);

/**
 * ------------------------------------------------------------------------------------------------------------------
 * 修正环境变量
 * ------------------------------------------------------------------------------------------------------------------
 */
 
// 去掉端口号
@$_SERVER['HTTP_HOST'] = substr($_SERVER['HTTP_HOST'], 0, strpos($_SERVER['HTTP_HOST'] . ':', ':'));

// HTTPS 检测
@$_SERVER['HTTPS'] = 0;

/**
 * ------------------------------------------------------------------------------------------------------------------
 * 初始化日志
 * ------------------------------------------------------------------------------------------------------------------
 */
require KC_ROOT_PATH . 'Service/Util/Kcslog.class.php';
KCSLog::init(array( 'LOG_PATH' => LOG_PATH, 'LOG_LEVEL' => LOG_LEVEL, 'PROFILE_TIME_LIMIT' => LOG_TIME_LIMIT ));
KCSLog::INFO(implode("\n                   ", [
        'Get      : ' . json_encode($_GET),
        'Post     : ' . json_encode($_POST),
        'Cookie   : ' . json_encode($_COOKIE),
        'Referer  : ' . (@isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '')
]), false, false);

/**
 * ------------------------------------------------------------------------------------------------------------------
 * 启动
 * ------------------------------------------------------------------------------------------------------------------
 */
require KC_ROOT_PATH . 'Think/ThinkPHP.php';