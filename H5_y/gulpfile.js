var gulp = require("gulp"),
	uglify = require("gulp-uglify"),
	cleanCss = require("gulp-clean-css"),
	rev = require('gulp-rev'),
	revCollector = require("gulp-rev-collector"),
	minifyHTML = require("gulp-minify-html"),
	del = require('del');

//删除dist目录下的全部内容
gulp.task('clean:dist', function(){
	return del(['dist/**/*', '!dist/favicon.ico', '!dist/favicon.png', '!dist/titleicon.png']);
});

//拷贝css文件
gulp.task('copy:css' ,[ 'clean:dist' ], function(){
	//更名css	
	return gulp.src(["**/*.css", '!node_modules/**/*.css'])
				.pipe(cleanCss())
				.pipe(rev())  //set hash key
				.pipe(gulp.dest("dist"))
				.pipe(rev.manifest()) //set hash key json	
				.pipe(gulp.dest("dist/rev/css"));
});

//拷贝js文件
gulp.task('copy:js' ,[ 'clean:dist' ], function(){
		//script目录下的js
	return gulp.src(["**/*.js", "!gulpfile.js", '!node_modules/**/*.js'])
				.pipe(rev())  //set hash key
				.pipe(gulp.dest("dist"))
				.pipe(rev.manifest()) //set hash key json
				.pipe(gulp.dest("dist/rev/js")); //dest hash key json
});

//拷贝html文件
gulp.task('copy:html' ,[ 'clean:dist', 'copy:css', 'copy:js'], function(){
	return 	gulp.src(['dist/rev/**/*.json', '**/*.html', '!node_modules/**/*.html', '!_html/*.html'])
		        .pipe( revCollector({
		            replaceReved: true
		        }) )
		        .pipe( gulp.dest('dist') );
});

gulp.task('test', ['clean:dist' , 'copy:css', 'copy:js', 'copy:html' ], function(){
	//拷贝不用压缩的css
	gulp.src("static/css/*.min.css")
	.pipe(gulp.dest("dist/static/css")); 
    //不更名的js
	gulp.src('static/js/avalon.mobile.js')
	.pipe(uglify())
	.pipe(gulp.dest("dist/static/js"));

	gulp.src("static/js/*.min.js")
	.pipe(gulp.dest("dist/static/js"));
	//其他
	gulp.src(["static/**", "!static/css/*.css", "!static/js/*.js"])
	.pipe(gulp.dest("dist/static"));
})

gulp.task("dist", ['test'], function(){
	//压缩js
	gulp.src( ['dist/**/*.js', "!js/*.min.js"]).pipe(uglify()).pipe( gulp.dest('dist') );
	//压缩html
	gulp.src( 'dist/**/*.html').pipe( minifyHTML( { empty:true, spare:true }) ).pipe( gulp.dest('dist') );
});

