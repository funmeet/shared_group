var Mall = {};
Mall.LOCK = false;
Mall.host = simpleStorage.get( "host" ) || "http://"+location.hostname;
Mall.request = function( url, data){
	var token = simpleStorage.get( "token" );
	var config = {
		url: Mall.host+url,
		dataType: "json",
		type: "GET",	
	}
	if( data ){
		config.data = data;
		config.type = "POST";
	}
	if( token ){
		config.headers = {
			'API-TOKEN': token
		}
	}
	return $.ajax( config )
		.always(function( data ){
			if( data.error_code == 20000 ){
				window.location.href = "/H5/login?toUrl="+window.location.href;
			}
			if( data.data && data.data.list && data.data.list.length == 0 && !data.data.question ){
				if( !$( ".empty" ).length ){
					$( ".content" ).append( "<div class='empty' style='background:url(./images/wsj.png) center center no-repeat; top: 0; background-size: 80%; position: absolute; width: 100%; height:100%'><div>" )
				}
			}
			else{
				if( $( ".empty" ).length ){
					$( ".empty" ).remove();
				}
			}
		})
}

var clientWidth;
if(/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {
    clientWidth = document.documentElement.clientWidth;
} else {
    $( ".page" ).css( {"width": "540px", "margin": "0 auto", "overflow": "visible"} );
    $( "body" ).css( {"font-size": "16px", "overflow": "visible" } )
    $( ".page-group" ).css( "overflow", "visible" )
    clientWidth = $( ".page" ).width();
}
Mall.mobile = function( v ){
	if( !v ){
		return null
	}
	else{
		return /^(((13[0-9]{1})|(17[0-9]{1})|(15[0-9]{1})|(18[0-9]{1})|(14[0-9]{1}))+\d{8})$/.test(v)
	}
}
Mall.email = function( v ){
	if( !v ){
		return null
	}
	else{
		return /^([A-Z0-9]+[_|\_|\.]?)*[A-Z0-9]+@([A-Z0-9]+[_|\_|\.]?)*[A-Z0-9]+\.[A-Z]{2,3}$/i.test(v)
	}
}
Mall.idCard = function( val ){
        if ((/^\d{15}$/).test(val)) {
            return true;
        }
        else if((/^\d{17}[0-9xX]$/).test(val)){
            var vs = "1,0,x,9,8,7,6,5,4,3,2".split(","),
                ps = "7,9,10,5,8,4,2,1,6,3,7,9,10,5,8,4,2".split(","),
                ss = val.toLowerCase().split(""),
                r = 0;
            for (var i = 0; i < 17; i++) {
                r += ps[i] * ss[i];
            }
            return (vs[r % 11] == ss[17]);
        }
}
Mall.parseUrl = function( url ){
	var params = {};
	if( url.indexOf( "?" )>-1 ){
		if( url.indexOf( "&" )==-1 ){
			url = url.split("?")[1];
			params[url.split("=")[0]] = url.split("=")[1];
		}
		else{
			url = url.split("?")[1];
			var urls = url.split("&");
			urls.map( function( k, v ){
				params[k.split("=")[0]] = k.split("=")[1];
			} )
		}
	}
	else{
		return false
	}
	return params
}
Mall.checkLogin = function(){
	var token = simpleStorage.get( "token" ),
		expire_time = simpleStorage.get( "expire_time" );
	if( ( !token || new Date().getTime() > expire_time ) && window.location.href.indexOf("/H5/login") == -1 ){
		if( window.location.href.indexOf("/H5/register") == -1 ){
			window.location.href = "/H5/login?toUrl="+window.location.href;
		}
	}
	Mall.request( "/api/SiteConfig" )
	    .always( function( config ){
	        if( config.error_code == 0 ){
	            simpleStorage.set("open", config.data.site.open);
	            simpleStorage.set("min_money", config.data.draw_money.min_money);
	            simpleStorage.set("fee_percent", config.data.draw_money.fee_percent);
	            simpleStorage.set("money_multiple", config.data.draw_money.money_multiple);
	            if( avalon.vmodels["custom"] ){
	            	avalon.vmodels["custom"].data = config.data;
	            }
	            if( avalon.vmodels["index"] ){
	            	$( "footer p" ).html( config.data.site.copyright );
	            	avalon.vmodels["index"].data = config.data.site;
	            }
	            if( config.data.site.open == 0 ){
	            	window.location.href = "/H5/updata"
	            }
	        }
	        else{
	            $.alert(config.error_msg)
	        }
	    } )
}
Mall.checkLogin();