<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\User;

class Notice extends \Service\Common\BaseModel
{
    protected $name = 'notice';

    CONST TYPE_CUSTOMER_SERVICE = 0;//客服回答
    CONST TYPE_ORDER_EXPRESS = 1;//订单已发货
    CONST TYPE_ORDER_FINANCE = 2;//财务中心

    const TYPE_LABEL = [
        self::TYPE_CUSTOMER_SERVICE => '互动问答',
        self::TYPE_ORDER_EXPRESS => '交易物流',
        self::TYPE_ORDER_FINANCE => '财务中心',
    ];

    //通知列表
    public function noticeList($uid, $type)
    {
        //通知列表
        $list = (array)$this->where([
            'uid' => $uid,
            'type' => $type,
        ])->order('id desc')->page(1, 50)->select();

        //标记已读
        $where = [
            'uid' => $uid,
            'type' => $type,
            'is_read' => 0
        ];
        $unread_num = $this->where($where)->count();
        if ($unread_num) {
            M('user')->where([
                'uid' => $uid
            ])->save([
                'total_unread' => ['exp', "total_unread-$unread_num"]
            ]);

            $this->where($where)->save([
                'is_read' => 1
            ]);
        }

        return $this->format($list);
    }

    public function format($data)
    {
        $type_label = self::TYPE_LABEL;
        return $this->_format($data, function (&$row) use ($type_label) {
            $row['type_label'] = $type_label[$row['type']];
        });
    }

    //消息中心
    public function noticeCenter($uid)
    {
        $sub_query = $this
            ->field('max(id) id')
            ->where([
                'uid' => $uid
            ])
            ->group('type')
            ->select(false);
        $list = (array)$this
            ->table("$sub_query a")
            ->join('__NOTICE__ b on b.id=a.id')
            ->order('a.id desc')
            ->select();
        foreach ($list as &$row) {
            $row['type_label'] = self::TYPE_LABEL[$row['type']];
        }
        return $list;
    }

    //提现成功通知
    public function addDrawMoney($draw)
    {
        $content = "您的提现{$draw['draw_money']}元已发放";
        $this->_add($draw['uid'], self::TYPE_ORDER_FINANCE, $content, $draw['draw_id']);
    }

    //汇款成功通知
    public function addRemit($remit)
    {
        $content = "平台已收到您的{$remit['receive_money']}元汇款";
        $this->_add($remit['uid'], self::TYPE_ORDER_FINANCE, $content, $remit['id']);
    }

    //转账收款通知
    public function addReceiveTransfer($transfer)
    {
        $content = "收到来自[{$transfer['transfer_name']}]的{$transfer['transfer_money']}元转账";
        $this->_add($transfer['uid'], self::TYPE_ORDER_FINANCE, $content, $transfer['transfer_id']);
    }

    //收到代付通知
    public function receiveInvitePay($order)
    {
        $content = "收到[{$order['from_name']}]的代付邀请";
        $this->_add($order['uid'], self::TYPE_ORDER_FINANCE, $content, $order['order_no']);
    }

    //代付成功通知
    public function invitePay($order)
    {
        $content = "您的订单[{$order['goods_title']}]代付成功";
        $this->_add($order['uid'], self::TYPE_ORDER_FINANCE, $content, $order['order_no']);
    }

    //客服通知
    public function addCustomerService($question)
    {
        $content = "您的提问[{$question['title']}]有新的回复了";
        $this->_add($question['uid'], self::TYPE_CUSTOMER_SERVICE, $content, $question['id']);
    }

    //订单发货
    public function addOrderExpress($order)
    {
        $content = "您的订单[{$order['order_no']}]已发货";
        $this->_add($order['uid'], self::TYPE_ORDER_EXPRESS, $content, $order['order_no']);
    }

    public function _add($uid, $type, $content, $type_id)
    {
        $insert_id = $this->add([
            'create_time' => date('Y-m-d H:i:s'),
            'uid' => $uid,
            'type' => $type,
            'type_id' => $type_id,
            'content' => $content,
            'is_read' => 0,
        ]);
        if (!$insert_id) {
            throw new \Exception("插入通知失败");
        }

        $update = M('user')->where(['uid' => $uid])->save([
            'total_unread' => ['exp', "total_unread+1"]
        ]);
        if ($update === false) {
            throw new \Exception("累计未读消息数失败");
        }
    }
}