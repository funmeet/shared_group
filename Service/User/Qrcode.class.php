<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\User;

class Qrcode extends \Service\Common\BaseModel
{
    protected $name = 'pic';
    
    public function getUserQrcodeUrl($uid)
    {
    	$qrcode_pic = M('user')->where(['uid'=>$uid])->getField('qrcode_pic');
    	if ($qrcode_pic){
	    	$qrcode_url = $this->alias('a')
	    	                   ->join('__PICTURE__ b on b.id = a.pic')
	    	                   ->where(['a.id' => $qrcode_pic])
	    	                   ->getField('b.url');
    	}else{
    		$qrcode_url = './images/dd.png';
    	}
    	return $qrcode_url;
    }
    
    public function getUserQrcode($uid,$id)
    {
    	$result = $this->where(['id'=>$id])
    	               ->find();
    	$result['qrcode_url'] = M('picture')->where(['id'=>$result['pic']])
    	                                    ->getField('url');
    	M('user')->where(['uid'=>$uid])
    	         ->save([
    	           'qrcode_pic' => $result['id']
    	         ]);
    	return $result;
    }

	public function getQrcode()
	{
		$where['status'] = 0;
		$result = $this->where($where)
		               ->select();
		if ($result) {
			foreach ($result as $k=>$v){
				$result[$k]['qrcode_url'] = M('picture')->where(['id'=>$v['pic']])
				                                        ->getField('url');
			}
		}
		return $result;
	}

}