<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\User;

class BalanceTransfer extends \Service\Common\BaseModel
{
    protected $name = 'balance_transfer';

    //转账记录
    public function transferList($uid, $page_num = 1, $page_size = 5)
    {
        $where = [
            'uid' => $uid,
            'to_uid' => $uid,
            '_logic' => 'or'
        ];
        $list = (array)$this->where($where)->order('id desc')->page($page_num, $page_size)->select();
        foreach ($list as &$row) {
            if ($row['uid'] == $uid) {
                $title = "转出给{$row['mobile']}({$row['real_name']})";
                $status = 0;
            } else {
                $user = O('user')->getUserInfo($row['to_uid']);
                $title = "{$user['mobile']}({$user['real_name']})转入";
                $status = 1;
            }
            $row['title'] = $title;
            $row['status'] = $status;
        }
        $count = $this->where($where)->count();
        return [
            $list,
            $page_count = $count,
            $page_total = ceil($count / $page_size),
            $total_transfer_out = get_decimal($this->where($where)->sum('money')),
            $total_transfer_in = get_decimal($this->where(['to_uid' => $uid])->sum('money')),
        ];
    }

    //转账提交
    public function transferSubmit($data, $login_info)
    {
        //验证支付密码
        OE('user')->checkpaypassword($login_info['uid'], $data['pay_password']);

        //收款人验证
        $user = M('user')->where([
            'mobile' => $data['mobile'],
            'nickname' => $data['nickname']
        ])->find();
        if (!$user) {
            throw new \Exception("没有找到该用户");
        }
        if ($user['uid'] == $login_info['uid']) {
            throw new \Exception("不能转给自己");
        }

        //金额验证
        if (!($data['money'] && $login_info['balance'] >= $data['money'])) {
            throw new \Exception("余额不足");
        }

        try {
            $this->startTrans();
            //转账记录
            $insert_id = $this->add([
                'uid' => $login_info['uid'],
                'to_uid' => $user['uid'],
                'money' => $data['money'],
                'real_name' => $data['nickname'],
                'mobile' => $data['mobile'],
                'create_time' => date('Y-m-d H:i:s'),
            ]);
            if (!$insert_id) {
                throw new \Exception("转账记录插入失败");
            }
            //资金记录
            OE('user')->transfer($login_info['uid'], $user['uid'], $data['money'], $insert_id);
            $this->commit();
        } catch (\Exception $e) {
            $this->rollback();
            throw new \Exception($e->getMessage());
        }
    }


}