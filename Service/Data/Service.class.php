<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Data;

use Service\Common\BaseService;

class Service extends BaseService
{
    public function getClientList($start_time = '', $end_time = '', $company_id = '')
    {
        return User::getInstance()->getClientList($start_time, $end_time, $company_id);
    }

    public function getCouponList($start_time = '', $end_time = '', $company_id = '')
    {
        return Coupon::getInstance()->getCouponList($start_time, $end_time, $company_id);
    }
    
    public function getPacketsList($start_time = '', $end_time = '')
    {
    	return Packets::getInstance()->getPacketsList($start_time, $end_time);
    }
    
    public function explodePackets($list = [])
    {
    	return Packets::getInstance()->explodeAgent($list);
    }
    
    public function count($start_time = '',$end_time = '')
    {
    	return Count::getInstance()->count($start_time,$end_time);
    }
    
    public function getTeam($start_time = '',$end_time = '', $mobile = '')
    {
    	return Count::getInstance()->getTeam($start_time,$end_time, $mobile );
    }
}