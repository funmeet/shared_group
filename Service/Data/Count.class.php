<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Data;

use Kcdns\Admin\Controller\PublicController;
class Count extends User
{
    protected $name = 'order';
    
    public function count($start_time = '',$end_time = '')
    {
    	$total_order_money = $this->getTotalOrderMoney($start_time ,$end_time);//总订单金额
    	$total_commisson_money = $this->getCommissonMoney($start_time ,$end_time);//佣金总额
    	$yjs_commisson_money = $this->getCommissonMoney($start_time ,$end_time,1);//已结算佣金
    	$wjs_commisson_money = $this->getCommissonMoney($start_time ,$end_time,0);//未结算佣金
    	$total_red_packets = O('packets')->getTotalPacketsQuota('',$start_time ,$end_time);//领取的红包金额
    	$total_cash = $this->getCash($start_time ,$end_time);
    	$yf_total_cash = $this->getCash($start_time ,$end_time,1);
    	$wf_total_cash = $total_cash - $yf_total_cash;
    	return [
    	     'total_order_money'=>$this->formatNum($total_order_money),
    	     'total_commisson_money'=>$this->formatNum($total_commisson_money),
    	     'yjs_commisson_money'=>$this->formatNum($yjs_commisson_money),
    	     'wjs_commisson_money'=>$this->formatNum($wjs_commisson_money),
    	     'total_red_packets'=>$this->formatNum($total_red_packets),
    	     'total_cash'=>$this->formatNum($total_cash),
    	     'yf_total_cash'=>$this->formatNum($yf_total_cash),
    	     'wf_total_cash'=>$this->formatNum($wf_total_cash),
    	];
    }
    
    public function getCash($start_time = '',$end_time = '',$status = '')
    {
    	$where = [];
    	if ($status !== '') {
    		$where['status'] = $status;
    	}
    	if ($start_time || $end_time) {
    		if ($start_time) {
    			$where['create_time'][] = ['egt', $start_time . ' 00:00:00'];
    		}
    		if ($end_time) {
    			$where['create_time'][] = ['elt', $end_time . ' 23:59:59'];
    		}
    	}
    	return  M('draw_cash')->where($where)->sum('draw_money');
    }
    
    public function getTotalOrderMoney($start_time = '',$end_time = '')
    {
    	$where['pay_status'] = 1;
    	if ($start_time || $end_time) {
    		if ($start_time) {
    			$where['pay_time'][] = ['egt', $start_time . ' 00:00:00'];
    		}
    		if ($end_time) {
    			$where['pay_time'][] = ['elt', $end_time . ' 23:59:59'];
    		}
    	}
    	
    	return  M('order')->where($where)->sum('pay_money');
    }
    
    public function getCommissonMoney($start_time = '' ,$end_time = '',$settle_status = '')
    {
    	$where = [];
    	if ($settle_status !== '') {
    		$where['settle_status'] = $settle_status;
    	}
    	
    	if ($start_time || $end_time) {
    		if ($start_time) {
    			$where['create_time'][] = ['egt', $start_time . ' 00:00:00'];
    		}
    		if ($end_time) {
    			$where['create_time'][] = ['elt', $end_time . ' 23:59:59'];
    		}
    	}
    	 
    	return  M('commission_log')->where($where)->sum('money');
    }
    
    
    
    public function getTeam($start_time = '',$end_time = '', $mobile = '')
    {
    	$where = [];
    	$cond = [];
    	if ($mobile === '') {
    	   return '';
    	}
    	$where['b.mobile'] = $mobile;
    	$cond['b.mobile'] = $mobile;
    	if ($start_time || $end_time) {
    		if ($start_time) {
    			$where['a.create_time'][] = ['egt', $start_time . ' 00:00:00'];
    			$cond['a.settle_time'][] = ['egt', $start_time . ' 00:00:00'];
    		}
    		if ($end_time) {
    			$where['a.create_time'][] = ['elt', $end_time . ' 23:59:59'];
    			$cond['a.settle_time'][] = ['elt', $end_time . ' 23:59:59'];
    		}
    	}
    	$total_performance = M('team_log')->alias('a')
    	                                  ->join('__USER__ b on b.uid = a.uid')
								    	  ->where($where)
								    	  ->sum('a.performance')?:0;
    	$cond['a.settle_status'] = 1;
    	$commission = M('commission_log')->alias('a')
    	                                 ->join('__USER__ b on b.uid = a.uid')
    	                                 ->where($cond)
    	                                 ->sum('money')?:0;
    	
    	return [
    	   $this->formatNum($total_performance),
    	   $this->formatNum($commission),
    	];
    }
    
    public function formatNum($num)
    {
    	return  number_format($num, 2, '.', '');
    }
    
}