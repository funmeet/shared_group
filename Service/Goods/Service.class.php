<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Goods;

use Service\Common\BaseService;

class Service extends BaseService
{
	
	public function getTypeList()
	{
		return Goods::getInstance()->getTypeList();
	}
	
    public function getDetail($goods_id, $uid = 0)
    {
        return Goods::getInstance()->getDetail($goods_id, $uid);
    }
    
    public function getIndexList($uid = 0, $is_vip = 0)
    {
    	return Goods::getInstance()->getIndexList($uid, $is_vip);
    }

    public function getGroupList($page_size, $uid = 0, $is_vip = 0)
    {
        return Goods::getInstance()->getGroupList($page_size, $uid, $is_vip);
    }
    
    public function getGroupList2($page_size, $uid = 0,$poster_id,$order = 'id',$mode = 'desc')
    {
    	return Goods::getInstance()->getGroupList2($page_size, $uid,$poster_id,$order,$mode);
    }

    public function getGroupList3($page_size, $uid = 0,$poster_id,$order = 'id',$mode = 'desc')
    {
        return Goods::getInstance()->getGroupList3($page_size, $uid,$poster_id,$order,$mode);
    }

    public static function getSelectCategory($type = 0)
    {
        if (ACTION_NAME == 'index') {
            return M('poster')->where(['id' => $type])->getField('title');
        } else {
            return M('poster')->where(['category' => 'goods_category'])->order('level asc,id asc')->getField('id,title', true);
        }
    }

    public function search($keyword)
    {
        return Goods::getInstance()->search($keyword);
    }
}