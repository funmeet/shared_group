<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Spec;

use Service\Common\BaseService;

class Service extends BaseService
{
	

    public static function getSelectGoods($type = 0)
    {
        if (ACTION_NAME == 'index') {
            return M('goods')->where(['id' => $type])->getField('title');
        } else {
            return M('goods')->order('id asc')->getField('id,title', true);
        }
    }

    public function search($keyword)
    {
        return Goods::getInstance()->search($keyword);
    }
}