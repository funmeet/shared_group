<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api;

class Doc
{

    /**
     * 生成接口测试工具
     *
     * @param unknown $urlBase
     *            接口路径
     * @param unknown $hosts
     *            域名列表
     * @param string $version
     *            接口版本
     *
     * @return string $docHtml
     */
    public static function html($urlBase, $hosts = [], $version = 'V1')
    {
        $apiItem = [
            'hosts' => $hosts,
            'urlBase' => $urlBase,
            'api' => [],
            'code' => Code::$$version
        ];

        $apiDir = __DIR__ . "/$version/";
        $apiSuffix = ".class.php";
        foreach (glob("{$apiDir}*{$apiSuffix}") as $docFile) {
            $files[] = $docFile;
        }

        rsort($files);

        foreach ($files as $docFile) {
            $apiName = substr($docFile, strlen($apiDir), -strlen($apiSuffix));
            $className = "\\Service\\Api\\$version\\$apiName";
            $apiInstance = new $className();

            $row = [];
            $row['input'] = $apiInstance->input;
            $row['output'] = $apiInstance->output;
            $row['title'] = $apiInstance->title;
            $row['desc'] = str_ireplace('[host]', $_SERVER['HTTP_HOST'], !$apiInstance->desc ? '' : $apiInstance->desc);
            $row['group'] = $apiInstance->group;
            $row['apiName'] = $apiName;

            $key = $apiInstance->group;
            $apiItem['api'][$key][] = $row;
        }

        require __DIR__ . "/Doc/{$version}.html";
    }

    protected static function _loadStatic($file)
    {
        $tag = substr($file, '-3') == ".js" ? "script" : "style";
        echo "<$tag>" . file_get_contents(__DIR__ . "/Doc/Static/$file") . "</$tag>";
    }
}