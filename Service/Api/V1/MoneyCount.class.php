<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class MoneyCount
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "资金统计";
    public $group = '财务';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'total' => [
            'balance' => 'label=余额;',
            'frozen_money' => 'label=冻结金额;',
        ],
        'income' => [
            'total' => 'label=收入总计;',
            'remit' => 'label=汇款充值;',
            'transfer_in' => 'label=会员转入;',
            'commission' => 'label=佣金分红;',
            'invite_pay' => 'label=邀请代付;',
        ],
        'expense' => [
            'total' => 'label=支出总计;',
            'transfer_out' => 'label=余额转出;',
            'draw_cash' => 'label=余额提现;',
            'order' => 'label=消费购物;',
            'pay_invite' => 'label=订单代付;',
        ]
    ];

    public function run($param, $uid, $login_info)
    {
        return [
            'total' => [
                'balance' => $login_info['balance'],
                'frozen_money' => $login_info['frozen_money'],
            ],
            'income' => [
                'total' => $login_info['total_income'],
                'remit' => get_decimal(M('money_log')->where(['type' => 1, 'uid' => $uid])->sum('change_money')),
                'transfer_in' => $login_info['total_transfer_in'],
                'commission' => get_decimal(M('commission_log')->where(['uid' => $uid, 'settle_status' => 1])->sum('money')),
                'invite_pay' => $login_info['total_invite_fee'],
            ],
            'expense' => [
                'total' => $login_info['total_expense'],
                'transfer_out' => $login_info['total_transfer_out'],
                'draw_cash' => $login_info['total_draw_cash'],
                'order' => $login_info['total_buy'],
                'pay_invite' => $login_info['total_invite_pay'],
            ]
        ];
    }
}
