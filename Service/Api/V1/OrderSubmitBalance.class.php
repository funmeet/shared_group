<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderSubmitBalance
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "普通下单";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'goods[]' => 'required;int;label=商品id->购买数量;comment=goods[goods_id]＝buy_num&goods[goods_id]＝buy_num',
        'spec[]' => 'string;label=商品id->规格;comment=spec[goods_id]＝spec_value&spec[goods_id]＝spec_value',
        'address_id' => 'required;int;label=地址id',
        'red_packets' => 'label=使用红包金额;comment=0或不填则不使用红包',
        'phone' => 'required;mobile;label=手机号',
        'remark' => 'label=备注',
        'spec' => '规格'
    ];

    public $output = [
        'order_no' => 'required;bigint;label=订单号;comment=用于支付订单',
    ];

    public function run($param, $uid)
    {
        OE('user')->chenckmMobile($param['phone'],$uid);
        $order_no = OE('order')->submit($param, $uid);
        return [
            'order_no' => $order_no
        ];
    }
}
