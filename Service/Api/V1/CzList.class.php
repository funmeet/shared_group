<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CzList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "充值";
    public $group = '用户';
    public $desc = "";

    public $input = [

    ];

    public $output = [

    ];

    const type_label = [
        0 => '支付宝',
        1 => '微信',
        2 => '银行汇款',
    ];
    const status_label = [
        0 => '提交中',
        1 => '充值成功',
        2 => '驳回',
    ];

    public function run($param, $uid, $login_info)
    {
//        var_dump($param);exit();

        $ret = M('remit')->where([
            'uid' => $uid,
        ])->select();
       foreach($ret as &$item){
           $item['type_label'] = self::type_label[$item['type']];
           $item['status_label'] = self::status_label[$item['status']];
       }
//        OE('address')->submit($param, $uid);
        return ['list' => $ret];
    }
}