<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CommissionBonusList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "分红佣金";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'level' => 'required;enum=2,14,15,16,17;label=分红级别;comment=2服务佣金 14皇冠会员 15皇冠大使会员 16至尊会员 17五星',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_bonus' => 'label=总额;',
        'list' => [
            [
                'title' => 'label=时间;',
                'money' => 'label=金额;',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid, $loin_info)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total, $total_bonus) = OE('user')->commissionBonusList($uid, $param['level'], $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_bonus' => $total_bonus,
            'list' => $list,
        ];
    }
}
