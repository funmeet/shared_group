<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class QuestionDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "问题详情";
    public $group = '在线客服';
    public $desc = "";

    public $input = [
        'question_id' => 'required;int;label=问题id;comment=',
    ];

    public $output = [
        'question' => [
            'id' => 'label=问题id;',
            'title' => 'label=问题标题;',
            'status' => 'label=问题状态;',
            'status_label' => 'label=问题状态;',
            'create_time' => 'label=创建时间;',
        ],
        'list' => [
            [
                'is_questioner' => 'label=是否提问人;comment=0不是1是',
                'avatar_url' => 'label=头像地址;',
                'content' => 'label=回答内容;',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid, $login_info)
    {
        $result=OE('user')->questionDetail($param['question_id'], $uid, $login_info);
        return $result;
    }
}
