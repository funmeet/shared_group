<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CommissionSaleList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "销售佣金";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'level' => 'required;enum=1,2,12,13;label=佣金级别;comment=1销售佣金 2服务佣金 12金牌分红 13钻石分红',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_commission' => 'label=总佣金;',
        'list' => [
            [
                'uid' => 'label=用户id;',
                'real_name' => 'label=姓名;',
                'avatar_url' => 'label=头像;',
                'mobile' => 'label=手机号;',
                'name' => 'label=姓名;',
                'total_commission' => 'label=总收益佣金;',
                'total_buy' => 'label=总消费;',
                'total_contribution' => 'label=总贡献',
            ]
        ]
    ];

    public function run($param, $uid, $loin_info)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total, $total_commission) = OE('user')->commissionSaleList($uid, $param['level'], $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_commission' => $total_commission,
            'list' => $list,
        ];
    }
}
