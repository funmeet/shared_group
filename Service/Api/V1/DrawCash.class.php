<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class DrawCash
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "提现";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'draw_money' => 'required;number;min=0.01;label=提现金额;comment=',
        'pay_password' => 'required;label=支付密码;',
    ];

    public $output = [
    ];

    public function run($param, $uid, $login_info)
    {
        OE('user')->drawCash($param, $uid, $login_info);
        return [];
    }
}
