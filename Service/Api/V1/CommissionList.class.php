<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CommissionList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "佣金明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'type' => 'required;int;label=佣金类型;comment=0代付佣金1一级销售佣2二级销售佣金3管理层佣金',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_commission' => 'label=总佣金;',
        'list' => [
            [
                'title' => 'label=标题;',
                'type' => 'label=佣金类型;',
                'type_label' => 'label=佣金类型;',
                'order_no' => 'label=订单号;',
                'money' => 'label=佣金',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total, $total_commission) = OE('user')->commissionList($uid, $param['type'], $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_commission' => $total_commission,
            'list' => $list,
        ];
    }
}
