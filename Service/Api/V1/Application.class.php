<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Application
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "商家入驻";
    public $group = '商家';
    public $desc = "";

    public $input = [
    'type' => 'label=入驻类型;',
    'username' => 'label=联系人;',
    'mobile' => 'label=联系电话;',
    'company' => 'label=公司名称;',
    'nature' => 'label=入驻性质;',
    'mode' => 'label=入驻方式;',
    'make_time' => 'label=预约最佳时间;',
    ];

    public $output = [
        'type' => 'label=入驻类型;',
        'type_label' => 'label=入驻类型;',
        'username' => 'label=联系人;',
        'mobile' => 'label=联系电话;',
        'company' => 'label=公司名称;',
        'nature' => 'label=入驻性质;',
        'nature_label' => 'label=入驻性质;',
        'mode' => 'label=入驻方式;',
        'mode_label' => 'label=入驻方式;',
        'make_time' => 'label=预约最佳时间;',
        
        
    ];

    public function run($param, $uid)
    {
       OE('business')->application($param, $uid);
        return true;
    }
}
