<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class SiteConfig
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "配置";
    public $group = '网站';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'site' => [
            'open' => 'label=网站是否开放;comment=0未开放跳转到维护页,1开放',
            'copyright' => 'label=版权;',
            'open_yibao' => 'label=是否显示艺宝按钮;comment=0不显示1显示',
            'open_expert' => 'label=是否显示专家在线;comment=0不显示1显示',
        ],
        'draw_money' => [
            'min_money' => 'label=提现最小金额;',
            'fee_percent' => 'label=提现手续费比例;',
            'money_multiple' => 'label=提现最小面额;',
        ],
        'customer_service' => [
            'type_list' => [
                [
                    'id' => 'label=类型id;',
                    'title' => 'label=类型标题;',
                ]
            ]
        ],
        'postage' => [
            'free_min_money' => 'label=订单满多少钱免邮费;comment=没有满足则按每件商品各自的邮费总和'
        ],
        'hongbao' => [
            'ads' => [
                'img' => 'label=广告图片地址',
                'link' => 'label=链接地址',
            ]
        ],
        'goods' => [
            'max_buy_tip' => 'label=商品限购提示',
        ],
        'info_is_full' => 'label=用户资料是否完整',
        'wap_reg_api_open' => 'label=站外注册是否开启',
    ];

    public function run($param, $uid)
    {
        $question_type = get_list_config('QUESTION_TYPE');
        $type_list = [];
        foreach ($question_type as $key => $val) {
            $type_list[] = [
                'id' => $key,
                'title' => $val,
            ];
        }

        if($uid){
            $user_detail =  OE('user')->getDetail($uid);
            if($user_detail['mobile']){
                $info_is_full = 1;
            }else{
                $info_is_full = 0;
            }
        }else{
            $info_is_full = 0;
        }


        return [
            'site' => [
                'open' => C('WEB_SITE_CLOSE') ? 0 : 1,
                'copyright' => C('WEB_SITE_COPYRIGHT'),
                'open_yibao' => C('WEB_SITE_OPEN_YIBAO'),
                'open_expert' => C('WEB_SITE_OPEN_EXPERT'),
            ],
            'draw_money' => [
                'min_money' => C('DRAW_MIN_MONEY', null, 0.01),
                'fee_percent' => C('DRAW_MONEY_FEE', null, 0),
                'money_multiple' => C('DRAW_MONEY_MULTIPLE', null, 0),
            ],
            'customer_service' => [
                'type_list' => $type_list,
            ],
            'postage' => [
                'free_min_money' => C('FREE_MIN_MONEY')
            ],
            'hongbao' => [
                'ads' => [
                    'img' => get_cover(C('hongbao_img'), 'path'),
                    'link' => C('hongbao_link'),
                ]
            ],
            'goods' => [
                'max_buy_tip' => C('max_buy_tip'),
            ],
            'info_is_full' => $info_is_full,
            'wap_reg_api_open' => C('WAP_REG_API_OPEN') ? 1 : 0,
        ];
    }
}
