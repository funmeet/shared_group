<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartRemove
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "移除商品";
    public $group = '购物车';
    public $desc = "";

    public $input = [
        'goods_ids[]' => 'required;label=商品id;comment=多个id用数组形式,例如goods_ids[]＝123&goods_ids[]＝124',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->removeGoods($param['goods_ids'], $uid);
        return;
    }
}
