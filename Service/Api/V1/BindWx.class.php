<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class BindWx
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "绑定用户";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'old_password' => 'required;length=6,;label=登录密码;',
        'old_username' => 'required,;label=老会员用户名;',
        'confirm_password' => 'required;length=6,;label=确认密码;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('user')->bindWx($param, $uid);
        return [];
    }
}
