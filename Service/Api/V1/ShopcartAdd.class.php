<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartAdd
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "加入购物车";
    public $group = '购物车';
    public $desc = "";

    public $input = [
        'goods_id' => 'required;int;label=商品id;',
        'spec_value' => 'string;label=商品规格;comment=传中文，格式：颜色:黑色 尺码:S码;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->addCart($param, $uid);
        return;
    }
}
