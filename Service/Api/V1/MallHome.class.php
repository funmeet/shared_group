<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class MallHome
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "首页";
    public $group = '商城';
    public $desc = "";

    public $input = [
        'page_size' => 'int;label=每组分类读取多少个商品;comment=默认每组分类显示5个商品',
		'is_vip' => 'int;label=vip购买资格;comment='
    ];

    public $output = [
        'banner' => [
            [
                'title' => 'label=标题;comment=',
                'pic_path' => 'label=图片地址;comment=',
                'link_url' => 'label=链接地址;comment=',
                'target_str' => 'label=链接打开方式;comment=',
            ]
        ],
        'goods' => [
            [
                'id' => 'label=商品类别',
                'title' => 'label=商品类别',
                'pic' => 'label=banner图片',
                'link' => 'label=banner跳转地址',
                'list' => [
                    [
                        'id' => 'label=商品id;',
                        'title' => 'label=标题;',
                        'intro' => 'label=描述;',
                        'sell_price' => 'label=零售价;',
                        'vip_price' => 'label=会员价;',
                        'max_buy' => 'label=限购;',
                        'contribution' => 'label=贡献;',
                        'deviation_point' => 'label=抵扣积分;',
                        'give_point' => 'label=赠送积分;',
                        'sell_num' => 'label=销量;',
                        'stock' => 'label=库存;',
                        'postage' => 'label=邮费;',
                        'detail' => 'label=详情;',
                        'create_time' => 'label=创建时间;',
                        'update_time' => 'label=更新时间;',
                        'list_cover' => 'label=列表封面;comment=单图',
                        'picture_urls' => 'label=详情封面;comment=多图',
                        'poster' => 'label=首页海报图;comment=单图',
						'unit' => 'label=单位',
						'sum_1' => 'label=一级数量',
						'sum_2' => 'label=二级数量',
						'sum_3' => 'label=三级数量',
						'price_1' => 'label=一级价格',
						'price_2' => 'label=二级价格',
						'price_3' => 'label=三级价格',
                    ]
                ]
            ]
        ],
        'index_list' => [
			[
				'id' => 'label=商品id;',
				'title' => 'label=标题;',
				'intro' => 'label=描述;',
				'sell_price' => 'label=零售价;',
				'vip_price' => 'label=会员价;',
				'max_buy' => 'label=限购;',
				'contribution' => 'label=贡献;',
				'deviation_point' => 'label=抵扣积分;',
				'give_point' => 'label=赠送积分;',
				'sell_num' => 'label=销量;',
				'stock' => 'label=库存;',
				'postage' => 'label=邮费;',
				'detail' => 'label=详情;',
				'create_time' => 'label=创建时间;',
				'update_time' => 'label=更新时间;',
				'list_cover' => 'label=列表封面;comment=单图',
				'picture_urls' => 'label=详情封面;comment=多图',
				'poster' => 'label=首页海报图;comment=单图',
				'unit' => 'label=单位',
				'sum_1' => 'label=一级数量',
				'sum_2' => 'label=二级数量',
				'sum_3' => 'label=三级数量',
				'price_1' => 'label=一级价格',
				'price_2' => 'label=二级价格',
				'price_3' => 'label=三级价格',
			]
        ],
        'poster' => [
                 [
	              'id' =>'label=类别id',
	              'title' => 'label=标题',
	              'pic' => 'label=banner图片',
	              'pic_url'=>'label=图片url',
	              'link' => 'label=banner跳转地址',
                  ]
        		],
		'index_adv_link' => 'label=banner跳转地址',
		'index_adv_pic'=>'label=图片url',
    ];

    public function run($param, $uid)
    {
        $banner = O('site')->getPosterList('mall_home_banner');
        $goods = O('goods')->getGroupList($param['page_size'], $uid, $param['is_vip']);
        $poster = O('goods')->getTypeList();
        $index_list = O('goods')->getIndexList($uid, $param['is_vip']);
		$index_adv = O('site')->getPosterList('site_home_banner');
		$index_adv = $index_adv[0];
        return [
            'banner' => $banner,
            'goods' => $goods,
            'poster' => $poster,
            'index_list'=>$index_list,
            'index_adv_link'=>$index_adv['link_url'],
            'index_adv_pic'=>$index_adv['pic_path'],
        ];
    }
}
