<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderSubmitInvite
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "代付下单";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'goods[]' => 'required;int;label=商品id->购买数量;comment=goods[goods_id]＝buy_num&goods[goods_id]＝buy_num',
        'address_id' => 'required;int;label=地址id',
        'pay_password' => 'required;label=支付密码;comment=余额支付为必填',
        'mobile' => 'required;label=代付人手机号;comment=代付为必填',
        'phone' => 'required;label=手机号;comment=为必填',
        'red_packets' => 'label=使用红包金额;comment=0或不填则不使用红包',
    ];

    public $output = [
        'order_no' => 'required;bigint;label=订单号;comment=用于支付订单',
    ];

    public function run($param, $uid, $login_info)
    {
    	OE('user')->chenckmMobile($param['phone'],$uid);
        $order_no = OE('order')->submitInvite($param, $uid, $login_info);
        return [
            'order_no' => $order_no
        ];
    }
}
