<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Model;

class GetOtherUserInfo
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "用户信息";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'mobile' => 'required;label=手机号;',
    ];

    public $output = [
        'username' => 'label=用户名;',
        'real_name' => 'label=真实姓名;',
        'nickname' => 'label=昵称;',
    ];

    public function run($param)
    {
        $model = new Model();
        $info = $model->table('__USER__')
            ->field('uid,nickname,real_name')
            ->where(['mobile' => $param['mobile']])
            ->find();
        if (!$info) {
            throw new \Exception("该手机号未注册");
        }
        if(!$info['real_name'] && !$info['nickname']){
            throw new \Exception("真实姓名和昵称为空");
        }
        return $info;
    }
}
