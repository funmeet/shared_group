<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Model;

class GoodsDetail
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "详情";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'goods_id' => 'required;int;label=商品id;',
    ];

    public $output = [
        'id' => 'label=商品id;',
        'title' => 'label=标题;',
        'intro' => 'label=描述;',
        'sell_price' => 'label=零售价;',
        'vip_price' => 'label=会员价;',
        'max_buy' => 'label=限购;',
        'contribution' => 'label=贡献;',
        'deviation_point' => 'label=抵扣积分;',
        'give_point' => 'label=赠送积分;',
        'sell_num' => 'label=销量;',
        'stock' => 'label=库存;',
        'postage' => 'label=邮费;',
        'detail' => 'label=详情;',
        'create_time' => 'label=创建时间;',
        'update_time' => 'label=更新时间;',
        'list_cover' => 'label=列表封面;comment=单图',
        'picture_urls' => 'label=详情封面;comment=多图',
        'is_rebuy' => 'label=是否复购;comment=0不是 1是',
        'vip_level' => 'label=vip级别',
        'is_vip' => 'label=vip购买资格限制',
        'unit' => 'label=单位',
        'sum_1' => 'label=一级数量',
        'sum_2' => 'label=二级数量',
        'sum_3' => 'label=三级数量',
        'price_1' => 'label=一级价格',
        'price_2' => 'label=二级价格',
        'price_3' => 'label=三级价格',
        'spec' => [
            [
                'name' => 'label=规格名称',
                'list' => [
                    [
                        'id' => 'label=id',
                        'v' => 'label=名称',
                    ]
                ]
            ]
        ]
    ];

    public function run($param, $uid, $login_info)
    {
        $result = OE('goods')->getDetail($param['goods_id'], $uid);
        $model = new Model();
        $user_data = $model->table('__USER__')
            ->field('uid, vip_level')
            ->where(array('uid' => $uid))
            ->find();
        $result['vip_level'] = $user_data['vip_level'];
        $spec_list = M('spec')->where(['goods_id' => $param['goods_id']])->select();
        foreach($spec_list as &$item){
            $v_arr = preg_split("/(-|,|，)/",$item['v']);
            foreach($v_arr as $k => $v){
                $item['list'][] = [
                    'id' => $k,
                    'v' => $v,
                ];
            }
        }
        $result['spec'] = $spec_list;
        return $result;
    }
}
