<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class QuestionAnswer
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "问题追问";
    public $group = '在线客服';
    public $desc = "";

    public $input = [
        'question_id' => 'required;label=问题id',
        'content' => 'required;label=内容;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('user')->questionAnswer($param['question_id'], $uid, $param['content']);
        return [];
    }
}
