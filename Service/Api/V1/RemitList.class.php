<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class RemitList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "汇款明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'status' => 'required;int;enum=0,1,2;label=汇款状态;comment=0已提交1审核成功2审核失败',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'list' => [
            [
                'bank' => 'label=订单号;',
                'receive_money' => 'label=入款金额;',
                'remit_money' => 'label=汇款金额;',
                'remit_time' => 'label=汇款时间;',
                'remit_name' => 'label=汇款人;',
                'recharge_mobile' => 'label=要充值用户的手机号;',
                'remark' => 'label=备注;',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('user')->remitList($uid, $param['status'], $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
        ];
    }
}
