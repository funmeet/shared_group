<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Finance
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "财务统计";
    public $group = '财务';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'balance' => 'label=余额;',
        'total_point' => 'label=购物积分;',
        'total_income' => 'label=会员收益',
        'goods_income' => 'label=销售佣金',
        'service_income' => 'label=服务佣金',
        'total_red_packets' => 'label=红包收益',
        'red_packets' => 'label=红包余额',
        'jinpai_income' => 'label=金牌分红',
        'zuanshi_income' => 'label=钻石分红',
        'huangguan_income' => 'label=皇冠分红',
        'huangguandashi_income' => 'label=皇冠大使分红',
        'zhizun_income' => 'label=至尊分红',
        'wuxing_income' => 'label=五星分红',
        'service_center_income' => 'label=服务中心',
        'total_order' => 'label=总订单数',
        'order_unpaid_num' => 'label=未支付订单数',
        'order_paid_num' => 'label=已支付订单数',
        'order_send_num' => 'label=已发货订单数',
        'order_receive_num' => 'label=已收货订单数',
        'info_is_full' => 'label=用户资料是否完整',
    ];

    public function run($param, $uid, $login_info)
    {
        $result = OE('user')->finance($login_info);
        return $result;
    }
}
