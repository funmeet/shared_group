<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class QuestionList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "问题列表";
    public $group = '在线客服';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'list' => [
            [
                'id' => 'label=问题id;',
                'title' => 'label=标题;',
                'type' => 'label=类型;',
                'type_label' => 'label=类型;',
                'status' => 'label=状态;',
                'status_label' => 'label=状态;',
                'create_time' => 'label=创建时间;',
                'is_read' => 'label=是否已读;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('user')->questionList($uid, $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
        ];
    }
}
