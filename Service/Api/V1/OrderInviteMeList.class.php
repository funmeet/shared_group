<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderInviteMeList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "别人邀请我代付";
    public $group = '代付';
    public $desc = "";

    public $input = [
        'friend_status' => 'required;enum=0,1,2;label=代付状态;comment=0已提交1已支付2已拒绝',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'list' => [
            [
                'order_no' => 'label=订单号;',
                'order_owner' => 'label=订单归属人;comment=别人邀请我代付的时候',
                'order_owner_mobile' => 'label=订单归属人手机号;comment=别人邀请我代付的时候',
                'order_status' => 'label=订单状态;',
                'order_status_label' => 'label=订单状态描述;',
                'order_contribution' => 'label=订单总贡献;',
                'give_point' => 'label=订单赠送积分;',
                'deviation_point' => 'label=订单抵扣积分;',
                'goods_id' => 'label=商品id;',
                'buy_num' => 'label=购买数量;',
                'goods_price' => 'label=商品价格;',
                'pay_money' => 'label=支付金额;',
                'pay_time' => 'label=支付时间;',
                'create_time' => 'label=创建时间;',
                'friend_status' => 'label=代付状态;',
                'friend_mobile' => 'label=代付人手机号;',
                'friend_name' => 'label=代付人姓名;',
                'friend_fee_money' => 'label=服务费金额;',
                'reason' => 'label=拒绝原因;',
                'goods' => [
                    [
                        'title' => 'label=商品标题;',
                        'intro' => 'label=商品描述;',
                        'give_point' => 'label=赠送积分;',
                        'picture_urls' => 'label=图片;',
                        'deviation_point' => 'label=抵扣积分;',
                        'contribution' => 'label=商品贡献;',
                    ]
                ],
            ]
        ],
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('order')->inviteMeList($uid, $param['friend_status'], $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
        ];
    }
}
