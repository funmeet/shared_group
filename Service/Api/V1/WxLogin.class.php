<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Log;

class wxLogin
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "微信登录";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'openid' => 'required;label=openid;comment=微信用户openid',
    ];

    public $output = [
        'api_token' => 'label=接口token;comment=用户登录状态标识,通过header参数API-TOKEN=api_token提交;',
        'expire_time' => 'label=接口token过期时间;',
    ];

    public function run($param, $uid)
    {
        $result = OE('user')->wxLogin($param['openid']);
        Log::w_log(json_encode($result));
        list($api_token,$expire_time) = $result;
        return [
            'api_token' => $api_token,
            'expire_time' => $expire_time,
        ];
    }
}
