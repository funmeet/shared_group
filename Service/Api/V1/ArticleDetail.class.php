<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ArticleDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "详情";
    public $group = '文章';
    public $desc = "";

    public $input = [
        'article_id' => 'required;int;label=文章id;',
    ];

    public $output = [
        'id' => 'label=id;',
        'title' => 'label=标题;',
        'cate_title' => 'label=分类标题;',
        'description' => 'label=描述;',
        'cover_path' => 'label=封面地址;',
        'create_time' => 'label=创建时间;',
        'content' => 'label=内容;',
    ];

    public function run($param, $uid)
    {
        $result = OE('site')->articleDetail($param['article_id']);
        return $result;
    }
}
