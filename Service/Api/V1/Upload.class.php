<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Upload
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "上传头像";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'api_token' => 'required;label=接口token;',
    ];

    public $output = [
        'avatar_url' => 'label=头像地址;',
    ];

    public function run($param, $uid)
    {
        $result = OE('file')->upload();
        //$re = OE('user')->qrcode($uid);
        $data = json_decode($result, true);
        return [
            'avatar_url' => $data['result']['url']
        ];
    }
}
