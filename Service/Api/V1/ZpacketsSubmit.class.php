<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ZpacketsSubmit
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "红包转余额";
    public $group = '财务';
    public $desc = "";

    public $input = [

        'money' => 'required;number;label=转账金额;',
        'pay_password' => 'required;label=支付密码;',
    ];

    public $output = [
    ];

    public function run($param, $uid, $login_info)
    {
        OE('user')->zpacketsSubmit($param,$login_info);
        return [];
    }
}
