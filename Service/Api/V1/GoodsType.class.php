<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GoodsType
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "首页";
    public $group = '商城';
    public $desc = "";

    public $input = [
        'page_size' => 'int;label=每组分类读取多少个商品;comment=默认每组分类显示5个商品',
        'id'=>'int;label=id',
        'order'=>'label=排序，综合：id，销量：sell_num，价格：vip_price',
        'mode'=>'label=排序方式，升序：asc,降序：desc',
    ];

    public $output = [
        'banner' => [
            [
                'title' => 'label=标题;comment=',
                'pic_path' => 'label=图片地址;comment=',
                'link_url' => 'label=链接地址;comment=',
                'target_str' => 'label=链接打开方式;comment=',
            ]
        ],
        'goods' => [
            [
                'id' => 'label=商品类别',
                'title' => 'label=商品类别',
                'pic' => 'label=banner图片',
                'link' => 'label=banner跳转地址',
                'list' => [
                    [
                       // 'str'=>'label=商品id;',
                        'id' => 'label=商品id;',
                        'title' => 'label=标题;',
                        'intro' => 'label=描述;',
                        'sell_price' => 'label=零售价;',
                        'vip_price' => 'label=会员价;',
                        'max_buy' => 'label=限购;',
                        'contribution' => 'label=贡献;',
                        'deviation_point' => 'label=抵扣积分;',
                        'give_point' => 'label=赠送积分;',
                        'sell_num' => 'label=销量;',
                        'stock' => 'label=库存;',
                        'postage' => 'label=邮费;',
                        'detail' => 'label=详情;',
                        'create_time' => 'label=创建时间;',
                        'update_time' => 'label=更新时间;',
                        'list_cover' => 'label=列表封面;comment=单图',
                        'picture_urls' => 'label=详情封面;comment=多图',
                        'poster' => 'label=首页海报图;comment=单图',
                    ]
                ]
            ]
        ],
        
        
    ];

    public function run($param, $uid)
    {
        $banner = O('site')->getPosterList('mall_home_banner');
        $goods = O('goods')->getGroupList2($param['page_size'], $uid,$param['id'],$param['order'],$param['mode']);
        

        return [
            'banner' => $banner,
            'goods' => $goods,
           
        ];
    }
}
