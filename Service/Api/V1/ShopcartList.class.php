<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "购物车清单";
    public $group = '购物车';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'list' => [
            [
                'goods_id' => 'label=商品id;',
                'buy_num' => 'label=购买数量;',
                'title' => 'label=标题;',
                'intro' => 'label=描述;',
                'sell_price' => 'label=零售价;',
                'vip_price' => 'label=会员价;',
                'max_buy' => 'label=限购;',
                'is_rebuy' => 'label=是否复购;',
                'contribution' => 'label=贡献;',
                'deviation_point' => 'label=抵扣积分;',
                'give_point' => 'label=赠送积分;',
                'stock' => 'label=库存;',
                'postage' => 'label=邮费;',
                'list_cover' => 'label=列表封面;comment=单图',
                'spec_label' => 'label=规格',
                'unit' => 'label=单位',
                'sum_1' => 'label=一级数量',
                'sum_2' => 'label=二级数量',
                'sum_3' => 'label=三级数量',
                'price_1' => 'label=一级价格',
                'price_2' => 'label=二级价格',
                'price_3' => 'label=三级价格',

            ]
        ],
        'vip_level' => 'label=vip等级'
    ];

    public function run($param, $uid, $login_info)
    {
        $list = OE('order')->cartList($uid);
        return [
            'list' => $list,
            'vip_level' => $login_info['vip_level']
        ];
    }
}
