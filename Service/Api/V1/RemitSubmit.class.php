<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class RemitSubmit
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "提交汇款";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'bank' => 'required;label=银行;',
        'remit_money' => 'required;number;min=0.01;label=汇款金额;',
        'remit_time' => 'required;date;label=汇款时间;commit=2011-1-1',
        'remit_name' => 'required;label=汇款人;',
        'recharge_mobile' => 'required;mobile;label=要充值用户的手机号;',
        'remark' => 'label=备注;',
    ];

    public $output = [

    ];

    public function run($param, $uid)
    {
        OE('user')->submitRemit($param, $uid);
        return [];
    }
}
