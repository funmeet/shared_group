<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GoodsSearch
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "搜索";
    public $group = '商品';
    public $desc = "";

    public $input = [
        'keyword' => 'required;label=关键词',
    ];

    public $output = [
        'list' => [
            [
                'id' => 'label=商品id;',
                'title' => 'label=标题;',
                'intro' => 'label=描述;',
                'sell_price' => 'label=零售价;',
                'vip_price' => 'label=会员价;',
                'max_buy' => 'label=限购;',
                'contribution' => 'label=贡献;',
                'sell_num' => 'label=销量;',
                'stock' => 'label=库存;',
                'postage' => 'label=邮费;',
                'list_cover' => 'label=列表封面;comment=单图',
                'picture_urls' => 'label=详情封面;comment=多图',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $list = O('goods')->search($param['keyword']);
        return compact('list');
    }
}
