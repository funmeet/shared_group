<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AreaDict
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "地区";
    public $group = '词典';
    public $desc = "";

    public $input = [
    ];

    public $output = [

    ];

    public function run($param, $uid)
    {
        $result=OE('common')->getAreaDict();
        return $result;
    }
}
