<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class AddressMatch
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "地址匹配";
    public $group = '地址';
    public $desc = "";

    public $input = [
        'content' => 'required;string;label=地址内容',
    ];

    public $output = [
        'name' => 'label=收件人姓名',
        'mobile' => 'label=收件人手机号',
        'province_id' => 'label=省id',
        'province' => 'label=省',
        'city_id' => 'label=市id',
        'city' => 'label=市',
        'area_id' => 'label=区id',
        'area' => 'label=区',
        'address' => 'label=详细地址',
    ];

    public function run($param, $uid)
    {
        $a = "大熊15578788765   湖南省长沙市岳麓区枫华府第D9栋";
        $a = $param['content'];

        $address_arr = preg_split("/(\n|\r|\t| )/" , $a);
        $address = '';
        foreach($address_arr as $k => $v){
            if(strpos($v,'省')  && strpos($v,'市')){
                $address = $v;
            }

        }
//        var_dump($address);exit();

        if(preg_match('/1\d{10}/', $a, $mobiles)){ // 手机
            $result['mobile'] = $mobiles[0];
        } else if(preg_match('/(\d{3,4}-)?\d{7,8}/', $a, $mobiles)){ // 固定电话
            $result['mobile'] = $mobiles[0];
        }

        // 识别姓名-必须空格分享的--概率
        preg_match_all('/[\x{4e00}-\x{9fa5}]{2,}/iu', $a,$names);
        if($names){
            $name_where = '';
            foreach ($names[0] as $name){
                // 必须是大于1个字符且小于5个字符的
                if(1 < mb_strlen($name,'utf-8') && mb_strlen($name, 'utf-8') < 5){
                    $sub_name = mb_substr($name, 0, 1, 'utf-8');
                    $name_where .= "name like '{$sub_name}%' or ";
                }
            }
        }
        $result['name'] = $names[0][0];

        $add_arr = explode('省', $address);
        if(count($add_arr)>1){
            $result['province'] = $add_arr[0];
            $add_city_arr = $add_arr = explode('市', $add_arr[1]);
            if(count($add_city_arr)>1){
                $result['city'] = $add_city_arr[0];
                $add_coun_arr = preg_split("/(区|县)/",$add_city_arr[1]);
                if(count($add_coun_arr)>1){
                    $result['cou'] = $add_coun_arr[0];
                    $result['address'] = $add_coun_arr[1];
                }else{
                    $result['address'] = $add_coun_arr[0];
                }

            }else{
                $result['address'] = $add_city_arr[0];
            }


        }else{
            $result['address'] = $add_arr[0];
        }



        if($result['province']){
            $ret = M('area')->where([
                'area_name' => ['like', "%{$result['province']}%"],
                'area_deep' => 1])
                ->find();
            $result['province_id'] = $ret['area_id'];
            $result['province'] = $ret['area_name'];
        }
        if($result['city']){
            $ret = M('area')->where(['area_name' => [
                'like', "%{$result['city']}%"],
                'area_deep' => 2])->find();
            $result['city_id'] = $ret['area_id'];
            $result['city'] = $ret['area_name'];
        }

        if($result['cou']){
            $ret = M('area')->where(['area_name' => [
                'like', "%{$result['cou']}%"],
                'area_deep' => 3])->find();
            $result['area_id'] = $ret['area_id'];
            $result['area'] = $ret['area_name'];
        }

//        var_dump($result);exit();

        return $result;
    }
}