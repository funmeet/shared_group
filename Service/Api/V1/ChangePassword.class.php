<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ChangePassword
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "修改密码";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'old_password' => 'required;length=6,;label=登录密码;',
        'new_password' => 'required;length=6,;label=新密码;',
        'confirm_password' => 'required;length=6,;label=确认密码;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('user')->changePassword($param, $uid);
        return [];
    }
}
