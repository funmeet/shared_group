<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CzSubmit
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "充值";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'cz_img' => 'required;label=截图',
//        'account' => 'required;label=账号',
        'remit_money' => 'required;decimal;label=金额',
        'type' => 'required;label=0支付宝 1微信',
    ];

    public $output = [

    ];

    public function run($param, $uid, $login_info)
    {
//        var_dump($param);exit();

        $ret = M('remit')->add([
            'remit_name' => $login_info['nickname'],
            'remit_money' => $param['remit_money'],
//            'account' => $param['account'],
            'img' => $param['cz_img'],
            'type' => $param['type'],
            'uid' => $uid,
            'to_uid' => $uid,
            'create_time' => date('Y-m-d H:i:s'),
            'remit_time' => date('Y-m-d')
        ]);
        if(!$ret) throw new \Exception('充值提交失败');
//        OE('address')->submit($param, $uid);
        return true;
    }
}