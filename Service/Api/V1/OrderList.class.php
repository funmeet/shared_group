<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "列表";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'order_status' => 'required;int;enum=0,1,2,3;label=订单状态;comment=0已提交1已支付2已发货3已收货',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'list' => [
            [
                'order_no' => 'label=订单号;',
                'order_status' => 'label=订单状态;',
                'order_status_label' => 'label=订单状态描述;',
                'order_contribution' => 'label=订单总贡献;',
                'give_point' => 'label=订单赠送积分;',
                'deviation_point' => 'label=订单抵扣积分;',
                'friend_fee_money' => 'label=代付抵扣金额;comment=',
                'postage' => 'label=订单邮费;',
                'total_money' => 'label=应付金额;',
                'pay_money' => 'label=支付金额;',
                'pay_time' => 'label=支付时间;',
                'create_time' => 'label=创建时间;',
                'goods' => [
                    [
                        'title' => 'label=商品标题;',
                        'sell_price' => 'label=零售价;',
                        'vip_price' => 'label=会员价;',
                        'max_buy' => 'label=限购数量',
                        'buy_num' => 'label=购买数量;',
                        'goods_total_money' => 'label=应付金额;',
                        'goods_pay_money' => 'label=实付金额;',
                        'goods_deviation_point' => 'label=商品总抵扣积分;',
                        'goods_give_point' => 'label=商品总赠送积分;',
                        'goods_contribution' => 'label=商品总贡献值;',
                        'intro' => 'label=商品描述;',
                        'list_cover' => 'label=图片;',
                    ]
                ]
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('order')->lists($param['order_status'], $uid, $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'list' => $list,
        ];
    }
}
