<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class SynLogin
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "同步登录";
    public $group = '用户';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        'syn_login_url' => 'label=同步登录地址;',
    ];

    public function run($param, $uid)
    {
        $syn_login_url = OE('user')->getSynLoginUrl($uid, 1);
        return [
            'syn_login_url' => $syn_login_url
        ];
    }
}
