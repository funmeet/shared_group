<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class PacketsDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "红包收益";
    public $group = '财务';
    public $desc = "";

    public $input = [
       // 'level' => 'required;enum=1,2;label=佣金级别;comment=1零售佣金2服务佣金',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_red_packets' => 'label=总红包金额;',
        'list' => [
            [
                'uid' => 'label=用户id;',
                'real_name' => 'label=姓名;',
                'avatar_url' => 'label=头像;',
                'mobile' => 'label=手机号;',
                'name' => 'label=姓名;',
                'total_commission' => 'label=总收益佣金;',
                'total_buy' => 'label=总消费;',
                'total_contribution' => 'label=总贡献',
                'type'=> 'label=红包类型',
                'status'=> 'label=红包类型2',
                'money'=> 'label=红包金额',
                'create_time'=> 'label=获得时间',
                'username'=>'label=用户名',
            ]
        ]
    ];

    public function run($param, $uid, $loin_info)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total) = OE('packets')->packets($uid, $page_num, $page_size);
        foreach ($list as $k=>$v){
        	$list[$k]['username'] = M('ucenter_member')->where(['id'=>$v['uid']])->getField('username');
        	$list[$k]['status'] = $v['type'] == '1' ?0:1;
        	switch ($v['type']) {
        		case '0':
        		$list[$k]['type'] = '每日红包收益';
        		break;
        		case '1':
        		$list[$k]['type'] = '红包转余额';
        		break;
        		default:
        			;
        		break;
        	}
        }
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_red_packets' => $loin_info["total_red_packets"],
            'list' => $list,
        ];
    }
}
