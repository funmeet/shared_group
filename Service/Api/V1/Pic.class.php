<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Pic
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "二维码背景图";
    public $group = '用户';
    public $desc = "";

    public $input = [
         'id'=>'label=图片id;',
    ];

    public $output = [
                [
                    'id' =>'label=图片id;',
                    'title' =>'label=标题;',
                    'pic' =>'label=图片id;',
                    'qrcode_url' =>'label=图片连接地址;',
                    'status' =>'label=是否显示;',
                ]
    ];

    public function run($param, $uid)
    {
        
        $result = OE('user')->getUserQrcode($uid,$param['id']);
        return $result;
    }
}
