<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ShopcartChangeNum
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "修改数量";
    public $group = '购物车';
    public $desc = "";

    public $input = [
        'goods_id' => 'required;int;label=商品id;',
        'buy_num' => 'required;min=1;int;label=购买数量;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->changeNum($param, $uid);
        return;
    }
}
