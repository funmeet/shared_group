<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class QuestionEvaluate
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "问题评价";
    public $group = '在线客服';
    public $desc = "";

    public $input = [
        'question_id' => 'required;label=问题id',
        'status' => 'required;label=状态;enum=1,2;comment=1已解决2未解决',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('user')->questionEvaluate($param, $uid);
        return [];
    }
}
