<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class CommissionSaleDetail
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "销售佣金明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'level' => 'required;enum=1,12,13;label=佣金级别;comment=1销售佣金 12金牌分红 13钻石分红',
        'uid' => 'required;int;label=用户;comment=下级用户id',
        'status' => 'required;enum=0,1;label=佣金状态;comment=0预期收益1已收益',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_commission' => 'label=总佣金;',
        'order_real_name' => 'label=订单用户姓名;',
        'list' => [
            [
                'title' => 'label=标题;',
                'order_contribution' => 'label=贡献值;',
                'money' => 'label=收益佣金;',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total, $total_commission, $user) = OE('user')->commissionSaleDetail($uid, $param, $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_commission' => $total_commission,
            'order_real_name' => $user['real_name'],
            'list' => $param['status'] == 1 ? $list : [],
        ];
    }
}
