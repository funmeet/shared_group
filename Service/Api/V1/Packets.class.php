<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Packets
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "红包收益";
    public $group = '财务';
    public $desc = "";

    public $input = [
       // 'level' => 'required;enum=1,2;label=佣金级别;comment=1零售佣金2服务佣金',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_red_packets' => 'label=总红包收益;',
        'uid'=>'label=用户id;',
        'avatar_url' => 'label=头像;',
        'mobile' => 'label=手机号;',
        'name' => 'label=姓名;',
        'day_red_packets'=>'label=今日红包收益;',
        'list' => [
            [
                'uid' => 'label=用户id;',
                'real_name' => 'label=姓名;',
                'avatar_url' => 'label=头像;',
                'mobile' => 'label=手机号;',
                'name' => 'label=姓名;',
                'total_commission' => 'label=总收益佣金;',
                'total_buy' => 'label=总消费;',
                'total_contribution' => 'label=总贡献',
            ] 
        ]
    ];

    public function run($param, $uid, $loin_info)
    {
      
        $day_red_packets = OE('packets')->getTotalPacketsQuota($uid,date('Y-m-d') , date('Y-m-d'),0);
        return [
            'name'=>$loin_info["name"],
            'mobile'=>$loin_info["mobile"],
            'real_name'=>$loin_info["real_name"],
            'avatar_url'=>$loin_info["avatar_url"],
            'total_red_packets' => $loin_info["total_red_packets"],
            'day_red_packets'=>$day_red_packets,
            'uid'=>$loin_info["uid"],
            
        ];
    }
}
