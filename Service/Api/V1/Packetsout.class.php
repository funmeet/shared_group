<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Packetsout
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "领取红包";
    public $group = '财务管理';
    public $desc = "";

    public $input = [
    ];

    public $output = [
        //'s'=>'label=已收货订单数'
    ];

    public function run($param, $uid)
    {
    	$packets = OE('packets')->packetsout($uid);
       return ['packets'=>$packets];
    }
}
