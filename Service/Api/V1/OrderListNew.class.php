<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

use Think\Model;

class OrderListNew
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "列表1";
    public $group = '订单';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'type' => 'int;label=类型;comment=',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        $type_arr = [1,2,3];
        if(!in_array($param['type'], $type_arr)) throw new \Exception('类型值不正确');
        $sql ="SELECT a.*,c.nickname, c.referer_id FROM `k_team_log` AS a
LEFT JOIN k_order AS b ON b.id=a.order_id
LEFT JOIN k_user AS c ON c.uid=b.uid WHERE a.uid=$uid;";
        $model = new Model();
        $list = $model->query($sql);
        $data = [];
        if($param['type'] == 1){
            foreach($list as $item){
                if($item['referer_id'] == $uid) $data[] = $item;
            }
        }elseif($param['type'] == 2){
            foreach($list as $item){
                $t_uid = $item['referer_id'];
                while(true){
                    if(!$t_uid) break;
                    if($t_uid == $uid && $item['uid'] != $uid) {
                        $data[] = $item;
                        break;
                    }
                    $user_data = M('user')->where(['uid' => $t_uid])->find();
                    $t_uid = $user_data['referer_id'];
                }
            }
        }else{
            $data = $list;
        }
        $total_money = get_decimal(M('team_log')->where(['uid' => $uid])->sum('money'));
        $total_num = M('team_log')->where(['uid' => $uid])->count();
        $month_start = date('Y-m-01 00:00:00');
        $month_end = date('Y-m-01 00:00:00', strtotime('+1months'));
        $month_total_money = get_decimal(M('team_log')->where([
            'uid' => $uid,
            'create_time' => [['EGT', $month_start], ['LT', $month_end]]
        ])
            ->sum('money'));
        $month_total_num = M('team_log')->where([
            'uid' => $uid,
            'create_time' => [['EGT', $month_start], ['LT', $month_end]]
        ])->count();
        $day_total_money = get_decimal(M('team_log')->where([
            'uid' => $uid,
            'create_time' => [['EGT', date('Y-m-d 00:00:00')], ['LT', date('Y-m-d 23:59:59')]]
        ])->sum('money'));
        $day_total_num = M('team_log')->where([
            'uid' => $uid,
            'create_time' => [['EGT', date('Y-m-d 00:00:00')], ['LT', date('Y-m-d 23:59:59')]]
        ])->count();
        return [
            'list' => $data,
            'total_money' => $total_money,
            'total_num' => $total_num,
            'month_total_money' => $month_total_money,
            'month_total_num' => $month_total_num,
            'day_total_money' => $day_total_money,
            'day_total_num' => $day_total_num,
        ];
    }
}
