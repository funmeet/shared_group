<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Register
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "注册";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'mobile' => 'required;mobile;label=手机号',
        'referer' => 'required;label=推荐人;comment=支持填推荐人的手机号、邮箱、用户名',
        'password' => 'required;length=6,;label=密码',
        'confirm_password' => 'required;label=确认密码',
//        'code' => 'required;label=短信验证码',
    ];

    public $output = [
        'api_token' => 'label=接口token;comment=用户登录状态标识,通过header参数API-TOKEN=api_token提交;',
        'expire_time' => 'label=接口token过期时间;',
    ];

    public function run($param, $uid)
    {
        list($api_token, $expire_time) = OE('user')->register($param);
        return [
            'api_token' => $api_token,
            'expire_time' => $expire_time,
        ];
    }
}
