<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class memberAgent
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "申请区域代理";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'company' => 'label=公司名称;',
       // 'fee' => 'label=代理费;',
        'business_licence' => 'label=营业执照地址;',
        'level' => 'enum=0,1;label=申请等级;',
        'location' => 'label=申请地区;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('agent')->applyAgent($param, $uid);
        return [];
    }
}
