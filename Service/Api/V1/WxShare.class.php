<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class WxShare
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "微信分享";
    public $group = '用户';
    public $desc = "";

    public $input = [
        'url' => 'required;hosturl;label=要分享的地址',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        $result = OE('Wechat')->getSignPackage($param['url']);
        return $result;
    }
}
