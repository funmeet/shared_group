<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class QuestionSubmit
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "问题提交";
    public $group = '在线客服';
    public $desc = "";

    public $input = [
        'type' => 'required;label=问题类型;comment=传分类id,参见 SiteConfig 的 customer_service',
        'title' => 'required;label=标题;',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('user')->submitQuestion($param, $uid);
        return [];
    }
}
