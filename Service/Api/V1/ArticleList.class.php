<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ArticleList
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "列表";
    public $group = '文章';
    public $desc = "";

    public $input = [
        'cate' => 'required;label=文章类别标识;comment=新闻公告:notice精彩报道:activity',
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'cate_title' => 'label=文章分类标题;',
        'list' => [
            [
                'id' => 'label=id;',
                'title' => 'label=标题;',
                'description' => 'label=描述;',
                'cover_path' => 'label=封面地址;',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total, $cate_title) = OE('site')->articleLists($param['cate'], $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'cate_title' => $cate_title,
            'list' => $list,
        ];
    }
}
