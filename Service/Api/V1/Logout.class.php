<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class Logout
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "登出";
    public $group = '用户';
    public $desc = "";

    public $input = [
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        O('user')->logout($uid);
        return [];
    }
}
