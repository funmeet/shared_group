<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class GetAgent
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "申请区域代理信息";
    public $group = '用户';
    public $desc = "";

    public $input = [
    ];

    public $output = [
	    'company' => 'label=公司名称;',
	    'referee' => 'label=推荐人;',
	    //'fee' => 'label=代理费;',
	    'business_licence' => 'label=营业执照地址;',
	    'level' => 'label=申请等级;',
	    'level_label' => 'label=申请等级;',
	    'location' => 'area;label=申请地区;',
	    'location_label' => 'label=所在地区;',
        'check_status' => 'label=申请状态;',
        'check_status_label' => 'label=申请状态;',
        
    ];

    public function run($param, $uid)
    {
        $result = OE('agent')->getAgent($uid);
        
        return $result;
    }
}
