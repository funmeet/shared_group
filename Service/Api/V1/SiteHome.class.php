<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class SiteHome
{
    public $login = false; //是否需要登录才能访问该接口
    public $title = "首页";
    public $group = '网站';
    public $desc = "";

    public $input = [
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        $tmp = O('site')->getPosterList('site_home_banner');

        $banner = [];
        foreach ($tmp as $row) {
            $new = [];
            $new['title'] = $row['title'];
            $new['pic_path'] = $row['pic_path'];
            $new['link_url'] = $row['link_url'];
            $new['target_str'] = $row['target_str'];
            $banner[] = $new;
        }

        return [
            'banner' => $banner,
        ];
    }
}
