<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class BankList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "银行列表";
    public $group = '词典';
    public $desc = "";

    public $input = [
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        $list = OE('common')->getBankList();
        return $list;
    }
}
