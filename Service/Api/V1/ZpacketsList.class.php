<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class ZpacketsList
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "转换明细";
    public $group = '财务';
    public $desc = "";

    public $input = [
        'page_num' => 'int;label=分页;comment=默认为1',
        'page_size' => 'int;label=分页条数;comment=默认为5',
    ];

    public $output = [
        'page_num' => 'label=当前页码;',
        'page_size' => 'label=每页条数;',
        'page_count' => 'label=总条数;',
        'page_total' => 'label=总页数;',
        'total_zpackets' => 'label=总转出金额;',
        'total_transfer_in' => 'label=总转入金额;',
        'list' => [
            [
                'title' => 'label=标题;',
                'status' => 'label=状态;comment=0转出1转入',
                'money' => 'label=金额',
                'create_time' => 'label=创建时间;',
            ]
        ]
    ];

    public function run($param, $uid)
    {
        $page_num = $param['page_num'] ?: 1;
        $page_size = $param['page_size'] ?: 5;
        list($list, $page_count, $page_total, $total_zpackets) = OE('user')->zpacketsList($uid, $page_num, $page_size);
        return [
            'page_num' => $page_num,
            'page_size' => $page_size,
            'page_count' => $page_count,
            'page_total' => $page_total,
            'total_zpackets' => $total_zpackets,
           
            'list' => $list,
        ];
    }
}
