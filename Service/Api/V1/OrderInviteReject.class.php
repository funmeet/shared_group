<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Api\V1;

class OrderInviteReject
{
    public $login = true; //是否需要登录才能访问该接口
    public $title = "拒绝代付";
    public $group = '代付';
    public $desc = "";

    public $input = [
        'order_no' => 'required;label=订单号;comment=',
        'reason' => 'label=拒绝理由;comment=',
    ];

    public $output = [
    ];

    public function run($param, $uid)
    {
        OE('order')->inviteReject($param['order_no'], $param['reason'], $uid);
        return [];
    }
}
