<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Agent;

use Service\Common\BaseService;

class Service extends BaseService
{
    public function getAgent($uid)
    {
        return Agent::getInstance()->getAgent($uid);
    }
    
    public function applyAgent($param, $uid)
    {
    	return Agent::getInstance()->applyAgent($param, $uid);
    }
    
    public function examine($data, $id)
    {
    	return Agent::getInstance()->examine($data, $id);
    }
    
    public function settle($id)
    {
    	return Agent::getInstance()->settle($id);
    }
    
    public function getCheckStatus($id)
    {
    	return Agent::getInstance()->getCheckStatus($id);
    }
}