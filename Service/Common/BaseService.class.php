<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Common;

class BaseService
{
    //格式化后台列表数据
    public static function formatCallback($data)
    {
        return self::callback(__FUNCTION__, $data);
    }

    //插入前回调
    public static function beforeInsertCallback($data)
    {
        return self::callback(__FUNCTION__, $data);
    }

    //插入后回调
    public static function afterInsertCallback($result, $data)
    {
        return self::callback(__FUNCTION__, $result, $data);
    }

    //更新后回调
    public static function afterUpdateCallback($result, $data)
    {
        return self::callback(__FUNCTION__, $result, $data);
    }

    private static function callback($fun_name, $arg1, $arg2 = [])
    {
        $call_class = \get_called_class();
        $class_name = basename(dirname(str_replace('\\', '/', $call_class)));
        $class_base = substr($call_class, 0, strrpos($call_class, '\\'));
        $class = $class_base . '\\' . $class_name;

        if (is_callable([$class, $fun_name])) {
            if (is_callable($callable = join('::', [$class, 'getInstance']))) {
                $instance = call_user_func_array($callable, []);
                return $instance->$fun_name($arg1, $arg2);
            }
        }
    }
}