<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Wx;

use Service\Common\BaseService;

class Service extends BaseService
{
    protected $appKey;
    protected $appSecret;

    public function __construct()
    {
        if (WX_JS_CONFIG_OPEN_DEBUG) {
            $this->appKey = WX_JS_CONFIG_APP_KEY;
            $this->appSecret = WX_JS_CONFIG_APP_SECRET;
        } else {
            $this->getWxConfig();
        }
    }


    protected function getWxConfig($where = [])
    {
        $where OR $where = array(
            'type' => 'weixin',
            'status' => 1
        );
        $payConfig = M('OauthSettings')->where($where)->find();
        if (!$payConfig) {
            throw new \Exception('无效的微信配置');
        }
        $payConfig['config'] = json_decode(kcone_think_decrypt($payConfig['config']), true);

        $config = array();
        foreach ($payConfig['config'] as $item) {
            $config[$item['key']] = $item['val'];
        }

        $this->appKey = $config['APP_KEY'];
        $this->appSecret = $config['APP_SECRET'];
    }

    public function sendWxMsg($template_id,$data)
    {
    	return Wx::getInstance()->sendWxMsg($template_id, $data,$this->appKey,$this->appSecret);
    }
    
}