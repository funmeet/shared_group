<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Wx;

use Kcdns\Service\Util\FHttp;
use Think\Log;

class Wx extends \Service\Common\BaseModel
{
    protected $name = 'wx';



	/**
	 * 发送微信模板消息
	 * @param $data
	 * @param string $app_key
	 * @param string $app_secret
	 * @return bool
	 */
	public function sendWxMsg($template_id, $data, $app_key='', $app_secret=''){

		$access_token = $this->getAccessToken($app_key, $app_secret);
		Log::w_log('##########token:  '.$access_token);
		$url = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token={$access_token}";
		$param = array(
			'touser' => $data['openid'], //oQla-59Qd5BFjV4lBbo7z9nMhgvY
			'template_id' => '4-4eUi8xUn_h-TmMOH7o4AzM2rFxKqjteuE1KMeRhiY',
			'url' => 'http://qch.showany.com/H5/myshare',
			'data' => array(
				'productType' => array(
					'value' => $data['productType'],
					'color' => '#173177'
				),
				'name' => array(
					'value' => $data['name'],
					'color' => '#173177'
				),
				'accountType' => array(
					'value' => $data['accountType'],
					'color' => '#173177'
				),
				'account' => array(
					'value' => $data['account'],
					'color' => '#173177'
				),
				'time' => array(
					'value' => date('Y-m-d H:i:s'),
					'color' => '#173177'
				),
				'remark' => array(
					'value' => $data['remark'],
					'color' => '#173177'
				),
			)

		);
		Log::w_log('##########param:  '.json_encode($param));
//		$result = http_post_data($url, json_encode($param));
//		$result = FHttp::http_post_data($url, $param);
		$result = $this->send_post($url, json_encode($param));
		$result = json_decode($result, true);
		Log::w_log('##########result:  '.json_encode($result));
		if($result['errcode'] == 0){
			return true;
		}else{
			return false;
		}
	}

	public function send_post($url, $data_string) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json; charset=utf-8',
				'Content-Length: ' . strlen($data_string))
		);

		$data = curl_exec($ch);
		curl_close($ch);
		return $data;
	}

	/**
	 * 获取token
	 * @param $app_key
	 * @param $app_secret
	 * @return bool
	 */
	public function getAccessToken($app_key, $app_secret){
		$options = $this->config;
		$Url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid={$app_key}&secret={$app_secret}";
//		$result = http_get($Url);
		Log::w_log('获取token--url:'.$Url);
		$result = OE('util')->curl($Url);
		Log::w_log('获取token结果:'.$result);
		$result = json_decode($result, true);
		if($result['access_token']){
			return $result['access_token'];
		}else{
			return false;
		}
	}

    
    
    
    
}