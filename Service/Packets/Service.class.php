<?php
// +-------------------------------------------------------------------
// |
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Service\Packets;

use Service\Common\BaseService;

class Service extends BaseService
{
	public function packets($uid, $page_num = 1, $page_size = 5)
	{
		return Packets::getInstance()->packets($uid, $page_num, $page_size);
	}

    public function packetsout($uid)
    {
        return Packets::getInstance()->packetsout($uid);
    }
    
    public function getTotalPacketsQuota($uid = '',$start_time = '' , $end_time = '',$type ='')
    {
    	return Packets::getInstance()->getTotalPacketsQuota($uid, $start_time , $end_time,$type );
    }
    
    public function getOrderQuota($start_time = '' , $end_time = '')
    {
    	return Packets::getInstance()->getOrderQuota($start_time, $end_time);
    }
    
}