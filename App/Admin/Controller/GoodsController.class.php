<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class GoodsController extends CommonController
{
    public function add()
    {
    try {
	       if (IS_POST) {
	       	$no = I('post.goods.no','');
		       	if ($no != '') {
		       	   OE('business')->checkNo($no);
		       }
	       }
	       
	       parent::add();
	    } catch (\Exception $e) {
	       $this->error($e->getMessage());
	    }
    }
    
    public function edit()
    {
    	try {
    		if (IS_POST) {
    		  $no = I('post.goods.no','');
		       	if ($no != '') {
		       	   OE('business')->checkNo($no);
		       }
    			 
    		}
    		parent::edit();
    	} catch (\Exception $e) {
    		$this->error($e->getMessage());
    	}
    
    }
    
    
    
}