<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class DrawCashController extends CommonController
{

    public function edit()
    {
        if (IS_POST) {
            try {
                OE('user')->checkDrawMoney(I('get.draw_cash.id'), I('post.draw_cash.status'), I('post.draw_cash.remark'));
            } catch (\Exception $e) {
                $this->error($e->getMessage());
            }
            $this->success('操作成功');
        } else {
            parent::edit();
        }
    }
}