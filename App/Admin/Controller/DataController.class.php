<?php
// +-------------------------------------------------------------------
// | 
// +-------------------------------------------------------------------
// | Copyright (c) 2009-2016 All rights reserved.
// +-------------------------------------------------------------------
namespace Admin\Controller;

use Kcdns\Admin\Controller\CommonController;

class DataController extends CommonController
{
   
    
    public function packets()
    {
    	
    	$list = O('data')->getPacketsList(I('get.start_time'), I('get.end_time'));
    	if (I('get.is_load') == 1) {
    		O('data')->explodePackets($list);
    		exit();
    	}
    	$this->assign([
    			'list' => $list,
    			
    			]);
    	$this->display();
    }
    
    public function count()
    {
    	$list = O('data')->count(I('get.start_time'), I('get.end_time'));
    	$this->assign('company', $list);
    	$this->display();
    }
    
}