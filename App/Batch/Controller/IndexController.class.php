<?php

namespace Batch\Controller;

use Think\Controller;

class IndexController extends Controller
{
    public function index()
    {
        if (PHP_SAPI != 'cli') {
            exit('非法操作');
        }

        O('batch')->run();
    }

    public function progress($batch_id = '')
    {
        list($status,$info,$title)=O('batch')->getBatchInfo($batch_id);
        exit(json_encode([
            'status' => $status,
            'info' => $info,
            'title' => $title
        ], JSON_UNESCAPED_UNICODE));
    }

    //合并数据库
    public function syn()
    {
        OE('user')->syn();
    }

    //更新推荐关系
    public function synReferer()
    {
        O('user')->synReferer();
    }
}
